`timescale 1ns/1ps

`include "packages.sv"

/*
Simple module which sends receives data package via UART. Package looks like: AAAAAAFD_byte0_byte1_byte2_byte3_byte4_byte5byte6_DF 
Then replies to PC back
Dependencies: ModTranceiver
*/
module slave_packet_transceiver
#(parameter INPUT_BUS_WIDTH = 64,
  parameter BAUDRATE = 3_000_000,
  parameter CLK_RATE = 100_000_000)
(input  logic clk,
 input  logic rstn,
 
 input  logic [INPUT_BUS_WIDTH-1:0] data_vector,
 
 output connections::uart_wrapper_connection_t data_from_packet,
 output logic new_data_strobe,
 output logic busy,
 output logic led,
 
 output logic tx,
 input  logic rx,
 
 input  logic event_flag);
 
 
localparam BYTE_LENGTH = 8; 
localparam BYTES_QTY_TO_SEND =  INPUT_BUS_WIDTH / BYTE_LENGTH;

localparam integer INT_BAUD_DIVIDER = CLK_RATE / (3*BAUDRATE);
localparam real REAL_BAUD_DIVIDER = CLK_RATE / BAUDRATE;
 
logic transmitter_is_busy, transmitter_loaded_data, receiver_data_is_ready, parity_error, frame_error; //inputs
logic start_transmission, master_got_data; //outputs

logic [7:0] data_to_transmitter, data_from_receiver, rx_data_buffer;
logic [BYTES_QTY_TO_SEND-1:0][7:0] data_vector_buffer;
logic [($clog2(BYTES_QTY_TO_SEND))-1:0] sended_bytes_counter;



typedef struct packed
{
	logic [7:0] preamble_frame_delimiter;	
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx;
	logic [23:0] mtx_duty_cycle_pulse_width;
	logic [7:0] end_of_frame;
} packet_from_pc_t;
 
packet_from_pc_t receiving_packet;
localparam packet_from_pc_t packet_pattern = '{ preamble_frame_delimiter:8'hFD,
																mdr_phase: 8'h0,
																mdr_att_rx: 8'h0,
																mdr_att_tx:8'h0,																
																mtx_duty_cycle_pulse_width: 24'h0,
																end_of_frame: 8'hDF };

 
ModTranceiver 
#(.DATA_BITS(8),
  .PARITY_MODE("NONE"),
  .STOP_BITS(1),
  .DATA_DIR("LSB"),
  .BAUD_DIVIDER(INT_BAUD_DIVIDER),
  .REAL_DIVIDER(REAL_BAUD_DIVIDER)) 
 
uart_module 
(.CLK(clk), 
 .RSTn(rstn),
 .RX(rx),
 .TDi(data_to_transmitter),
 .TRDYi(start_transmission),
 .RRDYi(master_got_data),		
 .TX(tx),
 .TE(transmitter_is_busy),
 .TRDYo(transmitter_loaded_data),
 .RDo(data_from_receiver),
 .RRDYo(receiver_data_is_ready),
 .PE(parity_error),
 .FE(frame_error));
    

logic event_flag_catch0, event_flag_catch1;	 
logic event_flag_goes_up;
assign event_flag_goes_up = (event_flag_catch0 && ~event_flag_catch1);
	 
always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		event_flag_catch0 <= 0;
		event_flag_catch1 <= 0;
	end
	else
	begin
		event_flag_catch0 <= event_flag;
		event_flag_catch1 <= event_flag_catch0;
	end
end	 

	 
enum {enUART_IDLE, enDECIDING_WHAT_PART_OF_FRAME_IS_READING, enREADING_PREAMBLE, enREADING_FRAME, enREADING_END_OF_FRAME, enWAITING_FOR_EVENT, enUART_SENDING, enUART_ROLLING_NEXT_BYTE} transceiver_machine_state;
enum {enMDR_PHASE_BYTE, enMDR_ATT_RX_BYTE, enMDR_ATT_TX_BYTE, enMTX_DUTY_CYCLE_PULSE_WIDTH_0, enMTX_DUTY_CYCLE_PULSE_WIDTH_1, enMTX_DUTY_CYCLE_PULSE_WIDTH_2} frame_counter; 

logic reading_preamble, reading_frame;
 
always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		ResetMainCycle();
	end
	else 
	begin
	
		case(transceiver_machine_state)
		
		
		enUART_IDLE:
		begin
			new_data_strobe <= 0;
			if(receiver_data_is_ready) 
			begin
				$display($time, " wrapper sees some data from receiver!");
				master_got_data <= 1;
				if(~parity_error && ~frame_error)
				begin
					$display($time, " data is correct=%h", data_from_receiver);
					rx_data_buffer <= data_from_receiver;												
					transceiver_machine_state <= enDECIDING_WHAT_PART_OF_FRAME_IS_READING;
				end
				else 
				begin //package is broken. Lets start over!
					reading_preamble <= 1;
					reading_frame <= 0;
					receiving_packet.preamble_frame_delimiter <= 0;
					$display($time," data is WRONG!");
				end	
			end
			else
			begin
				start_transmission <= 0;
				master_got_data <= 0;
				data_to_transmitter <= '0;
			end
		end
	
	
		enDECIDING_WHAT_PART_OF_FRAME_IS_READING:
		begin
			if(~receiver_data_is_ready)
			begin
				master_got_data <= 0;
				if(reading_preamble) transceiver_machine_state <= enREADING_PREAMBLE;
				else if(reading_frame) transceiver_machine_state <= enREADING_FRAME;
					  else transceiver_machine_state <= enREADING_END_OF_FRAME;				
			end
		end
		
		
		enREADING_PREAMBLE:
		begin
			if(rx_data_buffer == packet_pattern.preamble_frame_delimiter)
			begin
				$display("Preambula is correct!");
				reading_preamble <= 0;
				reading_frame <= 1;
				receiving_packet.preamble_frame_delimiter <= 0;
			end
			transceiver_machine_state <= enUART_IDLE;			
		end
		
		
		enREADING_FRAME:
		begin
			$display("Reading frame...counter=%s", frame_counter, "rx_data_buffer=%h", rx_data_buffer);
		
			case(frame_counter)
			
			enMDR_PHASE_BYTE: 							begin receiving_packet.mdr_phase   		  		  				<= rx_data_buffer; frame_counter <= enMDR_ATT_RX_BYTE;   				     end
			enMDR_ATT_RX_BYTE:							begin receiving_packet.mdr_att_rx 		  		  				<= rx_data_buffer; frame_counter <= enMDR_ATT_TX_BYTE;   				     end
			enMDR_ATT_TX_BYTE:							begin receiving_packet.mdr_att_tx  		  		  				<= rx_data_buffer; frame_counter <= enMTX_DUTY_CYCLE_PULSE_WIDTH_0; 	 	  end
			enMTX_DUTY_CYCLE_PULSE_WIDTH_0: 			begin receiving_packet.mtx_duty_cycle_pulse_width[23:16]	<= rx_data_buffer; frame_counter <= enMTX_DUTY_CYCLE_PULSE_WIDTH_1;   	  end
			enMTX_DUTY_CYCLE_PULSE_WIDTH_1:			begin receiving_packet.mtx_duty_cycle_pulse_width[15:8] 	<= rx_data_buffer; frame_counter <= enMTX_DUTY_CYCLE_PULSE_WIDTH_2;       end
			enMTX_DUTY_CYCLE_PULSE_WIDTH_2: 			begin receiving_packet.mtx_duty_cycle_pulse_width[7:0]  	<= rx_data_buffer; frame_counter <= enMDR_PHASE_BYTE; reading_frame <= 0; end
			
			default:;
			
			endcase
			
			transceiver_machine_state <= enUART_IDLE;
		end
		
		
		enREADING_END_OF_FRAME:
		begin
			if(packet_pattern.end_of_frame == rx_data_buffer)
			begin
				$display("Got correct packet! Putting strobe");
				data_from_packet.mdr_phase							<= receiving_packet.mdr_phase;
				data_from_packet.mdr_att_rx						<= receiving_packet.mdr_att_rx;
				data_from_packet.mdr_att_tx						<= receiving_packet.mdr_att_tx;
				data_from_packet.mtx_duty_cycle_pulse_width	<= receiving_packet.mtx_duty_cycle_pulse_width;
				new_data_strobe <= 1;				
				transceiver_machine_state <= enWAITING_FOR_EVENT;
			end
			else
			begin
				$display("This isn't end of packet! Clearing ;(");
				transceiver_machine_state <= enUART_IDLE;
			end
			reading_preamble <= 1; //Waiting for next package
		end
		
		
		enWAITING_FOR_EVENT:
		begin
			if(event_flag_goes_up) 
			begin
				busy <= 1;
				SplitVector(data_vector, data_vector_buffer); //copy data to special vector 8 elements by 8
				transceiver_machine_state <= enUART_SENDING;
			end	
		end
	
	
		enUART_SENDING:
		begin			
			if(start_transmission) //already sending. Came from section below
			begin
				start_transmission <= 0;
				transceiver_machine_state <= enUART_ROLLING_NEXT_BYTE;
			end
			else
			begin
				if((~transmitter_is_busy) & transmitter_loaded_data)
				begin: transmitter_rdy
					data_to_transmitter <= data_vector_buffer[BYTES_QTY_TO_SEND - 1 - sended_bytes_counter][7:0];
					start_transmission <= 1;													
				end: transmitter_rdy
			end
		end
		
		
		enUART_ROLLING_NEXT_BYTE:
		begin
			if(sended_bytes_counter < BYTES_QTY_TO_SEND - 1)
			begin
				sended_bytes_counter <= sended_bytes_counter + 1;
				transceiver_machine_state <= enUART_SENDING;
			end
			else
			begin
				busy <= 0;
				sended_bytes_counter <= 0;
				transceiver_machine_state <= enUART_IDLE;
			end
		end
		
		default: transceiver_machine_state <= enUART_IDLE;
		
		endcase
		
	end
end

initial
begin
	ResetMainCycle();
	event_flag_catch0 = 0;
	event_flag_catch1 = 0;
end


function automatic void ResetMainCycle;
begin
	new_data_strobe = 0;
	sended_bytes_counter = 0;
	transceiver_machine_state = enUART_IDLE;
	rx_data_buffer = 0;
	data_vector_buffer = 0;
	start_transmission = 0;
	master_got_data = 0;
	busy = 0;	
	frame_counter = enMDR_PHASE_BYTE;
	reading_preamble = 1;
	reading_frame = 0;
end
endfunction

function automatic void SplitVector
(input logic [INPUT_BUS_WIDTH-1:0] input_data_vector, output logic [BYTES_QTY_TO_SEND-1:0][7:0] output_data_vector);
begin
	for(int i = 0; i < BYTES_QTY_TO_SEND; i = i + 1)
	begin
		output_data_vector [i] = input_data_vector[BYTE_LENGTH*i +:BYTE_LENGTH];
	end
end
endfunction 


endmodule: slave_packet_transceiver

//*********************************************************************
//*********************************************************************
//*********************************************************************

/*
Simple module which sends 64 bit input data through uart when received command. Dependencies: ModTranceiver

*/
module one_way_x_bit_transceiver_with_rx_control
#(parameter INPUT_BUS_WIDTH = 64,
  parameter BAUDRATE = 2_000_000,
  parameter CLK_RATE = 100_000_000)
(input  logic clk,
 input  logic rstn,
 
 input  logic [INPUT_BUS_WIDTH-1:0] data_vector,
 
 output logic [7:0] data_from_uart,
 output logic new_data_strobe,
 output logic busy,
 output logic led,
 
 output logic tx,
 input  logic rx,
 
 input  logic event_flag);
 
 
localparam BYTE_LENGTH = 8; 
localparam BYTES_QTY_TO_SEND =  INPUT_BUS_WIDTH / BYTE_LENGTH;

localparam integer INT_BAUD_DIVIDER = CLK_RATE / (3*BAUDRATE);
localparam real REAL_BAUD_DIVIDER = CLK_RATE / BAUDRATE;
 
logic transmitter_is_busy, transmitter_loaded_data, receiver_data_is_ready, parity_error, frame_error; //inputs
logic start_transmission, master_got_data; //outputs

logic [7:0] data_to_transmitter, data_from_receiver, rx_data_buffer;
logic [BYTES_QTY_TO_SEND-1:0][7:0] data_vector_buffer;
logic [($clog2(BYTES_QTY_TO_SEND))-1:0] sended_bytes_counter;

 
 ModTranceiver 
 #(.DATA_BITS(8),
   .PARITY_MODE("NONE"),
   .STOP_BITS(1),
   .DATA_DIR("LSB"),
   .BAUD_DIVIDER(INT_BAUD_DIVIDER),
   .REAL_DIVIDER(REAL_BAUD_DIVIDER)) 
 
 uart_module 
 (.CLK(clk), 
  .RSTn(rstn),
  .RX(rx),
  .TDi(data_to_transmitter),
  .TRDYi(start_transmission),
  .RRDYi(master_got_data),		
  .TX(tx),
  .TE(transmitter_is_busy),
  .TRDYo(transmitter_loaded_data),
  .RDo(data_from_receiver),
  .RRDYo(receiver_data_is_ready),
  .PE(parity_error),
  .FE(frame_error));
    

logic event_flag_catch0, event_flag_catch1;	 
logic event_flag_goes_up;
assign event_flag_goes_up = (event_flag_catch0 && ~event_flag_catch1);
	 
always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		event_flag_catch0 <= 0;
		event_flag_catch1 <= 0;
	end
	else
	begin
		event_flag_catch0 <= event_flag;
		event_flag_catch1 <= event_flag_catch0;
	end
end	 

	 
enum {enUART_IDLE, enUART_READING, enWAITING_FOR_EVENT, enUART_SENDING, enUART_ROLLING_NEXT_BYTE} transceiver_machine_state;
 

always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		ResetMainCycle();
	end
	else 
	begin
	
		case(transceiver_machine_state)
		
		
		enUART_IDLE:
		begin
			new_data_strobe <= 0;
			if(receiver_data_is_ready) 
			begin
				$display($time, " wrapper sees some data from receiver!");
				master_got_data <= 1;
				if(~parity_error && ~frame_error)
				begin
					$display($time, " data is correct!");
					rx_data_buffer <= data_from_receiver;												
					transceiver_machine_state <= enUART_READING;
				end
				else $display($time," data is WRONG!");
			end
			else
			begin
				start_transmission <= 0;
				master_got_data <= 0;
				data_to_transmitter <= '0;
			end
		end
	
	
		enUART_READING:
		begin
			if(~receiver_data_is_ready)
			begin
				led <= ~led;
				master_got_data <= 0;			
				new_data_strobe <= 1;
				data_from_uart <= rx_data_buffer;
				transceiver_machine_state <= enWAITING_FOR_EVENT;
			end
		end
		
		
		enWAITING_FOR_EVENT:
		begin
			if(event_flag_goes_up) 
			begin
				busy <= 1;
				SplitVector(data_vector, data_vector_buffer); //copy data to special vector 8 elements by 8
				transceiver_machine_state <= enUART_SENDING;
			end	
		end
	
	
		enUART_SENDING:
		begin			
			if(start_transmission) //already sending. Came from section below
			begin
				start_transmission <= 0;
				transceiver_machine_state <= enUART_ROLLING_NEXT_BYTE;
			end
			else
			begin
				if((~transmitter_is_busy) & transmitter_loaded_data)
				begin: transmitter_rdy
					data_to_transmitter <= data_vector_buffer[BYTES_QTY_TO_SEND - 1 - sended_bytes_counter][7:0];
					start_transmission <= 1;													
				end: transmitter_rdy
			end
		end
		
		
		enUART_ROLLING_NEXT_BYTE:
		begin
			if(sended_bytes_counter < BYTES_QTY_TO_SEND - 1)
			begin
				sended_bytes_counter <= sended_bytes_counter + 1;
				transceiver_machine_state <= enUART_SENDING;
			end
			else
			begin
				busy <= 0;
				sended_bytes_counter <= 0;
				transceiver_machine_state <= enUART_IDLE;
			end
		end
		
		default: transceiver_machine_state <= enUART_IDLE;
		
		endcase
		
	end
end

initial
begin
	ResetMainCycle();
	event_flag_catch0 = 0;
	event_flag_catch1 = 0;
end


function automatic void ResetMainCycle;
begin
	new_data_strobe = 0;
	sended_bytes_counter = 0;
	transceiver_machine_state = enUART_IDLE;
	rx_data_buffer = 0;
	data_vector_buffer = 0;
	start_transmission = 0;
	master_got_data = 0;
	busy = 0;
end
endfunction

function automatic void SplitVector
(input logic [INPUT_BUS_WIDTH-1:0] input_data_vector, output logic [BYTES_QTY_TO_SEND-1:0][7:0] output_data_vector);
begin
	for(int i = 0; i < BYTES_QTY_TO_SEND; i = i + 1)
	begin
		output_data_vector [i] = input_data_vector[BYTE_LENGTH*i +:BYTE_LENGTH];
	end
end
endfunction 


endmodule: one_way_x_bit_transceiver_with_rx_control

//*********************************************************************
//*********************************************************************
//*********************************************************************


module one_way_64_bit_transceiver_with_strobe_control
#(parameter INPUT_BUS_WIDTH = 64,
  parameter BAUDRATE = 2_000_000,
  parameter CLK_RATE = 100_000_000)
(input  logic clk,
 input  logic rstn,
 
 input  logic [INPUT_BUS_WIDTH-1:0] data_vector,
 output logic tx,
 input  logic event_flag,
 output logic busy);
 
 
localparam BYTE_LENGTH = 8; 
localparam BYTES_QTY_TO_SEND =  INPUT_BUS_WIDTH / BYTE_LENGTH;

localparam integer INT_BAUD_DIVIDER = CLK_RATE / (3*BAUDRATE);
localparam real REAL_BAUD_DIVIDER = CLK_RATE / BAUDRATE;
 
logic transmitter_is_busy, transmitter_loaded_data; //inputs
logic start_transmission; //outputs

logic [7:0] data_to_transmitter;
logic [BYTES_QTY_TO_SEND-1:0][7:0] data_vector_buffer;
logic [($clog2(BYTES_QTY_TO_SEND))-1:0] sended_bytes_counter;

 
 ModTranceiver 
 #(.DATA_BITS(8),
   .PARITY_MODE("NONE"),
   .STOP_BITS(1),
   .DATA_DIR("LSB"),
   .BAUD_DIVIDER(INT_BAUD_DIVIDER),
   .REAL_DIVIDER(REAL_BAUD_DIVIDER)) 
 
 uart_module 
 (.CLK(clk), 
  .RSTn(rstn),
  .RX(),
  .TDi(data_to_transmitter),
  .TRDYi(start_transmission),
  .RRDYi(),		
  .TX(tx),
  .TE(transmitter_is_busy),
  .TRDYo(transmitter_loaded_data),
  .RDo(),
  .RRDYo(),
  .PE(),
  .FE());
    

logic event_flag_catch0, event_flag_catch1;	 
logic event_flag_goes_up;
assign event_flag_goes_up = (event_flag_catch0 && ~event_flag_catch1);
	 
always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		event_flag_catch0 <= 0;
		event_flag_catch1 <= 0;
	end
	else
	begin
		event_flag_catch0 <= event_flag;
		event_flag_catch1 <= event_flag_catch0;
	end
end	 

	 
enum {enWAITING_FOR_EVENT, enUART_SENDING, enUART_ROLLING_NEXT_BYTE} transceiver_machine_state;
 

always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		ResetMainCycle();
	end
	else 
	begin
	
		case(transceiver_machine_state)
		
					
		enWAITING_FOR_EVENT:
		begin
			if(event_flag_goes_up) 
			begin
				busy <= 1;
				SplitVector(data_vector, data_vector_buffer); //copy data to special vector 8 elements by 8
				transceiver_machine_state <= enUART_SENDING;
			end
			else
			begin
				busy <= 0;
				start_transmission <= 0;
				data_to_transmitter <= '0;
			end
		end
	
	
		enUART_SENDING:
		begin			
			if(start_transmission) //already sending. Came from section below
			begin
				start_transmission <= 0;
				transceiver_machine_state <= enUART_ROLLING_NEXT_BYTE;
			end
			else
			begin
				if((~transmitter_is_busy) & transmitter_loaded_data)
				begin: transmitter_rdy
					data_to_transmitter <= data_vector_buffer[BYTES_QTY_TO_SEND - 1 - sended_bytes_counter][7:0];
					start_transmission <= 1;													
				end: transmitter_rdy
			end
		end
		
		
		enUART_ROLLING_NEXT_BYTE:
		begin
			if(sended_bytes_counter < BYTES_QTY_TO_SEND - 1)
			begin
				sended_bytes_counter <= sended_bytes_counter + 1;
				transceiver_machine_state <= enUART_SENDING;
			end
			else
			begin
				sended_bytes_counter <= 0;
				transceiver_machine_state <= enWAITING_FOR_EVENT;
			end
		end
		
		default: transceiver_machine_state <= enWAITING_FOR_EVENT;
		
		endcase
		
	end
end

initial
begin
	ResetMainCycle();
	event_flag_catch0 = 0;
	event_flag_catch1 = 0;
end


function automatic void ResetMainCycle;
begin
	sended_bytes_counter = 0;
	transceiver_machine_state = enWAITING_FOR_EVENT;
	data_vector_buffer = 0;
	start_transmission = 0;
	busy = 0;
end
endfunction

function automatic void SplitVector
(input logic [INPUT_BUS_WIDTH-1:0] input_data_vector, output logic [BYTES_QTY_TO_SEND-1:0][7:0] output_data_vector);
begin
	for(int i = 0; i < BYTES_QTY_TO_SEND; i = i + 1)
	begin
		output_data_vector [i] = input_data_vector[BYTE_LENGTH*i +:BYTE_LENGTH];
	end
end
endfunction 


endmodule: one_way_64_bit_transceiver_with_strobe_control