`ifndef CONNECTIONS
`define CONNECTIONS

package connections;

typedef struct packed
{
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx;
	logic [23:0] mtx_duty_cycle_pulse_width;
} uart_wrapper_connection_t;

endpackage



package common_values;

localparam [9:0] DUTY_CYCLE_MAX_VALUE = 1000;
localparam [7:0] PULSE_WIDTH_MAX_VALUE_US = 255;

endpackage

`endif //CONNECTIONS 