library ieee;
use ieee.std_logic_1164.all;

package ak_types is

	-- Короткие псевдонимы векторов стандартных размеров
	subtype pair is std_logic_vector ( 1 downto 0 );
	subtype nibble is std_logic_vector ( 3 downto 0 );
	subtype byte is std_logic_vector ( 7 downto 0 );
	subtype word is std_logic_vector ( 15 downto 0 );
	subtype dword is std_logic_vector ( 31 downto 0 );
	
	type int_array is array (integer range <>) of integer;
	type real_array is array (integer range <>) of real;
	type pair_array is array (integer range <>) of pair;
	type nibble_array is array (integer range <>) of nibble;
	type byte_array is array (integer range <>) of byte;
	type word_array is array (integer range <>) of word;
	type dword_array is array (integer range <>) of dword;
	
-- Операции преобразования типов
	pure function to_integer(	-- Converts v to integer
		v : std_logic_vector;	-- Source vector
		s : boolean := FALSE )	-- TRUE if signed
		return integer;
		
	pure function to_vector(	-- Converts i to std_logic_vector
		i : integer;			-- Source integer
		s : integer )			-- Vector size
		return std_logic_vector;
		
	pure function to_vector(	-- Converts i to std_logic_vector
		i : int_array;			-- Source integer array
		s : integer )			-- Component size
		return std_logic_vector;	-- size = s*(i'length)
		
	pure function to_byte(	-- Converts i to std_logic_vector( 7 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector;
		
	pure function to_word(	-- Converts i to std_logic_vector( 15 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector;
		
	pure function to_dword(	-- Converts i to std_logic_vector( 31 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector;
		
-- Дифференциальная пара
	type diff_logic is record
			P, N : std_logic;
		end record;
	type diff_logic_vector is array ( integer range <> ) of diff_logic;
	
-- Тактовый домен
	type clock_domain is record
			CLK,
			RSTn,
			EN : std_logic;
		end record;
	pure function to_clock_domain( c : std_logic ) return clock_domain;
	pure function to_clock_domain( c, r : std_logic ) return clock_domain;
	pure function to_clock_domain( c, r, e : std_logic ) return clock_domain;
	constant clock_domain_init : clock_domain := ( CLK=>'0', RSTn=>'0', EN=>'1' );
	
	pure function cd_rise( signal ck : clock_domain ) return boolean;
	pure function cd_fall( signal ck : clock_domain ) return boolean;
	pure function cd_reset( signal ck : clock_domain ) return boolean;

	-- process (CK)	-- Шаблон процесса
	-- begin
		-- if CK.RSTn='0' then
		-- elsif rising_edge(CK.CLK) and CK.EN='1' then
		-- end if;
	-- end process;
	
	-- 2-phase clock domain record
	type clock_domain_2ph is record
			CLK,
			CLK90,
			RSTn,
			EN : std_logic;
		end record;
	constant clock_domain_2ph_init : clock_domain_2ph := ( CLK=>'0', CLK90=>'0', RSTn=>'0', EN=>'1' );

-- Функции сравнения
	pure function match (
		a : std_logic_vector;
		b : std_logic_vector ) return std_logic;
	pure function match (
		a : std_logic_vector;
		b : integer ) return std_logic;
	pure function match (
		a : integer;
		b : integer ) return std_logic;
		
-- Битовые операции
	pure function parity( d : std_logic_vector ) return std_logic;
	pure function msb( v : std_logic_vector ) return integer;
	pure function msb( x : integer ) return integer;
	pure function priority( vi : std_logic_vector; h : boolean ) return std_logic_vector;
	pure function set( x : integer ) return integer;
	pure function demux( x : std_logic_vector ) return std_logic_vector;
	pure function setmsb( len : integer ) return std_logic_vector;
	pure function setmsb( ref : std_logic_vector ) return std_logic_vector;
	pure function setlsb( len : integer ) return std_logic_vector;
	pure function setlsb( ref : std_logic_vector ) return std_logic_vector;
	pure function zeros( len : integer ) return std_logic_vector;
	pure function zeros( ref : std_logic_vector ) return std_logic_vector;
	pure function ones( len : integer ) return std_logic_vector;
	pure function ones( ref : std_logic_vector ) return std_logic_vector;
	pure function bin2gray( vi : std_logic_vector ) return std_logic_vector;
	pure function gray2bin( vi : std_logic_vector ) return std_logic_vector;
	
-- Логические (тернарные) операции
	pure function selector( c : boolean; t, f : integer ) return integer;
	pure function selector( c : boolean; t, f : real ) return real;
	pure function selector( c : boolean; t, f : std_logic ) return std_logic;
	pure function selector( c : boolean; t, f : std_logic_vector ) return std_logic_vector;
	
-- Векторные функции
	pure function lowbyte( x : std_logic_vector ) return std_logic_vector;
	pure function highbyte( x : std_logic_vector ) return std_logic_vector;
	pure function lowbyte( x : integer ) return std_logic_vector;
	pure function highbyte( x : integer ) return std_logic_vector;
	pure function signext( v : std_logic_vector; i : integer ) return std_logic_vector;
	pure function zeroext( v : std_logic_vector; i : integer ) return std_logic_vector;
	pure function scale( v : std_logic_vector; i : integer ) return std_logic_vector;
	pure function reverse( v : std_logic_vector ) return std_logic_vector;
	pure function reorder( vi : std_logic_vector; ord : string ) return std_logic_vector;
	pure function mixbits( e, o : std_logic_vector ) return std_logic_vector;
	procedure shiftleft( signal v : inout std_logic_vector; l : std_logic );
	procedure shiftright( signal v : inout std_logic_vector; r : std_logic );
	pure function rotate( iv : std_logic_vector; val : integer ) return std_logic_vector;
	pure function round( v : std_logic_vector; d : integer ) return std_logic_vector;
	pure function lower( va, vb : std_logic_vector; s : boolean := FALSE ) return std_logic;
	pure function lower( va : std_logic_vector; i : integer; s : boolean := FALSE ) return std_logic;
	pure function greater( va, vb : std_logic_vector; s : boolean := FALSE ) return std_logic;
	pure function greater( va : std_logic_vector; i : integer; s : boolean := FALSE ) return std_logic;
	
-- Целочисленные функции
	pure function maximum ( a : integer; b : integer ) return integer;
	pure function minimum ( a : integer; b : integer ) return integer;
	pure function maximum ( a : real; b : real ) return real;
	pure function minimum ( a : real; b : real ) return real;
	pure function bestdiv ( src : integer; dst : integer ) return integer;
	pure function count_top( v : std_logic_vector; t : integer; r : integer := 0 ) return std_logic;

-- Элементарные операции для CRC и скремблирования
	pure function nextCRC( prev : std_logic_vector; sdi : std_logic; poly : std_logic_vector ) return std_logic_vector;
	pure function nextCRC( prev : std_logic_vector; sdi : std_logic_vector; poly : std_logic_vector ) return std_logic_vector;
	pure function magicCRC( poly : std_logic_vector ) return std_logic_vector;
	pure function nextScramState( prev : std_logic_vector; sdi : std_logic; poly : std_logic_vector ) return std_logic_vector;
	pure function nextScramState( prev : std_logic_vector; sdi : std_logic_vector; poly : std_logic_vector ) return std_logic_vector;
	pure function nextScramData( prev : std_logic_vector; sdi : std_logic; poly : std_logic_vector ) return std_logic;
	pure function nextScramData( prev : std_logic_vector; sdi : std_logic_vector; poly : std_logic_vector ) return std_logic_vector;
	pure function nextDescramState( prev : std_logic_vector; sdi : std_logic; poly : std_logic_vector ) return std_logic_vector;
	pure function nextDescramState( prev : std_logic_vector; sdi : std_logic_vector; poly : std_logic_vector ) return std_logic_vector;
	pure function nextDescramData( prev : std_logic_vector; sdi : std_logic; poly : std_logic_vector ) return std_logic;
	pure function nextDescramData( prev : std_logic_vector; sdi : std_logic_vector; poly : std_logic_vector ) return std_logic_vector;
	
	procedure inc ( signal v : inout std_logic_vector );
	procedure dec ( signal v : inout std_logic_vector );
	procedure inc ( signal i : inout integer );
	procedure dec ( signal i : inout integer );
	
-- Логические операторы над векторами
	pure function "or" ( l : std_logic_vector; r : std_logic ) return std_logic_vector;
	pure function "or" ( l : std_logic; r : std_logic_vector ) return std_logic_vector;
	pure function "and" ( l : std_logic_vector; r : std_logic ) return std_logic_vector;
	pure function "and" ( l : std_logic; r : std_logic_vector ) return std_logic_vector;
	pure function "xor" ( l : std_logic_vector; r : std_logic ) return std_logic_vector;
	pure function "xor" ( l : std_logic; r : std_logic_vector ) return std_logic_vector;
	pure function "ror" ( l : std_logic_vector; r : integer ) return std_logic_vector;
	pure function "rol" ( l : std_logic_vector; r : integer ) return std_logic_vector;
	pure function to_01( i : boolean ) return std_logic;
	
-- Текстовые функции
	pure function string32h( i : std_logic_vector ) return string;
	pure function string16h( i : std_logic_vector ) return string;
	pure function string8h( i : std_logic_vector ) return string;
	pure function string32h( i : integer ) return string;
	pure function string16h( i : integer ) return string;
	pure function string8h( i : integer ) return string;
	pure function bin_string( s : std_logic_vector ) return string;
	pure function bin_string( s : integer; len : integer ) return string;
	
	pure function char2hex( c : character ) return integer;
	pure function ascii( ai : character ) return integer;
	pure function ascii( ai : integer ) return character;
	pure function ascii2nibble ( av : byte ) return nibble;
	pure function nibble2ascii ( av : nibble ) return byte;
	pure function is_ascii_digit( av : byte ) return std_logic;

-- Компоненты

	-- Синхронизатор сброса
	component ResetSynchronizer is
		port (
			CLK		: in std_logic;			-- Global input clock
			RSTIn	: in std_logic := '1';	-- Global async RESET input (negative)
			RSTOn	: out std_logic );		-- Global sync RESET output (negative)
	end component;

	-- Переключатель тактовых сигналов
	component ClockSwitch is
		port (
			SEL, CLKA, CLKB : in std_logic;
			CLKOUT : out std_logic
			);
	end component;
		
	-- Драйвер индикации - удлиннитель импульсов
	component IndicatorDriver is
		generic (
			DATA_WIDTH		: integer := 3;			-- IO ports count
			CLK_DIVIDER		: integer := 5;			-- Data sampling divider
			SUB_DIVIDER		: integer := 4;			-- Minimum LED ON time divider
			INVERSION		: boolean := FALSE);	-- Output inversion
		port (
			CLK		: in std_logic;			-- Global clock
			RSTn	: in std_logic := '1';	-- Async. reset
			D	: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');	-- Pulse inputs (1)
			Q	: out std_logic_vector( DATA_WIDTH-1 downto 0 );					-- Pulse outputs (1)
			RO	: out std_logic );			-- Main divider output
	end component;
	
	-- Драйвер мультиплексной светодиодной индикации n*(n-1)
	component LEDMultiplexorNN1 is
		generic (
			MUX_FACT	: integer := 3;		-- Multiplexion factor, driver count
			LED_CFG		: string := "-";	-- Led order, like "012345" or "103254"
			COM_CATH	: boolean := FALSE;	-- TRUE = Common cathode, FALSE = anode
			DIVIDER		: integer := 127 );	-- Multiplexing frequency divider
		port (
			CLK		: in std_logic;			-- Global input clock
			RSTn	: in std_logic := '1';	-- Global async RESET (negative)
			
			D : in std_logic_vector( ((MUX_FACT-1)*MUX_FACT)-1 downto 0 );	-- Led inputs, non-inverting
			Q : out std_logic_vector( MUX_FACT-1 downto 0 ) );	-- Led drivers, 01Z
	end component;

	-- Целочисленный делитель частоты
	component ClockDivider is
		generic (
			CLKDIV	: integer := 1 );		-- Clock divider
		port (
			CLKI	: in std_logic;			-- Global input clock
			RSTn	: in std_logic := '1';	-- Global async RESET (negative)
			
			DIVO	: out std_logic;		-- Enable output pulse (CLKI/CLKDIV) = RISE or FALL
			CLKO	: out std_logic;		-- 50/50 clock output (CLKI/CLKDIV/2)
			RISE	: out std_logic;		-- CLKO rising edge pulse (CLKI/CLKDIV/2)
			FALL	: out std_logic );		-- CLKO falling edge pulse (CLKI/CLKDIV/2)
	end component;

	-- Дробный делитель частоты
	component FractClockDivider is
		generic (
			DIVIDER : real := 3.0;		-- Divider for Q output
			PRECESION : real := 100.0 );-- Division precesion
		port (
			CLK : in std_logic;			-- Main clock
			RSTn : in std_logic := '1';	-- Async. reset
			EN : in std_logic := '1';	-- Output & divide enable
			Q : out std_logic;			-- Output 1CLK pulses
			F2 : out std_logic );		-- Q/2 frequency
	end component;

	-- Междоменный синхронизатор данных 
	component DataSync is
		generic (
			REGW : integer := 4;					-- Register data width
			SYNCEN : boolean := TRUE );				-- Enable/disable synchronization
		port (
			DI : in std_logic_vector( REGW-1 downto 0 ) := (others=>'0');	-- Reg input
			DO : out std_logic_vector( REGW-1 downto 0 );					-- Reg output
			RSTIn : in std_logic := '1';			-- Async. reset input
			RSTOn : out std_logic;					-- Syncronized reset output
			EI : in std_logic := '1';				-- Input data enable
			CLKI, CLKO : in std_logic := '0' );		-- Reference clocks
	end component;

	-- Фиксированная линия задержки	
	component DelayLine is
		generic (
			LATENCY : integer := 3;		-- Delay value
			WIDTH : integer := 8 );		-- Data width
		port (
			D : in std_logic_vector( WIDTH-1 downto 0 ):= (others=>'0');	-- Input data
			Q : out std_logic_vector( WIDTH-1 downto 0 );					-- Output data
			CLK : in std_logic;				-- Global clock
			RSTn : in std_logic := '1' );	-- Async. reset, negative
	end component;

	component MAX6577 is
		generic (
			DATA_WIDTH : integer := 14;							-- Pulse counter data width
			CLK_DIV : integer := 20000000/8 );					-- Counting reset divider
		port (
			CLK : in std_logic;									-- Main clock
			RSTn : in std_logic := '1';							-- Async. reset
			DI : in std_logic;									-- Pulse input
			DO : out std_logic_vector( DATA_WIDTH-1 downto 0 );	-- Counter output
			OVF,												-- Counter overflow flag
			NEU,												-- New data pulse
			TOG : out std_logic );								-- New data toggle
	end component;

	component SimplePriority is
		generic (
			CODE_LENGTH : integer := 4;		-- Output code size
			HIGH_PRIOR : boolean := TRUE );	-- High/low priority selection
		port (
			CLK		: in std_logic;				-- Global clock
			RSTn	: in std_logic := '1';		-- Async. reset, negative
			
			SRC		: in std_logic_vector( (2**CODE_LENGTH)-1 downto 0 );	-- Source vector
			CODE	: out std_logic_vector( CODE_LENGTH-1 downto 0 );		-- Output priority code
			FLAG	: out std_logic );			-- Signal presence flag
	end component;

	component TokenArbiter is
		generic (
			TOKEN_START : boolean := FALSE );	-- Controller reset state
		port (
			CK			: in clock_domain;		-- Synchronization
			REQ			: in std_logic := '0';	-- Arbitration request
			ACK			: out std_logic;		-- Arbitration grant
			ChainIn		: in std_logic := '1';	-- Chain input
			ChainOut	: out std_logic );		-- Chain output
	end component;

	-- Многоразрядный сигма-дельта-ЦАП
	component ak_dac is
		generic (
			R_COUNT : integer := 1;			-- Кол-во выходных разрядов
			DATA_WIDTH : integer := 8;			-- Разрядность кода
			DATA_SIGN : boolean := FALSE );		-- Знаковый формат
		port (
			CK : in clock_domain := CLOCK_DOMAIN_INIT;
			D : in std_logic_vector( DATA_WIDTH-1 downto 0 );
			Q : out std_logic_vector( R_COUNT-1 downto 0 ) );
	end component;

	-- Простой генератор импульсов
	component ak_pulse is
		generic (	
			LOW_TIME : integer := 7;
			HIGH_TIME : integer := 3 );
		port (
			CK : in clock_domain := CLOCK_DOMAIN_INIT;
			Q : out std_logic;
			H : out std_logic;
			L : out std_logic );
	end component;

end package;

--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

package body ak_types is

-- Conversion operations	
	pure function to_integer(	-- Converts v to integer
		v : std_logic_vector;	-- Source vector
		s : boolean := FALSE )	-- TRUE if signed
		return integer is
		
		begin
			if is_x(v) then
				return 0;
			elsif s and (v(v'high)='1' or v(v'high)='H') then
				return conv_integer( v )-(2**v'length);
			else
				return conv_integer( v );
			end if;
		end function;
		
	pure function to_vector(	-- Converts i to std_logic_vector
		i : integer;			-- Source integer
		s : integer )			-- Vector size
		return std_logic_vector is
		
		begin
			return conv_std_logic_vector( i, s );
		end function;

	pure function to_vector(	-- Converts i to std_logic_vector
		i : int_array;			-- Source integer array
		s : integer )			-- Component size
		return std_logic_vector is	-- size = s*(i'length)
		
		variable ov : std_logic_vector( s * i'length - 1 downto 0 );
		begin
			for x in 0 to (s-1) loop
				ov( (x+1)*s-1 downto x*s ) := to_vector( i(x), s );
			end loop;
			return ov;
		end function;

	pure function to_byte(	-- Converts i to std_logic_vector( 7 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector is
		begin
			return to_vector( i, 8 );
		end function;
		
	pure function to_word(	-- Converts i to std_logic_vector( 15 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector is
		begin
			return to_vector( i, 16 );
		end function;
		
	pure function to_dword(	-- Converts i to std_logic_vector( 31 downto 0 )
		i : integer )			-- Source integer
		return std_logic_vector is
		begin
			return to_vector( i, 32 );
		end function;
		
	pure function selector( c : boolean; t, f : integer ) return integer is
		begin if c then return t; else return f; end if; end function;
		
	pure function selector( c : boolean; t, f : real ) return real is
		begin if c then return t; else return f; end if; end function;
		
	pure function selector( c : boolean; t, f : std_logic ) return std_logic is
		begin if c then return t; else return f; end if; end function;
		
	pure function selector( c : boolean; t, f : std_logic_vector ) return std_logic_vector is
		begin if c then return t; else return f; end if; end function;
		
	pure function to_01( i : boolean ) return std_logic is
		begin if i then return '1'; else return '0'; end if; end function;
		
	pure function to_clock_domain( c : std_logic ) return clock_domain is
		variable dom : clock_domain;
		begin
			dom.CLK := c;
			dom.RSTn := '1';
			dom.EN := '1';
			return dom;
		end function;
	
	pure function to_clock_domain( c, r : std_logic ) return clock_domain is
		variable dom : clock_domain;
		begin
			dom.CLK := c;
			dom.RSTn := r;
			dom.EN := '1';
			return dom;
		end function;
	
	pure function to_clock_domain( c, r, e : std_logic ) return clock_domain is
		variable dom : clock_domain;
		begin
			dom.CLK := c;
			dom.RSTn := r;
			dom.EN := e;
			return dom;
		end function;
	
	pure function cd_rise( signal ck : clock_domain ) return boolean is
		begin
		return ( rising_edge(ck.CLK) and (ck.EN='1') );
		end function;
		
	pure function cd_fall( signal ck : clock_domain ) return boolean is
		begin
		return ( falling_edge(ck.CLK) and (ck.EN='1') );
		end function;

	pure function cd_reset( signal ck : clock_domain ) return boolean is
		begin
		return ( ck.RSTn='0' );
		end function;

	procedure shiftleft( signal v : inout std_logic_vector; l : std_logic ) is
	begin
		v( v'low ) <= l;
		v( v'high downto v'low+1 ) <= v( v'high-1 downto v'low );
	end procedure;
	
	procedure shiftright( signal v : inout std_logic_vector; r : std_logic ) is
	begin
		v( v'high ) <= r;
		v( v'high-1 downto v'low ) <= v( v'high downto v'low+1 );
	end procedure;
	
	pure function rotate( iv : std_logic_vector; val : integer ) return std_logic_vector is
		variable ov : std_logic_vector(iv'range);
		variable j : integer;
	begin
		for i in iv'range loop
			j := ((i-iv'low+val) mod iv'length) + iv'low;
			ov(j) := iv(i);
		end loop;
		return ov;
	end function;
	
	pure function parity( d : std_logic_vector ) return std_logic is
	variable retv : std_logic;
	begin
		retv:='0';
		for i in d'range loop
			retv:= retv xor d(i);
		end loop;
		return retv;
	end function;
	
	pure function msb( v : std_logic_vector ) return integer is
	variable y : integer := -1;
	begin
		for i in v'range loop
			if v(i)='1' then
				y := i;
				exit;
			end if;
		end loop;
		return y;
	end function;

	pure function msb( x : integer ) return integer is
	variable v : std_logic_vector ( 31 downto 0 );
	begin
		v := conv_std_logic_vector( x, 32 );
		if x < 0 then
			v := not v;
		end if;
		return msb( v );
	end function;
	
	pure function count_top (
		v : std_logic_vector;	-- Input vector
		t : integer;			-- Comparison value
		r : integer := 0		-- Required
		) return std_logic is
		variable mask, top, req : std_logic_vector( v'range );
	begin
		top := to_vector( t, v'length );
		req := (others=>'0');
		req(r) := '1';
		req := req - 1;
		mask := v and (top or req);
		return match( mask, top );
	end function;
	
	pure function priority( vi : std_logic_vector; h : boolean ) return std_logic_vector is
		variable rv : std_logic_vector( msb(vi'length-1)+1 downto 0 );
		variable i : integer;
	begin
		rv(rv'high) := not match( vi, 0 );	-- Presence flag
		if h then	-- Search highest nonzero bit
			rv(rv'high-1 downto 0) := (others=>'0');
			for i in vi'high downto vi'low loop
				if vi(i)='1' then
					rv(rv'high-1 downto 0) := conv_std_logic_vector( i-vi'low, rv'length-1 );
					exit;
				end if;
			end loop;
		else	-- Search lowest nonzero bit
			rv(rv'high-1 downto 0) := (others=>'1');
			for i in vi'low to vi'high loop
				if vi(i)='1' then
					rv(rv'high-1 downto 0) := conv_std_logic_vector( i-vi'low, rv'length-1 );
					exit;
				end if;
			end loop;
		end if;
		return rv;
	end function;

	pure function set( x : integer ) return integer is
	begin
		return (2**(msb(x)+1))-1;
	end function;

	pure function setmsb( len : integer ) return std_logic_vector is
	variable rv : std_logic_vector( len-1 downto 0 );
	begin
		rv := (others=>'0');
		rv(rv'high) := '1';
		return rv;
	end function;
	
	pure function setmsb( ref : std_logic_vector ) return std_logic_vector is
	variable rv : std_logic_vector( ref'range );
	begin
		rv := (others=>'0');
		rv(rv'high) := '1';
		return rv;
	end function;

	pure function setlsb( len : integer ) return std_logic_vector is
	variable rv : std_logic_vector( len-1 downto 0 );
	begin
		rv := (others=>'0');
		rv(0) := '1';
		return rv;
	end function;
	
	pure function setlsb( ref : std_logic_vector ) return std_logic_vector is
	variable rv : std_logic_vector( ref'range );
	begin
		rv := (others=>'0');
		rv(rv'low) := '1';
		return rv;
	end function;
	
	pure function zeros( len : integer ) return std_logic_vector is
	variable rv : std_logic_vector( len-1 downto 0 );
	begin
		rv := (others=>'0');
		return rv;
	end function;
	
	pure function zeros( ref : std_logic_vector ) return std_logic_vector is
	variable rv : std_logic_vector( ref'range );
	begin
		rv := (others=>'0');
		return rv;
	end function;

	pure function ones( len : integer ) return std_logic_vector is
	variable rv : std_logic_vector( len-1 downto 0 );
	begin
		rv := (others=>'1');
		return rv;
	end function;
	
	pure function ones( ref : std_logic_vector ) return std_logic_vector is
	variable rv : std_logic_vector( ref'range );
	begin
		rv := (others=>'1');
		return rv;
	end function;
	
	pure function bin2gray( vi : std_logic_vector ) return std_logic_vector is
		variable vo : std_logic_vector(vi'range);
	begin
		if vi'length<2 then
			return vi;
		else
			vo(vo'high) := vi(vi'high);
			for i in vo'high-1 downto vo'low loop
				vo(i) := vi(i) xor vi(i+1);
			end loop;
			return vo;
		end if;
	end function;
	
	pure function gray2bin( vi : std_logic_vector ) return std_logic_vector is
		variable vo : std_logic_vector(vi'range);
	begin
		if vi'length<2 then
			return vi;
		else
			vo(vo'high) := vi(vi'high);
			for i in vo'high-1 downto vo'low loop
				vo(i) := vi(i) xor vo(i+1);
			end loop;
			return vo;
		end if;
	end function;
	
	pure function maximum (
		a : integer;
		b : integer ) return integer is
	begin
		if a>b then
			return a;
		else
			return b;
		end if;
	end function;
		
	pure function minimum (
		a : integer;
		b : integer ) return integer is
	begin
		if a<b then
			return a;
		else
			return b;
		end if;
	end function;
	
	pure function maximum (
		a : real;
		b : real ) return real is
	begin
		if a>b then
			return a;
		else
			return b;
		end if;
	end function;
		
	pure function minimum (
		a : real;
		b : real ) return real is
	begin
		if a<b then
			return a;
		else
			return b;
		end if;
	end function;
	
	pure function lower (
		va, vb : std_logic_vector;
		s : boolean := FALSE ) return std_logic is
		variable ta, tb, vo : std_logic_vector( va'length downto 0 );
		begin
			ta( ta'high-1 downto 0 ) := va;
			ta( ta'high ) := '0';
			tb( tb'high-1 downto 0 ) := vb;
			tb( tb'high ) := '0';
			if s then
				ta( ta'high-1 ) := not ta( ta'high-1 );
				tb( tb'high-1 ) := not tb( tb'high-1 );
			end if;
			vo := ta-tb;
			return vo(vo'high);
		end function;
	
	pure function lower(
		va : std_logic_vector;
		i : integer;
		s : boolean := FALSE ) return std_logic is
		begin
			return lower( va, to_vector( i, va'length ), s );
		end function;
	
	pure function greater (
		va, vb : std_logic_vector;
		s : boolean := FALSE ) return std_logic is
		variable ta, tb, vo : std_logic_vector( va'length downto 0 );
		begin
			return lower( vb, va, s );
		end function;
	
	pure function greater(
		va : std_logic_vector;
		i : integer;
		s : boolean := FALSE ) return std_logic is
		begin
			return greater( va, to_vector( i, va'length ), s );
		end function;
	
	pure function round(
		v : std_logic_vector;
		d : integer ) return std_logic_vector is
		variable va, vb, vr : std_logic_vector( v'length - d - 1 downto 0 );
		begin
			va := v( v'high downto d );
			vb := (others=>'0');
			if d>0 then
				vb(0) := v(d-1);
			end if;
			vr := va + vb;
			return vr;
		end function;
	
	pure function bestdiv (
		src : integer;
		dst : integer ) return integer is
	begin
		return ((src + ( (dst+1)/2 )) /dst );
	end function;

	pure function nextCRC(
		prev : std_logic_vector;
		sdi : std_logic;
		poly : std_logic_vector ) return std_logic_vector is
	variable invect, outvect : std_logic_vector( prev'range );
	begin
		invect := ( prev( prev'high-1 downto prev'low ) & '0' );
		outvect := invect xor (poly and (sdi xor prev( prev'high )));
		--invect := ( prev( prev'high-1 downto prev'low ) & sdi );
		--outvect := invect xor (poly and prev( prev'high ));
		return outvect;
	end function;
	
	pure function nextCRC(
		prev : std_logic_vector;
		sdi : std_logic_vector;
		poly : std_logic_vector ) return std_logic_vector is
	variable CRC : std_logic_vector( prev'range );
	begin
		CRC := prev;
		for i in sdi'low to sdi'high loop
			CRC := nextCRC( CRC, sdi(i), poly );
		end loop;
		return CRC;
	end function;
	
	pure function magicCRC(
		poly : std_logic_vector ) return std_logic_vector is
	variable test : std_logic_vector( poly'range );
	begin
		test := (others=>'1');
		return nextCRC( test, not test, poly );
	end function;
		
	pure function nextScramState(
		prev : std_logic_vector;
		sdi : std_logic;
		poly : std_logic_vector ) return std_logic_vector is
	variable invect, outvect : std_logic_vector( prev'range );
	begin	
		invect := ( prev( prev'high-1 downto prev'low ) & sdi );
		outvect := invect( prev'high downto 1 ) & ( parity( invect and poly ) xor prev(prev'high) );
		return outvect;
	end function;
	
	pure function nextScramState(
		prev : std_logic_vector;
		sdi : std_logic_vector;
		poly : std_logic_vector ) return std_logic_vector is
	variable outvect : std_logic_vector( prev'range );
	begin
		outvect := prev;
		for i in sdi'low to sdi'high loop
			outvect := nextScramState( outvect, sdi(i), poly );
		end loop;
		return outvect;
	end function;
	
	pure function nextScramData(
		prev : std_logic_vector;
		sdi : std_logic;
		poly : std_logic_vector ) return std_logic is
	variable invect, outvect : std_logic_vector( prev'range );
	begin	
		invect := ( prev( prev'high-1 downto prev'low ) & sdi );
		outvect := invect( prev'high downto 1 ) & ( parity( invect and poly ) xor prev(prev'high) );
		return outvect( prev'low );
	end function;

	pure function nextScramData(
		prev : std_logic_vector;
		sdi : std_logic_vector;
		poly : std_logic_vector ) return std_logic_vector is
	variable statevect : std_logic_vector( prev'range );
	variable outvect : std_logic_vector( sdi'range );
	begin
		statevect := prev;
		for i in sdi'low to sdi'high loop
			outvect(i) := nextScramData( statevect, sdi(i), poly );
			statevect := nextScramState( statevect, sdi(i), poly );
		end loop;
		return outvect;
	end function;

	pure function nextDescramState(
		prev : std_logic_vector;
		sdi : std_logic;
		poly : std_logic_vector ) return std_logic_vector is
	variable outvect : std_logic_vector( prev'range );
	begin
		outvect := ( prev( prev'high-1 downto prev'low ) & sdi );
		return outvect;
	end function;
	
	pure function nextDescramState(
		prev : std_logic_vector;
		sdi : std_logic_vector;
		poly : std_logic_vector ) return std_logic_vector is
	variable outvect : std_logic_vector( prev'range );
	begin
		outvect := prev;
		for i in sdi'low to sdi'high loop
			outvect := nextDescramState( outvect, sdi(i), poly );
		end loop;
		return outvect;
	end function;
	
	pure function nextDescramData(
		prev : std_logic_vector;
		sdi : std_logic;
		poly : std_logic_vector ) return std_logic is
	variable outvect : std_logic_vector( prev'range );		
	begin
		outvect := ( prev( prev'high-1 downto prev'low ) & sdi );
		return ( parity( outvect and poly ) xor prev( prev'high ) );
	end function;
	
	pure function nextDescramData(
		prev : std_logic_vector;
		sdi : std_logic_vector;
		poly : std_logic_vector ) return std_logic_vector is
	variable statevect : std_logic_vector( prev'range );
	variable outvect : std_logic_vector( sdi'range );
	begin
		statevect := prev;
		for i in sdi'low to sdi'high loop
			outvect(i) := nextDescramData( statevect, sdi(i), poly );
			statevect := nextDescramState( statevect, sdi(i), poly );
		end loop;
		return outvect;
	end function;
	
	pure function demux(
		x : std_logic_vector ) return std_logic_vector is
	variable j : integer range 0 to (2**(x'length))-1;
	variable y : std_logic_vector( (2**(x'length))-1 downto 0 );
	begin
		j := conv_integer(x);
		for i in y'range loop
			if i=j then
				y(i):='1';
			else
				y(i) := '0';
			end if;
		end loop;
		return y;
	end function;
	
	pure function lowbyte( x : std_logic_vector ) return std_logic_vector is
	variable y : std_logic_vector( 7 downto 0 );
	begin
		if x'length<8 then
			y := (others=>'0');
			y(x'range) := x;
		else
			y := x(y'range);
		end if;
		return y;
	end;
	
	pure function highbyte( x : std_logic_vector ) return std_logic_vector is
	variable y : std_logic_vector( 7 downto 0 );
	begin
		if x'length<8 then
			y := (others=>'0');
		elsif x'length<16 then
			y := (others=>'0');
			y(x'high-8 downto 0) := x(x'high downto 8);
		else
			y := x(15 downto 8);
		end if;
		return y;
	end;
	
	pure function lowbyte( x : integer ) return std_logic_vector is
	variable y : std_logic_vector( 15 downto 0 );
	begin
		y := conv_std_logic_vector( x, 16 );
		return lowbyte( y );
	end function;

	pure function highbyte( x : integer ) return std_logic_vector is
	variable y : std_logic_vector( 15 downto 0 );
	begin
		y := conv_std_logic_vector( x, 16 );
		return highbyte( y );
	end function;

	pure function signext(
		v : std_logic_vector;
		i : integer ) return std_logic_vector is
		variable retv : std_logic_vector ( i-1 downto 0 );
	begin
		retv := ( others=> v(v'high) );
		retv( v'length-1 downto 0 ) := v;
		return retv;
	end function;
	
	pure function zeroext(
		v : std_logic_vector;
		i : integer ) return std_logic_vector is
		variable retv : std_logic_vector ( i-1 downto 0 );
	begin
		retv := ( others=> '0' );
		retv( v'length-1 downto 0 ) := v;
		return retv;
	end function;
	
	pure function scale(
		v : std_logic_vector;
		i : integer ) return std_logic_vector is
		variable retv : std_logic_vector ( i-1 downto 0 ) := (others=>'0');
	begin
		if i>v'length then
			retv( i-1 downto i-v'length ) := v;
			retv( i-v'length-1 downto 0 ) := (others=>'0');
		else
			retv := v( v'high downto v'high-i+1 );
		end if;
		return retv;
	end function;

	pure function match(
		a : std_logic_vector;
		b : std_logic_vector ) return std_logic is
	begin
		assert a'length = b'length report
			"MATCH: vectors' sizes are unmatched." severity ERROR;
		assert (a'left-a'right)*(b'left-b'right)>=0 report
			"MATCH: vectors' ranges are in opposite directions." severity ERROR;
			
		for i in 1 to a'length loop
			if a( a'low+i-1 ) /= b( b'low+i-1 ) then
				return '0';
			end if;
		end loop;
		return '1';
	end function;
	
	pure function reverse( v : std_logic_vector ) return std_logic_vector is
	variable rv : std_logic_vector( v'range );
	begin
		for i in v'range loop
			rv(i) := v( v'high-i+v'low );
		end loop;
		return rv;
	end function;
	
	pure function match(
		a : std_logic_vector;
		b : integer ) return std_logic is
		variable bv : std_logic_vector (a'range);
	begin
		bv := conv_std_logic_vector( b, a'length );
		for i in 1 to a'length loop
			if a( a'low+i-1 ) /= bv( a'low+i-1 ) then
				return '0';
			end if;
		end loop;
		return '1';
	end function;
	
	pure function match (
		a : integer;
		b : integer ) return std_logic is
	begin
		if a=b then
			return '1';
		else
			return '0';
		end if;
	end function;
	
	procedure inc( signal v : inout std_logic_vector ) is
	begin
		v <= v + 1;
	end procedure;
	
	procedure inc( signal i : inout integer ) is
	begin
		i <= i + 1;
	end procedure;
	
	procedure dec( signal v : inout std_logic_vector ) is
	begin
		v <= v - 1;
	end procedure;

	procedure dec( signal i : inout integer ) is
	begin
		i <= i - 1;
	end procedure;

	pure function "or" (
		l : std_logic_vector;
		r : std_logic ) return std_logic_vector is
	variable v : std_logic_vector (l'range);
	begin
		for i in l'range loop
			v(i) := l(i) or r;
		end loop;
		return v;
	end function;
		
	pure function "or" (
		l : std_logic;
		r : std_logic_vector ) return std_logic_vector is
	begin
		return ( r or l );
	end function;

	pure function "and" (
		l : std_logic_vector;
		r : std_logic ) return std_logic_vector is
	variable v : std_logic_vector (l'range);
	begin
		for i in l'range loop
			v(i) := l(i) and r;
		end loop;
		return v;
	end function;
		
	pure function "and" (
		l : std_logic;
		r : std_logic_vector ) return std_logic_vector is
	begin
		return ( r and l );
	end function;

	pure function "xor" (
		l : std_logic_vector;
		r : std_logic ) return std_logic_vector is
	variable v : std_logic_vector (l'range);
	begin
		for i in l'range loop
			v(i) := l(i) xor r;
		end loop;
		return v;
	end function;
		
	pure function "xor" (
		l : std_logic;
		r : std_logic_vector ) return std_logic_vector is
	begin
		return ( r xor l );
	end function;

	pure function "ror" (
		l : std_logic_vector;
		r : integer ) return std_logic_vector is
	variable y : std_logic_vector (l'range);
	variable i : integer;
	begin
		for j in l'range loop
			i := j-r;
			if (i<l'low) then
				i := i + l'length;
			end if;
			y(i) := l(j);
		end loop;
		return y;
	end function;
		
	pure function "rol" (
		l : std_logic_vector;
		r : integer ) return std_logic_vector is
	variable y : std_logic_vector (l'range);
	variable i : integer;
	begin
		for j in l'range loop
			i := j+r;
			if (i>l'high) then
				i := i - l'length;
			end if;
			y(i) := l(j);
		end loop;
		return y;
	end function;
	
	pure function bin_string( s : std_logic_vector ) return string is
		variable str : string( 1 to s'length );
		variable pos : integer;
	begin
		pos := 1;
		for i in s'range loop
			case s(i) is
				when '1' =>
					str(pos) := '1';
				when others =>
					str(pos) := '0';
			end case;
			pos := pos+1;
		end loop;
		return str;
	end function;
	
	pure function bin_string( s : integer; len : integer ) return string is
		variable v : std_logic_vector( len-1 downto 0 );
	begin
		v := conv_std_logic_vector( s, len );
		return bin_string(v);
	end function;
	
	pure function string32h( i : std_logic_vector ) return string is
	variable v : dword;
	variable s : string( 1 to 8 );
	constant ch : string( 1 to 16 ) := "0123456789ABCDEF";
	begin
		v := zeroext( i,v'length );
		for k in s'range loop
			s(s'high-k+1) := ch( conv_integer( v( k*4-1 downto k*4-4 ))+1);
		end loop;
		return s;
	end function;
	
	pure function string16h( i : std_logic_vector ) return string is
	variable v : word;
	variable s : string( 1 to 4 );
	constant ch : string( 1 to 16 ) := "0123456789ABCDEF";
	begin
		v := zeroext( i,v'length );
		for k in s'range loop
			s(s'high-k+1) := ch( conv_integer( v( k*4-1 downto k*4-4 ))+1);
		end loop;
		return s;
	end function;
	
	pure function string8h( i : std_logic_vector ) return string is
	variable v : byte;
	variable s : string( 1 to 2 );
	constant ch : string( 1 to 16 ) := "0123456789ABCDEF";
	begin
		v := zeroext( i,v'length );
		for k in s'range loop
			s(s'high-k+1) := ch( conv_integer( v( k*4-1 downto k*4-4 ))+1);
		end loop;
		return s;
	end function;
	
	pure function string32h( i : integer ) return string is
	variable v : dword;
	begin
		v := conv_std_logic_vector(i,v'length);
		return string32h(v);
	end function;
	
	pure function string16h( i : integer ) return string is
	variable v : word;
	begin
		v := conv_std_logic_vector(i,v'length);
		return string16h(v);
	end function;
	
	pure function string8h( i : integer ) return string is
	variable v : byte;
	begin
		v := conv_std_logic_vector(i,v'length);
		return string8h(v);
	end function;
	
	pure function char2hex( c : character ) return integer is
	begin
		if c>='0' and c<='9' then
			return character'pos(c)-character'pos('0');
		elsif c>='A' and c<='F' then
			return character'pos(c)+10-character'pos('A');
		elsif c>='a' and c<='f' then
			return character'pos(c)+10-character'pos('a');
		else
			return -1;
		end if;
	end function;			
	
	pure function ascii( ai : character ) return integer is
	begin
		return character'pos(ai);
	end function;

	pure function ascii( ai : integer ) return character is
	begin
		return character'val(ai);
	end function;

	pure function ascii2nibble ( av : byte ) return nibble is
		variable ai : integer range 0 to 255;
		variable rv : nibble;
		variable unknown : nibble;
	begin
		unknown := "----";
		ai := conv_integer( av );
		if ai < ascii('0') then
			rv := unknown;
		elsif ai < (ascii('9')+1) then
			rv := conv_std_logic_vector( ai-ascii('0'), 4 );
		elsif ai < ascii('A') then
			rv := unknown;
		elsif ai < (ascii('F')+1) then
			rv := conv_std_logic_vector( ai+10-ascii('A'), 4 );
		elsif ai < ascii('a') then
			rv := unknown;
		elsif ai < (ascii('f')+1) then
			rv := conv_std_logic_vector( ai+10-ascii('a'), 4 );
		else
			rv := unknown;
		end if;
		return rv;
	end function;

	pure function nibble2ascii ( av : nibble ) return byte is
		variable ai : integer range 0 to 255;
		variable rv : byte;
	begin
		ai := conv_integer( av );
		if ai < 10 then
			rv := conv_std_logic_vector( ai+ascii('0'), 8 );
		else
			rv := conv_std_logic_vector( ai-10+ascii('A'), 8 );
		end if;
		return rv;
	end function;
	
	pure function is_ascii_digit( av : byte ) return std_logic is
		variable ai : integer range 0 to 255;
		variable rv : std_logic;
	begin
		ai := conv_integer( av );
		if ai < ascii('0') then
			rv := '0';
		elsif ai < ascii('9')+1 then
			rv := '1';
		elsif ai < ascii('A') then
			rv := '0';
		elsif ai < ascii('F')+1 then
			rv := '1';
		elsif ai < ascii('a') then
			rv := '0';
		elsif ai < ascii('f')+1 then
			rv := '1';
		else
			rv := '0';
		end if;
		return rv;
	end function;
			
	pure function reorder( vi : std_logic_vector; ord : string ) return std_logic_vector is
		variable vo : std_logic_vector( vi'range ) := (others=>'0');
		variable src, dst : integer;
	begin
		if ord'length<=1 then
			return vi;
		else
			for i in ord'range loop
				vo(i-1) := vi(char2hex(ord(i)));
			end loop;
			return vo;
		end if;
	end function;
		
	pure function mixbits( e, o : std_logic_vector ) return std_logic_vector is
		variable q : std_logic_vector( (e'length*2)-1 downto 0 );
	begin
		for i in e'range loop
			q(i*2) := e(i);
			q(i*2+1) := o(i);
		end loop;
		return q;
	end function;
		
end package body;

-------------------------------------------------------------------------------
-- Indicator driver

library ieee;
use ieee.std_logic_1164.all;

entity IndicatorDriver is
	generic (
		DATA_WIDTH		: integer := 3;			-- IO ports count
		CLK_DIVIDER		: integer := 5;			-- Data sampling divider
		SUB_DIVIDER		: integer := 4;			-- Minimum LED ON time divider
		INVERSION		: boolean := FALSE);	-- Output inversion
	port (
		CLK		: in std_logic;			-- Global clock
		RSTn	: in std_logic := '1';	-- Async. reset
		D	: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');	-- Pulse inputs (1)
		Q	: out std_logic_vector( DATA_WIDTH-1 downto 0 );					-- Pulse outputs (1)
		RO	: out std_logic );			-- Main divider output
end entity;

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

architecture rtl of IndicatorDriver is
	type count_array is array ( 0 to DATA_WIDTH-1 ) of integer range 0 to SUB_DIVIDER-1;
	signal MainCounter : integer range 0 to CLK_DIVIDER-1 := CLK_DIVIDER-1;
	signal Dreg, Qreg : std_logic_vector ( DATA_WIDTH-1 downto 0 );
	signal ChannelCounter : count_array := (others=>SUB_DIVIDER-1);
	signal MainReset : std_logic;
begin
	indrv: process ( CLK, RSTn )
	begin
		if RSTn='0' then
			MainCounter <= CLK_DIVIDER-1;
			MainReset <= '0';
			Dreg <= (others=>'1');
			Qreg <= (others=>'1');
			for i in 0 to DATA_WIDTH-1 loop
				ChannelCounter(i) <= SUB_DIVIDER-1;
			end loop;
			
		elsif CLK'event and CLK='1' then
			if MainReset='1' then
				MainCounter <= CLK_DIVIDER-1;
			else
				MainCounter <= MainCounter-1;
			end if;
			if MainCounter=1 then
				MainReset <= '1';
			else
				MainReset <= '0';
			end if;
			Dreg <= D;
			
			for i in 0 to DATA_WIDTH-1 loop
				if Dreg(i)='1' then
					Qreg(i) <= '1';
					ChannelCounter(i) <= SUB_DIVIDER-1;
				elsif MainReset='1' then
					if ChannelCounter(i)=0 then
						ChannelCounter(i) <= SUB_DIVIDER-1;
					else
						ChannelCounter(i) <= ChannelCounter(i)-1;
					end if;
					if ChannelCounter(i)=0 then
						Qreg(i) <= '0';
					end if;
				end if;
			end loop;
			
		end if;
	end process;
	Q <= not Qreg when INVERSION else Qreg;
	RO <= MainReset;
end;
	
-------------------------------------------------------------------------------
-- Reset synchronizer cirquit

library ieee, work;
use ieee.std_logic_1164.all;

entity ResetSynchronizer is
	port (
		CLK		: in std_logic;			-- Global input clock
		RSTIn	: in std_logic := '1';	-- Global async RESET (negative)
		RSTOn	: out std_logic );		-- Global sync RESET output (negative)
end entity;

architecture rtl of ResetSynchronizer is
	signal R0, R1 : std_logic;
begin
	process (CLK, RSTIn)
	begin
		if RSTIn='0' then
			R0 <= '0';
			R1 <= '0';
		elsif rising_edge(CLK) then
			R0 <= '1';
			R1 <= R0;
		end if;
	end process;
	RSTOn <= R1;
end architecture;


-------------------------------------------------------------------------------
-- Clock divider

library ieee;
use ieee.std_logic_1164.all;

entity ClockDivider is
	generic (
		CLKDIV	: integer := 1 );		-- Clock divider
	port (
		CLKI	: in std_logic;			-- Global input clock
		RSTn	: in std_logic := '1';	-- Global async RESET (negative)
		
		DIVO	: out std_logic;		-- Enable output pulse (CLKI/CLKDIV) = RISE or FALL
		CLKO	: out std_logic;		-- 50/50 clock output (CLKI/CLKDIV/2)
		RISE	: out std_logic;		-- CLKO rising edge pulse (CLKI/CLKDIV/2)
		FALL	: out std_logic );		-- CLKO falling edge pulse (CLKI/CLKDIV/2)
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

architecture rtl of ClockDivider is
	signal EN, CO : std_logic := '0';
begin

	DIVO <= EN;
	CLKO <= CO;
	
	zdiv: if CLKDIV<=0 generate
		EN <= '0' when RSTn='0' else '1' when rising_edge(CLKI);
		CO <= CLKI;
		RISE <= EN;
		FALL <= EN;
	end generate;
	
	idiv: if CLKDIV=1 generate
		EN <= '0' when RSTn='0' else '1' when rising_edge(CLKI);
		CO <= '0' when RSTn='0' else not CO when rising_edge(CLKI);
		RISE <= not CO;
		FALL <= CO;
	end generate;
	
	ediv: if CLKDIV>1 generate
		cntrs: block
			signal DIVCNT : integer range 0 to set(CLKDIV-1) := CLKDIV-1;
		begin
			process (CLKI,RSTn)
			begin
				if RSTn='0' then
					DIVCNT <= CLKDIV-1;
					EN <= '0';
					CO <= '0';
					RISE <= '0';
					FALL <= '0';
				elsif rising_edge(CLKI) then
					if EN='1' then
						CO <= not CO;
						DIVCNT <= CLKDIV-1;
					else
						DIVCNT <= DIVCNT-1;
					end if;
					if DIVCNT=1 then
						EN <= '1';
						RISE <= not CO;
						FALL <= CO;
					else
						EN <= '0';
						RISE <= '0';
						FALL <= '0';
					end if;
				end if;
			end process;
		end block;
	end generate;
	
end architecture;
	
--------------------------------------------------------------------------------------------------------
-- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER --
--------------------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;

entity FractClockDivider is
	generic (
		DIVIDER : real := 3.0;		-- Divider for Q output
		PRECESION : real := 100.0 );-- Division precesion
	port (
		CLK : in std_logic;			-- Main clock
		RSTn : in std_logic := '1';	-- Async. reset
		EN : in std_logic := '1';	-- Output & divide enable
		Q : out std_logic;			-- Output 1CLK pulses
		F2 : out std_logic );		-- Q/2 frequency
end entity;

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

architecture rtl of FractClockDivider is

	constant ADDER_WIDTH : integer := integer( ceil( log2(PRECESION) ) );
	constant COUNTER_WIDTH : integer := ADDER_WIDTH + integer( ceil( log2(DIVIDER) ) ) - 1;
	constant ADDER_VALUE : integer := integer((2.0**real(COUNTER_WIDTH))/DIVIDER);
	constant ADDER : std_logic_vector( ADDER_WIDTH-1 downto 0 ) := conv_std_logic_vector( ADDER_VALUE, ADDER_WIDTH );
	
	signal Counter : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
	signal EnOut, Ovf, OvfOut, PostDiv : std_logic;
	
begin
	process ( CLK, RSTn )
		variable AIN, AOUT : std_logic_vector( COUNTER_WIDTH downto 0 );
	begin
		if RSTn='0' then
			Counter <= (others=>'0');
			EnOut <= '0';
			Ovf <= '0';
			OvfOut <= '0';
			PostDiv <= '0';
		elsif CLK'event and CLK='1' then
		
			EnOut <= EN;
			OvfOut <= Ovf and EnOut;
			PostDiv <= PostDiv xor OvfOut;
			
			if EN='1' then
				AIN( AIN'high downto ADDER'length ) := (others=>'0');
				AIN( ADDER'range ) := ADDER;
				AOUT := AIN + ('0' & Counter);
				
				Counter <= AOUT(Counter'range);
				Ovf <= AOUT(AOUT'high);
			else
				Ovf <= '0';
			end if;
			
		end if;
	end process;
	
	Q <= OvfOut;
	F2 <= PostDiv;
	
end architecture;
		
---------------------------------------------------------------------------------------------
-- Safe clock switch

library ieee;
use ieee.std_logic_1164.all;

entity ClockSwitch is
	port (
		SEL, CLKA, CLKB : in std_logic;
		CLKOUT : out std_logic
		);
end entity;

--library work;
--use work.ak_hardware.all;

	architecture rtl of ClockSwitch is
		signal DFFA, DFFB, BUFIN : std_logic;
	begin
		process ( CLKA, CLKB )
		begin
			if CLKA'event and CLKA='0' then
				DFFA <= not ( SEL or DFFB );
			end if;
			if CLKB'event and CLKB='0' then
				DFFB <= SEL and ( not DFFA );
			end if;
		end process;

		BUFIN <= ( DFFA and CLKA ) or ( DFFB and CLKB );
		--clk_cell: buff port map ( BUFIN, CLKOUT );
		CLKOUT <= BUFIN;
	end architecture;

---------------------------------------------------------------------------------------------
-- Data syncronizer

library ieee;
use ieee.std_logic_1164.all;

entity DataSync is
	generic (
		REGW : integer := 4;					-- Register data width
		SYNCEN : boolean := TRUE );				-- Enable/disable synchronization
	port (
		DI : in std_logic_vector( REGW-1 downto 0 ) := (others=>'0');	-- Reg input
		DO : out std_logic_vector( REGW-1 downto 0 );					-- Reg output

		RSTIn : in std_logic := '1';			-- Async. reset input
		RSTOn : out std_logic;					-- Syncronized reset output
		EI : in std_logic := '1';				-- Input data enable
		CLKI, CLKO : in std_logic := '0' );		-- Reference clocks
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

architecture rtl of DataSync is
	subtype data_vector is std_logic_vector ( REGW-1 downto 0 );
	signal DataInter : data_vector := ( others=>'-' );
	signal EnableIn, EnableOut, ReadyIn, ReadyOut, ResetOut : std_logic := '0';
begin

	dissync: if not SYNCEN generate
		DO <= DI;
		RSTOn <= RSTIn;
	end generate;
	
	ensync: if SYNCEN generate

		RSTOn <= ResetOut;
		rstsync: ResetSynchronizer port map ( CLKO, RSTIn, ResetOut );
			
		process ( CLKI, RSTIn )
		begin
			if RSTIn='0' then
				EnableIn <= '0';
				ReadyIn <= '0';
				DataInter <= (others=>'0');
			elsif rising_edge(CLKI) then
				EnableIn <= (not ( ReadyIn xor ReadyOut )) and (not EnableIn) and EI;
				ReadyIn <= ReadyIn xor EnableIn;
				if EnableIn='1' then
					DataInter <= DI;
				end if;
			end if;
		end process;
		
		process ( CLKO, ResetOut )
		begin
			if ResetOut='0' then
				EnableOut <= '0';
				ReadyOut <= '0';
				DO <= (others=>'0');
			elsif rising_edge(CLKO) then
				EnableOut <= (( ReadyIn xor ReadyOut )) and (not EnableOut);
				ReadyOut <= ReadyOut xor EnableOut;
				if EnableOut='1' then
					DO <= DataInter;
				end if;
			end if;
		end process;

	end generate;

end;

---------------------------------------------------------------------------------------------
-- N(N-1) LED multiplexor

library ieee;
use ieee.std_logic_1164.all;

entity LEDMultiplexorNN1 is
	generic (
		MUX_FACT	: integer := 3;		-- Multiplexion factor, driver count
		LED_CFG		: string := "-";	-- Led order, like "012345" or "103254"
		COM_CATH	: boolean := FALSE;	-- TRUE = Common cathode, FALSE = anode
		DIVIDER		: integer := 127 );	-- Multiplexing frequency divider
	port (
		CLK		: in std_logic;			-- Global input clock
		RSTn	: in std_logic := '1';	-- Global async RESET (negative)
		
		D : in std_logic_vector( ((MUX_FACT-1)*MUX_FACT)-1 downto 0 );	-- Led inputs, non-inverting
		Q : out std_logic_vector( MUX_FACT-1 downto 0 ) );	-- Led drivers, 01Z
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

architecture rtl of LEDMultiplexorNN1 is
				
	pure function Multiplex( vi : std_logic_vector; a, c : integer ) return std_logic is
		variable d : integer;
	begin
		if a=c then
			return '1';
		else
			d := c-a-1;
			if d<0 then
				d := d + MUX_FACT;
			end if;
			return vi((a*(MUX_FACT-1))+d);
		end if;
	end function;
	
	pure function GetCathode( a : std_logic_vector; vi : std_logic_vector ) return std_logic_vector is
		variable c: std_logic_vector( a'range );
	begin
		c := a;
		for i in a'range loop
			if a(i)='1' then
				for j in a'range loop
					c(j) := c(j) or Multiplex( vi, i, j );
				end loop;
			end if;
		end loop;
		return c;
	end function;
	
	constant INPUT_SIZE : integer := MUX_FACT * (MUX_FACT-1);
	
	signal DivCounter : std_logic_vector( msb(DIVIDER-1) downto 0 );
	signal CommonEnable : std_logic;
	signal Anode, Cathode, Output : std_logic_vector( MUX_FACT-1 downto 0 );
	signal DirectInput, SyncInput : std_logic_vector( INPUT_SIZE-1 downto 0 );
	
begin
	
	Q <= Output;
	DirectInput <= D;
	
	outs: for i in Output'range generate
		Output(i) <=	'Z' when Cathode(i)='0' else
						(not Anode(i)) when COM_CATH else Anode(i);
	end generate;
	
	process ( CLK, RSTn )
	begin
		if RSTn='0' then
		
			DivCounter <= conv_std_logic_vector( DIVIDER-1, DivCounter'length );
			CommonEnable <= '0';
			Anode <= (0=>'1',others=>'0');
			Cathode <= (others=>'0');
			SyncInput <= (others=>'1');
			
		elsif rising_edge(CLK) then
		
			SyncInput <= reorder( DirectInput, LED_CFG );
			
			if CommonEnable='1' then
				DivCounter <= conv_std_logic_vector( DIVIDER-1, DivCounter'length );
				Anode <= Anode rol 1;
				Cathode <= GetCathode( (Anode rol 1), SyncInput );
			else
				DivCounter <= DivCounter-1;
			end if;
			CommonEnable <= match( DivCounter, 1 );
			
		end if;
	end process;
end architecture;
	
---------------------------------------------------------------------------------------------
-- Delay line

library ieee;
use ieee.std_logic_1164.all;

entity DelayLine is
	generic (
		LATENCY : integer := 3;		-- Delay value
		WIDTH : integer := 8 );		-- Data width
	port (
		D : in std_logic_vector( WIDTH-1 downto 0 ):= (others=>'0');	-- Input data
		Q : out std_logic_vector( WIDTH-1 downto 0 );					-- Output data
		CLK : in std_logic := '0';		-- Global clock
		RSTn : in std_logic := '1' );	-- Async. reset, negative
end entity;

library work;
use work.ak_types.all;

architecture rtl of DelayLine is
	subtype data_vector is std_logic_vector( WIDTH-1 downto 0 );
	type data_array is array( 0 to LATENCY ) of data_vector;
	signal Sreg : data_array := (others=>(others=>'0'));
begin
	Sreg(0) <= D;
	isdelay: if LATENCY>0 generate
		nextdelay: for i in 1 to LATENCY generate
			Sreg(i) <= (others=>'0') when RSTn='0' else Sreg(i-1) when rising_edge(CLK);
		end generate;
	end generate;
	Q <= Sreg(LATENCY);
end architecture;

---------------------------------------------------------------------------------------------
-- MAX6577 temperature sensor interface

library ieee;
use ieee.std_logic_1164.all;

entity MAX6577 is
	generic (
		DATA_WIDTH : integer := 14;							-- Pulse counter data width
		CLK_DIV : integer := 20000000/8;					-- Counting reset divider
		FILTER_TAPS : integer := 5 );						-- Digital low pass filter length
	port (
		CLK : in std_logic;									-- Main clock
		RSTn : in std_logic := '1';							-- Async. reset
		DI : in std_logic;									-- Pulse input
		DO : out std_logic_vector( DATA_WIDTH-1 downto 0 );	-- Counter output
		OVF,												-- Counter overflow flag
		NEU,												-- New data pulse
		TOG : out std_logic );								-- New data toggle
end entity;
	
library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

architecture rtl of MAX6577 is	
	signal PreSample : std_logic_vector( FILTER_TAPS-1 downto 0 );
	signal Cycle, Major, Minor, OvfReg, OvfOut, NewOut, TogOut : std_logic;
	signal CntReg, OutReg : std_logic_vector( DATA_WIDTH-1 downto 0 );
begin

	PreSample <= ( PreSample( PreSample'high-1 downto 0 ) & DI ) when rising_edge(CLK);
	DO <= OutReg;
	OVF <= OvfOut;
	NEU <= NewOut;
	TOG <= TogOut;
	
	process (CLK, RSTn)
	begin
		if RSTn='0' then
			Major <= '0';
			Minor <= '0';
			CntReg <= (others=>'0');
			OutReg <= (others=>'0');
			OvfReg <= '0';
			OvfOut <= '0';
			NewOut <= '0';
			TogOut <= '0';
		elsif rising_edge(CLK) then
			if match(PreSample,0)='1' then
				Major <= '1';
			elsif match(PreSample,-1)='1' then
				Major <= '0';
			end if;
			Minor <= Major;
			
			if Cycle='1' then
				CntReg(CntReg'high downto 1) <= (others=>'0');
				CntReg(0) <= Major xor Minor;
				OvfReg <= '0';
				TogOut <= not TogOut;
				
				OvfOut <= OvfReg;
				if OvfReg='1' then
					OutReg <= (others=>'1');
				else
					OutReg <= CntReg;
				end if;
			elsif (Major xor Minor) ='1' then
				CntReg <= CntReg+1;
				OvfReg <= OvfReg or match(CntReg,-1);
			end if;
			NewOut <= Cycle;
		end if;
	end process;

	divr: ClockDivider
		generic map ( CLK_DIV )	-- Clock divider
		port map (
			CLKI => CLK,		-- Global input clock
			RSTn => RSTn,		-- Global async RESET (negative)
			DIVO => Cycle );	-- Enable output pulse (CLKI/CLKDIV) = RISE or FALL

end architecture;

---------------------------------------------------------------------------------------------
-- Simple priority register

library ieee;
use ieee.std_logic_1164.all;

entity SimplePriority is
	generic (
		CODE_LENGTH : integer := 4;		-- Output code size
		HIGH_PRIOR : boolean := TRUE );	-- High/low priority selection
	port (
		CLK		: in std_logic;				-- Global clock
		RSTn	: in std_logic := '1';		-- Async. reset, negative
		
		SRC		: in std_logic_vector( (2**CODE_LENGTH)-1 downto 0 );	-- Source vector
		CODE	: out std_logic_vector( CODE_LENGTH-1 downto 0 );		-- Output priority code
		FLAG	: out std_logic );			-- Signal presence flag
end entity;


library work;
use work.ak_types.all;

architecture rtl of SimplePriority is
	signal InVector : std_logic_vector( (2**CODE_LENGTH)-1 downto 0 );
	signal OutCode : std_logic_vector( CODE_LENGTH downto 0 );
begin
	InVector <= SRC when HIGH_PRIOR else reverse(SRC);
	process (CLK, RSTn)
	begin
		if RSTn='0' then
			OutCode <= (others=>'0');
		elsif rising_edge(CLK) then
			OutCode <= priority( InVector, TRUE );
		end if;
	end process;
	CODE <= OutCode(OutCode'high-1 downto 0) when HIGH_PRIOR else not OutCode(OutCode'high-1 downto 0);
	FLAG <= OutCode(OutCode'high);
end architecture;

---------------------------------------------------------------------------------------------
-- Token ring distributed arbiteration controller

library ieee, work;
use ieee.std_logic_1164.all;
use work.ak_types.all;

entity TokenArbiter is
	generic (
		TOKEN_START : boolean := FALSE );	-- Controller reset state
	port (
		CK			: in clock_domain;		-- Synchronization
		REQ			: in std_logic := '0';	-- Arbitration request
		ACK			: out std_logic;		-- Arbitration grant
		ChainIn		: in std_logic := '1';	-- Chain input
		ChainOut	: out std_logic );		-- Chain output
end entity;

architecture rtl of TokenArbiter is
	signal TokenAck : std_logic := '0';
begin
	ACK <= TokenAck;
	process (CK)
	begin
		if CK.RSTn='0' then
			TokenAck <= '0';
			ChainOut <= to_01( TOKEN_START );
		elsif rising_edge( CK.CLK ) and CK.EN='1' then
			TokenAck <= ( ChainIn or TokenAck ) and REQ;
			ChainOut <= ( ChainIn or TokenAck ) and (not REQ );
		end if;
	end process;
end architecture;

-----------------------------------------------------------------------------
-- Многоразрядный сигма-дельта-ЦАП

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use work.ak_types.all;

entity ak_dac is
	generic (
		R_COUNT : integer := 1;			-- Кол-во выходных разрядов
		DATA_WIDTH : integer := 8;			-- Разрядность кода
		DATA_SIGN : boolean := FALSE );		-- Знаковый формат
	port (
		CK : in clock_domain := CLOCK_DOMAIN_INIT;
		D : in std_logic_vector( DATA_WIDTH-1 downto 0 );
		Q : out std_logic_vector( R_COUNT-1 downto 0 ) );
end entity;
		
architecture rtl of ak_dac is

	constant OVER_DIGITS : integer := integer(CEIL(LOG2(real(R_COUNT+1))));
	signal EffD : std_logic_vector( D'range );
	signal Sum, Add : std_logic_vector( DATA_WIDTH + OVER_DIGITS - 1 downto 0 );
	signal DCreg : std_logic_vector( R_COUNT-1 downto 0 );
	
	pure function garbage_set(
		n : integer;	-- Число установленных бит
		d : integer )	-- Количество выходных разрядов
		return std_logic_vector is
		
		variable sd : integer;
		variable y : std_logic_vector( d-1 downto 0 );
	begin
		sd := d/2;
		for i in y'range loop
			sd := sd + n;
			if (sd>=d) then
				y(i) := '1';
				sd := sd-d;
			else
				y(i) := '0';
			end if;
		end loop;
		return y;
	end function;

	subtype dc_out_vector is std_logic_vector( R_COUNT-1 downto 0 );
	type encode_tab_array is array (integer range <>) of dc_out_vector;
	signal encode_tab : encode_tab_array( 0 to 2**OVER_DIGITS-1);
	
begin

	d_signed: if DATA_SIGN generate
		EffD <= D xor setmsb( D );
	end generate;
	d_unsigned: if not DATA_SIGN generate
		EffD <= D;
	end generate;

	power2: if R_COUNT = 2**(OVER_DIGITS-1) generate
		single_bit: if R_COUNT=1 generate
			Add <= '0' & EffD;
		end generate;
		multi_bit: if R_COUNT/=1 generate
			Add <= '0' & EffD & zeros(OVER_DIGITS-1);
		end generate;
	end generate;
	
	no_power2: if R_COUNT /= 2**(OVER_DIGITS-1) generate
		Add <= (others=>'0') when CK.RSTn='0' else
			to_vector( R_COUNT * to_integer(EffD), Add'length )
			when (rising_edge(CK.CLK) and CK.EN='1');
	end generate;
	
	etab: for i in encode_tab'range generate
		encode_tab(i) <= garbage_set( i, R_COUNT );
	end generate;

	main: process (CK)
		variable AddLow : std_logic_vector(Sum'range);
	begin
		if CK.RSTn='0' then
			if DATA_SIGN then
				Sum <= zeros(OVER_DIGITS-1) & setmsb(DATA_WIDTH);
				DCreg( DCreg'length/2-1 downto 0 ) <= (others=>'0');
				DCreg( DCreg'length-1 downto DCreg'length/2 ) <= (others=>'1');
			else
				Sum <= (others=>'0');
				DCreg <= (others=>'0');
			end if;
		elsif rising_edge(CK.CLK) and CK.EN='1' then
		
			AddLow := (others=>'0');
			AddLow(DATA_WIDTH-1 downto 0) := Sum(DATA_WIDTH-1 downto 0);
			Sum <= Add + AddLow;
			DCreg <= encode_tab( to_integer(
				Sum(Sum'high downto Sum'high-OVER_DIGITS+1) ));
--			DCreg <= garbage_set( to_integer(
--				Sum(Sum'high downto Sum'high-OVER_DIGITS+1) ), DCreg'length );

		end if;
	end process main;	-- CK
	
	Q <= DCreg;

end architecture;

-----------------------------------------------------------------------------
-- Простой генератор импульсов

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

entity ak_pulse is
	generic (	
		LOW_TIME : integer := 7;
		HIGH_TIME : integer := 3 );
	port (
		CK : in clock_domain := CLOCK_DOMAIN_INIT;
		Q : out std_logic;
		H : out std_logic;
		L : out std_logic );
end entity;

architecture rtl of ak_pulse is

	constant CNT_WIDTH : integer := msb(LOW_TIME+HIGH_TIME-1)+1;
	signal CNT : std_logic_vector( CNT_WIDTH-1 downto 0 );
	signal PSTART, PSTOP : std_logic;
	
begin

	main: process ( CK )
	begin
		if CK.RSTn='0' then
			CNT <= to_vector(LOW_TIME+HIGH_TIME-1,CNT'length);
			PSTART <= '0';
			PSTOP <= '0';
			Q <= '0';
		elsif rising_edge( CK.CLK ) and CK.EN='1' then
			if PSTOP = '1' then
				CNT <= to_vector(LOW_TIME+HIGH_TIME-1,CNT'length);
				Q <= '0';
			else
				CNT <= CNT-1;
				if PSTART='1' then
					Q <= '1';
				end if;
			end if;
			PSTART <= match( CNT, HIGH_TIME+1 );
			PSTOP <= match( CNT, 1 );
		end if;	--CK
	end process;
	H <= PSTART;
	L <= PSTOP;

end architecture;

--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--

