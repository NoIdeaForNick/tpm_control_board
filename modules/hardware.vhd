library ieee;
use ieee.std_logic_1164.all;

package ak_hardware is

	constant DEF_MULT_LATENCY : integer := 2;
	constant DEF_RAM_LATENCY : integer := 2;
	constant DEF_RAM_UNKNOWN : std_logic := '0';
	constant DEF_RAM_INIT : std_logic := '0';

	component blvds is
		port (
			DI, OE : in std_logic;
			DO : out std_logic;
			P, N : inout std_logic );
	end component;
		
	component elvds is
		port (
			DI, OE : in std_logic;
			DO : out std_logic;
			P, N : out std_logic );
	end component;

	component rlvds is
		port (
			DI, OE : in std_logic;
			DO : out std_logic;
			P, N : in std_logic );
	end component;

	component olvds is
		port (
			DI, OE : in std_logic;
			DO : out std_logic;
			P, N : out std_logic );
	end component;
	
	component buff is
		port (
			I : in std_logic;
			O : out std_logic );
	end component;

	component clkpll is
		generic (
			CLK_REFERENCE : natural := 33554; -- kHz
			CLK_MULTIPLIER : natural := 1;
			CLK_DIVIDER : natural := 1 );
		port (
			REFCLK	: in std_logic;
			PLLCLK	: out std_logic;
			LOCKED	: out std_logic
			);
	end component;
	
	component clkpllbi is
		generic (
			CLK_REFERENCE : natural := 33554; -- kHz
			CLK_MULTIPLIER : natural := 1;
			CLK_DIVIDER : natural := 1;
			CLK0_PHASE : integer := 0;
			CLK1_PHASE : integer := 90 );
		port (
			REFCLK	: in std_logic;
			PLLCLK	: out std_logic;
			PLLCLK90 : out std_logic;
			LOCKED	: out std_logic
			);
	end component;

	component syncrom is
		generic (
			ADDR_WIDTH : integer := 10;
			DATA_WIDTH : integer := 8;
			READ_LATENCY : integer := DEF_RAM_LATENCY;
			INIT_VALUE : string := "*" );
		port (
			CLK	: in std_logic := '0';
			ADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			DO	: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0') );
	end component;

	component dpsyncram is
		generic (
			ADDR_WIDTH : integer := 10;
			DATA_WIDTH : integer := 8;
			READ_LATENCY : integer := DEF_RAM_LATENCY;
			INIT_VALUE : string := "*" );
		port (
			LCLK	: in std_logic := '0';
			LADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			LDI		: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
			LWE		: in std_logic := '0';
			LDO		: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
			
			RCLK	: in std_logic := '0';
			RADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			RDI		: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
			RWE		: in std_logic := '0';
			RDO		: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0') );
	end component;
	
	component spsyncram is
		generic (
			ADDR_WIDTH : integer := 10;
			DATA_WIDTH : integer := 8;
			READ_LATENCY : integer := DEF_RAM_LATENCY;
			INIT_VALUE : string := "*" );
		port (
			CLK	: in std_logic := '0';
			ADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			DI	: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
			WE	: in std_logic := '0';
			DO	: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0') );
	end component;

	component multuu is
		generic (
			A_WIDTH : integer := 16;	-- A source width
			B_WIDTH : integer := 16;	-- B source width
			LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>0)
		port (
			CLK : in std_logic;								-- Main clock
			A : in std_logic_vector( A_WIDTH-1 downto 0 );	-- Source A, unsigned
			B : in std_logic_vector( B_WIDTH-1 downto 0 );	-- Source B, unsigned
			Q : out std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 ) );	-- Result = A*B, unsigned
	end component;

	component multss is
		generic (
			A_WIDTH : integer := 16;	-- A source width
			B_WIDTH : integer := 16;	-- B source width
			LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>0)
		port (
			CLK : in std_logic;			-- Main clock
			A : in std_logic_vector( A_WIDTH-1 downto 0 );	-- Source A, signed
			B : in std_logic_vector( B_WIDTH-1 downto 0 );	-- Source B, signed
			Q : out std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 ) );	-- Result = A*B, signed
	end component;
	
	component multsu is
		generic (
			S_WIDTH : integer := 16;	-- S source width
			U_WIDTH : integer := 16;	-- U source width
			LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>=0)
		port (
			CLK : in std_logic;			-- Main clock
			S : in std_logic_vector( S_WIDTH-1 downto 0 );	-- Source S, signed
			U : in std_logic_vector( U_WIDTH-1 downto 0 );	-- Source U, unsigned
			Q : out std_logic_vector( S_WIDTH+U_WIDTH-1 downto 0 ) );	-- Result = S*U, signed
	end component;
	
end package;

--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--BODY--

--package body ak_hardware is
--end package body;

--COMPONENTS--COMPONENTS--COMPONENTS--COMPONENTS--COMPONENTS--COMPONENTS--

-- Bus LVDS

library ieee;
use ieee.std_logic_1164.all;

entity blvds is
	port (
		DI, OE : in std_logic;
		DO : out std_logic;
		P, N : inout std_logic );
end entity;

	architecture rtl of blvds is
		signal Pvect, Nvect, DOvect : std_logic_vector (0 downto 0);
	begin
		P <= DI when OE='1' else 'Z';
		N <= not DI when OE='1' else 'Z';
		Pvect(0) <= P;
		Nvect(0) <= N;
		DO <= DOvect(0);
		
		DOvect <= Pvect;				
	end architecture;

library ieee;
use ieee.std_logic_1164.all;

-- LVDS receiver

entity rlvds is
	port (
		DI, OE : in std_logic;
		DO : out std_logic;
		P, N : in std_logic );
end entity;

	architecture rtl of rlvds is
	begin
		DO <=	'1' when P='1' and N='0' else
				'0' when P='0' and N='1' else 'X';
	end architecture;

-- Z-LVDS

library ieee;
use ieee.std_logic_1164.all;

entity elvds is
	port (
		DI, OE : in std_logic;
		DO : out std_logic;
		P, N : out std_logic );
end entity;

	architecture rtl of elvds is
	begin
		P <= DI when OE='1' else 'Z';
		N <= not DI when OE='1' else 'Z';
		DO <= DI;
	end architecture;

-- LVDS output

library ieee;
use ieee.std_logic_1164.all;

entity olvds is
	port (
		DI, OE : in std_logic;
		DO : out std_logic;
		P, N : out std_logic );
end entity;

	architecture rtl of olvds is
	begin
		P <= DI;
		N <= not DI;
		DO <= DI;
	end architecture;

-- Clock/global buffer

library ieee;
use ieee.std_logic_1164.all;

entity buff is
	port (
		I : in std_logic;
		O : out std_logic );
end entity;

	architecture rtl of buff is
	begin
		O <= I;
	end architecture;

-- Unsigned multiplier

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;
use work.ak_hardware.all;

entity multuu is
	generic (
		A_WIDTH : integer := 16;	-- A source width
		B_WIDTH : integer := 16;	-- B source width
		LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>=0)
	port (
		CLK : in std_logic;			-- Main clock
		A : in std_logic_vector( A_WIDTH-1 downto 0 );	-- Source A, unsigned
		B : in std_logic_vector( B_WIDTH-1 downto 0 );	-- Source B, unsigned
		Q : out std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 ) );	-- Result = A*B, unsigned
end entity;

architecture rtl of multuu is
	subtype result_vector is std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 );
	type result_array is array ( 0 to LATENCY ) of result_vector;
	
	function multiply( A, B : std_logic_vector ) return std_logic_vector is
		variable R, ExtA : std_logic_vector( A'length + B'length -1 downto 0 );
	begin
		R := (others=>'0');
		for i in B'range loop
			ExtA := (others=>'0');
			if B(i)='1' then
				ExtA(i+A'length-1 downto i) := A;
			end if;
			R := R + ExtA;
		end loop;
		return R;
	end function;
	
	signal R : result_array := (others=>(others=>'0'));
begin
	R(0) <= multiply( A, B );
	lat: if LATENCY>=1 generate
		R(1 to LATENCY) <= R(0 to LATENCY-1) when rising_edge(CLK);
	end generate;
	Q <= R(LATENCY);
end architecture;

-- Signed multiplier

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;
use work.ak_hardware.all;

entity multss is
	generic (
		A_WIDTH : integer := 16;	-- A source width
		B_WIDTH : integer := 16;	-- B source width
		LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>=0)
	port (
		CLK : in std_logic;			-- Main clock
		A : in std_logic_vector( A_WIDTH-1 downto 0 );	-- Source A, signed
		B : in std_logic_vector( B_WIDTH-1 downto 0 );	-- Source B, signed
		Q : out std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 ) );	-- Result = A*B, signed
end entity;

architecture rtl of multss is
	function Identity( v : std_logic_vector ) return std_logic_vector is
		variable r : std_logic_vector( v'range );
	begin
		r := ( 0=>'1', others=>'0' );
		return r;
	end function;
	
	signal Aeff : std_logic_vector( A_WIDTH-1 downto 0 );
	signal Beff : std_logic_vector( B_WIDTH-1 downto 0 );
	signal Qpre, Qpost : std_logic_vector( A_WIDTH+B_WIDTH-1 downto 0 );
	signal S : std_logic_vector( 0 to LATENCY ) := (others=>'0');
begin
	S(0) <= A(A'high) xor B(B'high);
	lat: if LATENCY>=1 generate
		S(1 to LATENCY) <= S(0 to LATENCY-1) when rising_edge(CLK);
	end generate;
	
	Qpost <= Qpre when S(LATENCY)='0' else (not Qpre)+Identity(Qpre);
	Aeff <= A when A(A'high)='0' else (not A)+Identity(A);
	Beff <= B when B(B'high)='0' else (not B)+Identity(B);
	eng: multuu
		generic map (
			A_WIDTH => A_WIDTH,
			B_WIDTH => B_WIDTH,
			LATENCY => LATENCY )
		port map (
			CLK => CLK,
			A => Aeff,
			B => Beff,
			Q => Qpre );
			
	Q <= Qpost;
end architecture;

-- Mixed multiplier

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;
use work.ak_hardware.all;

entity multsu is
	generic (
		S_WIDTH : integer := 16;	-- S source width
		U_WIDTH : integer := 16;	-- U source width
		LATENCY : integer := DEF_MULT_LATENCY );	-- Calculation latency (>=0)
	port (
		CLK : in std_logic;			-- Main clock
		S : in std_logic_vector( S_WIDTH-1 downto 0 );	-- Source S, signed
		U : in std_logic_vector( U_WIDTH-1 downto 0 );	-- Source U, unsigned
		Q : out std_logic_vector( S_WIDTH+U_WIDTH-1 downto 0 ) );	-- Result = S*U, signed
end entity;

architecture rtl of multsu is
	function Identity( v : std_logic_vector ) return std_logic_vector is
		variable r : std_logic_vector( v'range );
	begin
		r := ( 0=>'1', others=>'0' );
		return r;
	end function;
	
	signal Aeff : std_logic_vector( S_WIDTH-1 downto 0 );
	signal Beff : std_logic_vector( U_WIDTH-1 downto 0 );
	signal Qpre, Qpost : std_logic_vector( S_WIDTH+U_WIDTH-1 downto 0 );
	signal Sig : std_logic_vector( 0 to LATENCY ) := (others=>'0');
begin
	Sig(0) <= S(S'high);
	lat: if LATENCY>=1 generate
		Sig(1 to LATENCY) <= Sig(0 to LATENCY-1) when rising_edge(CLK);
	end generate;
	
	Qpost <= Qpre when Sig(LATENCY)='0' else (not Qpre)+Identity(Qpre);
	Aeff <= S when S(S'high)='0' else (not S)+Identity(S);
	Beff <= U;
	eng: multuu
		generic map (
			A_WIDTH => S_WIDTH,
			B_WIDTH => U_WIDTH,
			LATENCY => LATENCY )
		port map (
			CLK => CLK,
			A => Aeff,
			B => Beff,
			Q => Qpre );
			
	Q <= Qpost;
end architecture;

-- Single port memory

library ieee, work;
use ieee.std_logic_1164.all;
use work.ak_types.all;
use work.ak_hardware.all;

entity spsyncram is
	generic (
		ADDR_WIDTH : integer := 10;
		DATA_WIDTH : integer := 8;
		READ_LATENCY : integer := DEF_RAM_LATENCY;
		INIT_VALUE : string := "*" );
	port (
		CLK	: in std_logic := '0';
		ADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		DI		: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
		WE		: in std_logic := '0';
		DO		: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0') );
end entity;

architecture rtl of spsyncram is
	subtype ram_word is std_logic_vector( DATA_WIDTH-1 downto 0 );
	constant ram_init : ram_word := (others=>DEF_RAM_INIT);
	constant ram_unknown : ram_word := (others=>DEF_RAM_UNKNOWN);
	type sup_ram_array is array ( integer range <> ) of ram_word;
	subtype ram_array is sup_ram_array( 0 to ((2**ADDR_WIDTH)-1) );
	type pipe_array is array ( 0 to READ_LATENCY ) of ram_word;
	
	signal RAM : ram_array := (others=>ram_init);
	signal LeftAddr : integer;
	signal LeftData : pipe_array := (others=>ram_unknown);
begin

	LeftAddr <= to_integer( ADR );
	LeftData(0) <= RAM( LeftAddr ) when LeftAddr>=0 else ram_unknown;
	DO <= LeftData( READ_LATENCY );
	pipe: if READ_LATENCY>0 generate
		LeftData( 1 to READ_LATENCY ) <= LeftData( 0 to READ_LATENCY-1 ) when rising_edge(CLK);
	end generate;	
	RAM( LeftAddr ) <= DI when WE='1' and rising_edge(CLK);
	
end architecture;
	
-- Single port ROM

library ieee, work;
use ieee.std_logic_1164.all;
use work.ak_hardware.all;

entity syncrom is
	generic (
		ADDR_WIDTH : integer := 10;
		DATA_WIDTH : integer := 8;
		READ_LATENCY : integer := DEF_RAM_LATENCY;
		INIT_VALUE : string := "*" );
	port (
		CLK	: in std_logic := '0';
		ADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		DO	: out std_logic_vector( DATA_WIDTH-1 downto 0 ) );
end entity;

library work;
use work.ak_types.all;
use work.romio.all;

architecture rtl of syncrom is
	subtype ram_word is std_logic_vector( DATA_WIDTH-1 downto 0 );
	constant ram_init : ram_word := (others=>DEF_RAM_INIT);
	constant ram_unknown : ram_word := (others=>DEF_RAM_UNKNOWN);
	type sup_ram_array is array ( integer range <> ) of ram_word;
	subtype ram_array is sup_ram_array( -1 to (2**ADDR_WIDTH)-1 );
	type pipe_array is array ( 0 to READ_LATENCY ) of ram_word;
	
	signal RAM : ram_array := (others=>ram_init);
	signal LeftAddr : integer;
	signal LeftData : pipe_array := (others=>ram_unknown);
begin

	LeftAddr <= to_integer( ADR );
	LeftData(0) <= RAM( LeftAddr ) when LeftAddr>=0 else ram_unknown;
	DO <= LeftData( READ_LATENCY );
	pipe: if READ_LATENCY>0 generate
		LeftData( 1 to READ_LATENCY ) <= LeftData( 0 to READ_LATENCY-1 ) when rising_edge(CLK);
	end generate;
	
	init: process
		variable data : int_array( 0 to RAM'high );
	begin
		if INIT_VALUE'length>1 then
			ReadMIF( data, INIT_VALUE );
			RAM(-1) <= ram_unknown;
			for i in 0 to RAM'high loop
				RAM(i) <= to_vector( data(i), RAM(0)'length );
			end loop;
		end if;
		wait;
	end process;
	
end architecture;
	
-- Dual port memory

library ieee, work;
use ieee.std_logic_1164.all;
use work.ak_types.all;
use work.ak_hardware.all;

entity dpsyncram is
	generic (
		ADDR_WIDTH : integer := 10;
		DATA_WIDTH : integer := 8;
		READ_LATENCY : integer := DEF_RAM_LATENCY;
		INIT_VALUE : string := "*" );
	port (
		LCLK	: in std_logic := '0';
		LADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		LDI		: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
		LWE		: in std_logic := '0';
		LDO		: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
		
		RCLK	: in std_logic := '0';
		RADR	: in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		RDI		: in std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0');
		RWE		: in std_logic := '0';
		RDO		: out std_logic_vector( DATA_WIDTH-1 downto 0 ) := (others=>'0') );
end entity;

architecture rtl of dpsyncram is
	subtype ram_word is std_logic_vector( DATA_WIDTH-1 downto 0 );
	constant ram_init : ram_word := (others=>DEF_RAM_INIT);
	constant ram_unknown : ram_word := (others=>DEF_RAM_UNKNOWN);
	type sup_ram_array is array ( integer range <> ) of ram_word;
	subtype ram_array is sup_ram_array( 0 to ((2**ADDR_WIDTH)-1) );
	type pipe_array is array ( 0 to READ_LATENCY ) of ram_word;
	
	signal RAM : ram_array := (others=>ram_init);
	signal LeftAddr, RightAddr : integer;
	signal LeftData, RightData : pipe_array := (others=>ram_unknown);
begin

	LeftAddr <= to_integer( LADR );
	RightAddr <= to_integer( RADR );
	LeftData(0) <= RAM( LeftAddr ) when LeftAddr>=0 else ram_unknown;
	RightData(0) <= RAM( RightAddr ) when RightAddr>=0 else ram_unknown;
	LDO <= LeftData( READ_LATENCY );
	RDO <= RightData( READ_LATENCY );
	
	pipe: if READ_LATENCY>0 generate
		LeftData( 1 to READ_LATENCY ) <= LeftData( 0 to READ_LATENCY-1 ) when rising_edge(LCLK);
		RightData( 1 to READ_LATENCY ) <= RightData( 0 to READ_LATENCY-1 ) when rising_edge(RCLK);
	end generate;
	
	-- Simulated but not syntesized
	process (LCLK, RCLK)
	begin
		if rising_edge(LCLK) then
			if LWE='1' then
				RAM( LeftAddr ) <= LDI;
			end if;
		end if;
		if rising_edge(RCLK) then
			if RWE='1' then
				RAM( RightAddr ) <= RDI;
			end if;
		end if;
	end process;

	-- Syntesized but not simulated
	--RAM( LeftAddr ) <= LDI when rising_edge(LCLK) and LWE='1';
	--RAM( RightAddr ) <= RDI when rising_edge(RCLK) and RWE='1';
	
end architecture;

-- Triple port memory

-- Quad port memory

-- PLL

library ieee;
use ieee.std_logic_1164.all;

entity clkpll is
	generic (
		CLK_REFERENCE : natural := 33554; -- kHz
		CLK_MULTIPLIER : natural := 1;
		CLK_DIVIDER : natural := 1
		);

	port (
		REFCLK	: in std_logic;
		PLLCLK	: out std_logic;
		LOCKED	: out std_logic
		);
end entity;

	architecture rtl of clkpll is
	begin
		process
			constant PLL_OUT_FREQ : integer := (CLK_REFERENCE * CLK_MULTIPLIER)/CLK_DIVIDER;
			constant PLL_PERIOD : time := (1 ms)/PLL_OUT_FREQ;
		begin
			LOCKED <= '0';
			PLLCLK <= '0';
			loop
				wait for PLL_PERIOD/2;
				PLLCLK <= '1';
				wait for PLL_PERIOD-(PLL_PERIOD/2);
				PLLCLK <= '0';
				LOCKED <= '1';
			end loop;
		end process;
	end architecture;

-- Biphase PLL

library ieee;
use ieee.std_logic_1164.all;

entity clkpllbi is
	generic (
		CLK_REFERENCE : natural := 33554; -- kHz
		CLK_MULTIPLIER : natural := 1;
		CLK_DIVIDER : natural := 1;
		CLK0_PHASE : integer := 0;
		CLK1_PHASE : integer := 90 );
	port (
		REFCLK	: in std_logic;
		PLLCLK	: out std_logic;
		PLLCLK90 : out std_logic;
		LOCKED	: out std_logic
		);
end entity;

	architecture rtl of clkpllbi is
	begin
		process
			constant PLL_OUT_FREQ : integer := (CLK_REFERENCE * CLK_MULTIPLIER)/CLK_DIVIDER;
			constant PLL_PERIOD : time := (1 ms)/PLL_OUT_FREQ;
		begin
			LOCKED <= '0';
			PLLCLK <= '0';
			PLLCLK90 <= '0';
			loop
				wait for PLL_PERIOD/4;
				PLLCLK <= '1';
				wait for (PLL_PERIOD/2)-(PLL_PERIOD/4);
				PLLCLK90 <= '1';
				wait for PLL_PERIOD/4;
				PLLCLK <= '0';
				wait for PLL_PERIOD-(PLL_PERIOD/2)-(PLL_PERIOD/4);
				PLLCLK90 <= '0';
				LOCKED <= '1';
			end loop;
		end process;
	end architecture;

--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--END--
