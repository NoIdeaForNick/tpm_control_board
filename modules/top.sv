`timescale 1ns/1ps

`include "packages.sv"

module top(clk, 
			  rx_from_ft232, 
			  tx_to_ft232, 
			  tpm_clk, tpm_ena, tpm_data, tpm_gate, tpm_status, tpm_gate_alias, 
			  leds, 
			  enable_driver_12, enable_driver_34, 
			  external_button, switch);

//common
localparam CLK_FREQUENCY = 100_000_000;
input logic clk;
logic rstn;
assign rstn = 1;

//driver
output logic enable_driver_12, enable_driver_34;
assign enable_driver_12 = 1;
assign enable_driver_34 = 1;

//btn and switch
input logic external_button;
input logic [3:0] switch;

//TPM
localparam TPM_OUTPUT_CLK_FREQUENCY = 10_000_000;
localparam ATTENUATORS_SETTINGS_DATA_WIDTH_BITS = 4;
localparam PHASE_SHIFTERS_SETTINGS_DATA_WIDTH_BITS = 5;


input logic tpm_status;
output logic tpm_clk, tpm_ena, tpm_data, tpm_gate, tpm_gate_alias;

assign tpm_gate = ~tpm_gate_alias;

logic [ATTENUATORS_SETTINGS_DATA_WIDTH_BITS-1:0] tx_attn_value, rx_attn_value;
logic [PHASE_SHIFTERS_SETTINGS_DATA_WIDTH_BITS-1:0] phase_value;
logic [$clog2(common_values::DUTY_CYCLE_MAX_VALUE)-1:0] duty_cycle_value; //not real duty cycle now. Its counter delay between gate back edge and next gate front edge
logic [$clog2(common_values::PULSE_WIDTH_MAX_VALUE_US)-1:0] pulse_width_value;
logic start_tpm_session;
logic is_tpm_module_rdy;
logic emitting_request;
logic cycle_emission;

tpm
#(.INPUT_CLK(CLK_FREQUENCY),
  .TARGET_TRM_CLK(TPM_OUTPUT_CLK_FREQUENCY))
  
tpm_signals_generator  
 (.clk(clk), 
  .rstn(rstn), 
  .tx_attn(tx_attn_value), 
  .rx_attn(rx_attn_value), 
  .phase(phase_value), 
  .start(start_tpm_session),
  .duty_cycle_value(duty_cycle_value),
  .pulse_width_value(pulse_width_value),
  .emitting_request(emitting_request),
  .cycle_emission(cycle_emission), 
  .rdy(is_tpm_module_rdy), 
  .tpm_clk(tpm_clk), 
  .tpm_ena(tpm_ena), 
  .tpm_data(tpm_data), 
  .tpm_gate(tpm_gate_alias));

//leds
output logic [2:0] leds;
assign leds[0] = tpm_status;
assign leds[1] = ~tpm_status;

//uart wrapper ft232
localparam QTY_OF_BYTES_SENDING_BACK_TO_PC = 2;
localparam BAUDRATE = 3_000_000;

input logic rx_from_ft232;
output logic tx_to_ft232;

logic new_data_from_uart_has_come;
logic start_uart_tx_event;
logic [QTY_OF_BYTES_SENDING_BACK_TO_PC*8-1:0] data_vector_to_uart;
logic uart_transmitter_is_busy;

connections::uart_wrapper_connection_t data_from_wrapper;

slave_packet_transceiver
#(.INPUT_BUS_WIDTH(QTY_OF_BYTES_SENDING_BACK_TO_PC*8),
  .BAUDRATE(BAUDRATE),
  .CLK_RATE(CLK_FREQUENCY))
  
uart_wrapper  
 (.clk(clk),
  .rstn(rstn),
  .data_vector(data_vector_to_uart),
  .data_from_packet(data_from_wrapper),
  .new_data_strobe(new_data_from_uart_has_come),
  .busy(uart_transmitter_is_busy),
  .led(),
  .tx(tx_to_ft232),
  .rx(rx_from_ft232),
  .event_flag(start_uart_tx_event));

//**************LINK LED*****************	 
always @(negedge rstn, posedge new_data_from_uart_has_come)
begin
	if(~rstn) leds[2] <= 1;	
	else leds[2] <= ~leds[2];
end

initial 	
begin
	leds[2] = 1;
end	

//*************TPM status and BTN************
logic status_buffered1, status_buffered2;
logic btn_buffered1, btn_buffered2;
logic [3:0] switch_buffered1, switch_buffered2;

always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		status_buffered1 <= 0;
		status_buffered2 <= 0;
		btn_buffered1 <= 0;
		btn_buffered2 <= 0;
		switch_buffered1 <= 0;
		switch_buffered2 <= 0;
	end
	else
	begin
		status_buffered1 <= tpm_status;
		status_buffered2 <= status_buffered1;
		btn_buffered1 <= external_button;
		btn_buffered2 <= btn_buffered1;
		switch_buffered1 <= switch;
		switch_buffered2 <= switch_buffered1;
	end
end

initial
begin
	status_buffered1 = 0;
	status_buffered2 = 0;
	btn_buffered1 = 0;
	btn_buffered2 = 0;
	switch_buffered1 = 0;
	switch_buffered2 = 0;
end

logic btn_goes_up;
assign btn_goes_up = (btn_buffered1 && ~btn_buffered2);  
  
//*************RX strobe catch************
logic  new_data_rx_catch1, new_data_rx_catch2;

always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		new_data_rx_catch1 <= 0;
		new_data_rx_catch2 <= 0;
	end
	else
	begin
		new_data_rx_catch1 <= new_data_from_uart_has_come;
		new_data_rx_catch2 <= new_data_rx_catch1;
	end
end

initial
begin
	new_data_rx_catch1 = 0;
	new_data_rx_catch2 = 0;
end

logic new_data_from_uart_goes_up; 
assign new_data_from_uart_goes_up = (new_data_rx_catch1 && ~new_data_rx_catch2);


//*************main cycle***************
localparam MDR_CMD = 8'b1000_0000,
           MTX_DUTY_CYCLE_PULSE_WIDTH_CMD = 24'b0000_0000_0011_1000_0000_0000;
			  
localparam MDR_CMD_MASK = 8'b1000_0000, //hides device id + information bits			  
			  MTX_DUTY_CYCLE_PULSE_WIDTH_CMD_MASK = 24'b0000_0000_0011_1000_0000_0000;
			  
localparam MDR_DATA_MASK = 8'b0001_1111; //hides cmd bits + device id			  
			  
localparam PHASE_SHIFTER_ID = 8'b0000_0000, //hide cmd bits + information bits
			  RX_ATTENUATOR_ID = 8'b0010_0000,
			  TX_ATTENUATOR_ID = 8'b0100_0000;
			  
localparam DEVICE_ID_MASK = 8'b0110_0000;

localparam MTX_DATA_BIT_CYCLE = 24'b0000_0000_0000_0010_0000_0000,
			  MTX_DATA_BIT_EMIT  = 24'b0000_0000_0000_0001_0000_0000,
			  MTX_DATA_BIT_SEND_TO_TPM_OR_SET_HERE = 24'b0000_0000_0000_0100_0000_0000;
		
localparam DUTY_CYCLE_LSB_POSITION = 14,
			  DUTY_CYCLE_MSB_POSITION = 23;
			  
logic [7:0] uart_data_buffer;
logic is_it_not_first_btn_press;

enum logic [2:0] {enIDLE_TOP_MACHINE, enBTN_PARSING_TOP_MACHINE, enPARSING_CMD_TOP_MACHINE, enSTART_SESSION_WITH_TPM_TOP_MACHINE, enREPLY_TO_PC_TOP_MACHINE} main_state;

always @(posedge clk, negedge rstn)
begin
	if(~rstn) ResetTopMachine();
	else
	begin
		
		case(main_state)

		
		enIDLE_TOP_MACHINE:
		begin
			if(new_data_from_uart_goes_up)
			begin
				$display("Got new data from PC");
				main_state <= enPARSING_CMD_TOP_MACHINE;
			end
			else if(btn_goes_up) 
					begin
						if(is_it_not_first_btn_press)
						begin
							$display("btn was pressed!");
							main_state <= enBTN_PARSING_TOP_MACHINE;
						end
						else is_it_not_first_btn_press <= 1;
					end	
		end
		
		enBTN_PARSING_TOP_MACHINE:
		begin
			emitting_request <= 1;
			phase_value <= {1'b0, switch_buffered2};
			rx_attn_value <= switch_buffered2;
			tx_attn_value <= switch_buffered2;
			main_state <= enSTART_SESSION_WITH_TPM_TOP_MACHINE;
		end

		
		enPARSING_CMD_TOP_MACHINE:
		begin:parsing_cmd
		
			//MTX DUTY PULSE CMD
			if((data_from_wrapper.mtx_duty_cycle_pulse_width & MTX_DUTY_CYCLE_PULSE_WIDTH_CMD_MASK) == MTX_DUTY_CYCLE_PULSE_WIDTH_CMD)
			begin
				$display("MTX cmd is valid");
				
				if(data_from_wrapper.mtx_duty_cycle_pulse_width & MTX_DATA_BIT_EMIT)	
				begin
					$display("Got emitting request!");
					emitting_request <= 1;			
				end	
				else 
				begin
					$display("No emitting request");
					emitting_request <= 0;
				end
				
				if(data_from_wrapper.mtx_duty_cycle_pulse_width & MTX_DATA_BIT_CYCLE)
				begin
					$display("Setting cycle emission");
					cycle_emission <= 1;
				end
				else
				begin
					$display("Resetting cycle emission");
					cycle_emission <= 0;					
				end
			
				if(data_from_wrapper.mtx_duty_cycle_pulse_width & MTX_DATA_BIT_SEND_TO_TPM_OR_SET_HERE)
				begin
					$display("Sending data to TPM");
					main_state <= enSTART_SESSION_WITH_TPM_TOP_MACHINE;
				end
				else
				begin
					$display("Just setting new data to FPGA regs");
					main_state <= enREPLY_TO_PC_TOP_MACHINE;
				end
				
				//DUTY CYCLE		
				duty_cycle_value <= data_from_wrapper.mtx_duty_cycle_pulse_width[DUTY_CYCLE_MSB_POSITION:DUTY_CYCLE_LSB_POSITION]; //truncate excess data
				$display("Set new duty cycle value=%h", data_from_wrapper.mtx_duty_cycle_pulse_width[DUTY_CYCLE_MSB_POSITION:DUTY_CYCLE_LSB_POSITION]);		
			
				//PULSE WIDTH
				pulse_width_value <= data_from_wrapper.mtx_duty_cycle_pulse_width[7:0];
				$display("Set new pulse width value=%h", data_from_wrapper.mtx_duty_cycle_pulse_width[7:0]);				
				
			end
			else 
			begin
				emitting_request <= 0;
				main_state <= enREPLY_TO_PC_TOP_MACHINE;
			end	
							
			//MDR PHASE CMD				
			if(data_from_wrapper.mdr_phase & MDR_CMD_MASK)
			begin
				$display("MDR phase cmd is valid");
				if((data_from_wrapper.mdr_phase & DEVICE_ID_MASK) == PHASE_SHIFTER_ID)
				begin
					phase_value <= data_from_wrapper.mdr_phase & MDR_DATA_MASK;
					$display("Set new phase value=%h", data_from_wrapper.mdr_phase & MDR_DATA_MASK);
				end
			end
	
			//MDR ATT RX
			if(data_from_wrapper.mdr_att_rx & MDR_CMD_MASK)
			begin
				$display("MDR att rx cmd is valid");
				if((data_from_wrapper.mdr_att_rx & DEVICE_ID_MASK) == RX_ATTENUATOR_ID)
				begin
					rx_attn_value <= data_from_wrapper.mdr_att_rx & MDR_DATA_MASK;
					$display("Set new att rx value=%h", data_from_wrapper.mdr_att_rx & MDR_DATA_MASK);
				end
			end
			
			//MDR ATT TX
			if(data_from_wrapper.mdr_att_tx & MDR_CMD_MASK)
			begin
				$display("MDR att tx cmd is valid");
				if((data_from_wrapper.mdr_att_tx & DEVICE_ID_MASK) == TX_ATTENUATOR_ID)
				begin
					tx_attn_value <= data_from_wrapper.mdr_att_tx & MDR_DATA_MASK;
					$display("Set new att tx value=%h", data_from_wrapper.mdr_att_tx & MDR_DATA_MASK);
				end
			end
							
		end:parsing_cmd

		
		enSTART_SESSION_WITH_TPM_TOP_MACHINE:
		begin
			if(~start_tpm_session)
			begin
				if(is_tpm_module_rdy) start_tpm_session <= 1;
			end
			else
			begin
				start_tpm_session <= 0;
				main_state <= enREPLY_TO_PC_TOP_MACHINE;	
			end	
		end

		
		enREPLY_TO_PC_TOP_MACHINE:
		begin
			if(~start_uart_tx_event)
			begin
				if(~uart_transmitter_is_busy)
				begin
					$display("Sending reply to PC");
					data_vector_to_uart <= { status_buffered2, emitting_request, cycle_emission, phase_value,					// 1 byte
													 tx_attn_value, rx_attn_value }; 															// 2 byte
					start_uart_tx_event <= 1;					
				end
			end
			else
			begin
				start_uart_tx_event <= 0;
				main_state <= enIDLE_TOP_MACHINE;
			end
		end

		
		default:;
		
		endcase
		
	end
end

initial ResetTopMachine();

function void ResetTopMachine();
	tx_attn_value = 0;
	rx_attn_value = 0;
	phase_value = 0;
	duty_cycle_value = 0;
	pulse_width_value = 0;
	start_tpm_session = 0;
	emitting_request = 0;
	cycle_emission = 0;
	start_uart_tx_event = 0;
	data_vector_to_uart = 0;
	uart_data_buffer = 0;
	main_state = enIDLE_TOP_MACHINE;
	is_it_not_first_btn_press = 0;
endfunction

endmodule
