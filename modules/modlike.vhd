--------------------------------------------------------------------------------------------
-- MODBUS-LIKE UART INTERFACE -- MODBUS-LIKE UART INTERFACE -- MODBUS-LIKE UART INTERFACE --
--------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;

package modlike is

-- Default values ------------------------------------------------------------
	
	subtype mod_data_type is std_logic_vector( 7 downto 0 );
	subtype mod_addr_type is std_logic_vector( 15 downto 0 );
	
	constant MOD_DATA_BITS		: integer := 8;			-- 1..32
	constant MOD_PARITY_MODE	: string := "NONE";		-- NONE, EVEN, ODD, MARK, SPACE
	constant MOD_STOP_BITS		: integer := 1;			-- 0, 1, 2, ...
	constant MOD_DATA_DIR		: string := "LSB";		-- LSB, MSB
	constant MOD_BAUD_DIVIDER	: integer := 8;		-- Rate = Fclk/BAUD_DIVIDER/3
	constant MOD_REAL_DIVIDER	: real := 25.0;		-- Rate = Fclk/REAL_DIVIDER
	constant MOD_OVERSAMPLING	: string := "x3";		-- Bit oversampling x3, x4
	constant MOD_DEF_ADDR 		: integer := 16#5A#;	-- Default MODBUS address
	constant MOD_DEF_LATENCY	: integer := 4;			-- Default bus read latency
	constant MOD_DUPLEX			: boolean := TRUE;		-- Default data direction mode

-- Internal constants --------------------------------------------------------
	
	constant MOD_SOF_MARKER		: mod_data_type := X"3A";
	constant MOD_CR_MARKER		: mod_data_type := X"0D";
	constant MOD_LF_MARKER		: mod_data_type := X"0A";	
	constant MOD_WRITE_REGS 	: mod_data_type := X"65";
	constant MOD_READ_REGS		: mod_data_type := X"64";
	constant MOD_ERR_COMMAND	: mod_data_type := X"01";
	constant MOD_ERR_FORMAT		: mod_data_type := X"03";

-- Components ----------------------------------------------------------------
	
	component ModDigitalPll is
		generic (
			OVERSAMPLING : string := MOD_OVERSAMPLING );	-- supported x3, x4
		port (
			CLK : in std_logic;			-- Global clock
			RSTn : in std_logic := '1';	-- Async. reset
			EN : in std_logic := '1';	-- Clock enable/freq. divider input
			SDI : in std_logic;			-- Async. data input
			INH : in std_logic := '0';	-- Input data inhibit
			SDO : out std_logic;		-- Synchronous data output
			SCK : out std_logic );		-- Synchronous clock/data enable
	end component;
	
	component ModDeserializer is
		generic (
			DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
			PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
			STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
			DATA_DIR		: string := MOD_DATA_DIR);		-- LSB, MSB
		port (
			CLK 	: in std_logic := '1';	-- Global clock
			RSTn	: in std_logic := '1';	-- Async. reset
			EN		: in std_logic := '1';	-- Serial clock enable (continious)
			SDI		: in std_logic;			-- Serial data input
			DO		: out std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data output
			RDYO	: out std_logic;		-- Parallel data output ready
			RDYI	: in std_logic := '1';	-- Data acquisition input
			PE		: out std_logic;		-- Parity error flag
			FE		: out std_logic );		-- Frame error flag
	end component;

	component ModSerializer is
		generic (
			DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
			PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
			STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
			DATA_DIR		: string := MOD_DATA_DIR);		-- LSB, MSB
		port (
			CLK 	: in std_logic := '1';	-- Global clock
			RSTn	: in std_logic := '1';	-- Async. reset
			EN		: in std_logic := '1';	-- Serial clock enable/freq. divider
			SDO		: out std_logic;		-- Serial data output
			SDE		: out std_logic;		-- Serial data enable output
			DI		: in std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data input
			SI		: in std_logic := '0';	-- Enable silense transmittion/data disable
			RDYO	: out std_logic;			-- Input register ready output
			RDYI	: in std_logic := '1' );	-- Data ready input
	end component;

	component ModIntDivider is
		generic (
			DIVIDER : integer := MOD_BAUD_DIVIDER );
		port (
			CLK : in std_logic;			-- Main clock
			RSTn : in std_logic := '1';	-- Async. reset
			EN : in std_logic := '1';	-- Output & divide enable
			Q : out std_logic );		-- Output pulses
	end component;
	
	component ModFractDivider is
		generic (
			DIVIDER : real := 3.0*real(MOD_BAUD_DIVIDER);
			PRECESION : real := 200.0 );
		port (
			CLK : in std_logic;			-- Main clock
			RSTn : in std_logic := '1';	-- Async. reset
			EN : in std_logic := '1';	-- Output & divide enable
			Q : out std_logic );		-- Output pulses
	end component;

	component ModTranceiver is
		generic (
			DUPLEX			: boolean := MOD_DUPLEX;		-- Enable/disable duplex mode
			DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
			PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
			STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
			DATA_DIR		: string := MOD_DATA_DIR;		-- LSB, MSB
			BAUD_DIVIDER	: integer := 8;					-- Rate = Fclk/BAUD_DIVIDER/3
			REAL_DIVIDER	: real := 25.0 );				-- Rate = Fclk/BAUD_DIVIDER
		port (
			CLK		: in std_logic;			-- Main clock
			RSTn	: in std_logic := '1';	-- Async. reset (0)
			
			TX		: out std_logic;		-- Serial output
			RX		: in std_logic := '1';	-- Serial input
			TE		: out std_logic;		-- Serial output enable
			
			TDi		: in std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data input
			TRDYi	: in std_logic := '0';		-- TX data ready input
			TRDYo	: out std_logic;			-- TX input register empty output
			
			RDo		: out std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data output
			RRDYi	: in std_logic := '1';	-- Data acquisition input
			RRDYo	: out std_logic;		-- Parallel data output ready
			PE		: out std_logic;		-- Parity error flag
			FE		: out std_logic );		-- Frame error flag
	end component;

	component ModDecoder is
		generic (
			ADDR_WIDTH : integer := 8;				-- "Packet size"
			MOD_ADDR : integer := MOD_DEF_ADDR );	-- Module address
		port (
			CLK : in std_logic;				-- Global clock
			RSTn : in std_logic := '1';		-- Async. reset
			
			DI : in mod_data_type;			-- Data input
			DIE : in std_logic := '1';		-- Input data ready (ack==1)
			DERR : in std_logic := '0';		-- Input data error flag
			
			DO : out mod_data_type;		-- Packet data out
			DOE : out std_logic;		-- Packet data write enable
			AO : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	--Packet data address
			
			RDYO : out std_logic;			-- Packet ready
			RDYI : in std_logic := '1';		-- Processor acknoledge
			
			RAM_AI : in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			RAM_DO : out mod_data_type );	-- Packet buffer interface
	end component;

	component ModCoder is
		generic (
			ADDR_WIDTH : integer := 8;				-- "Packet size"
			MOD_ADDR : integer := MOD_DEF_ADDR );	-- Module address
		port (
			CLK : in std_logic;				-- Global clock
			RSTn : in std_logic := '1';		-- Async. reset
			
			RAM_AI : in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
			RAM_DI : in mod_data_type := (others=>'0');-- Packet RAM data
			RAM_WE : in std_logic := '0';		-- Packet RAM write enable
			
			PktReq : in std_logic := '1';	-- Packet send request
			PktAck : out std_logic;			-- Packet send acknoledge
			
			TXD : out mod_data_type;	-- Data to transmit
			RDYO : out std_logic;		-- TXD enable
			RDYI : in std_logic := '1');-- Transmitter acknoledge
	end component;

	component ModProcessor is
		generic (
			ADDR_WIDTH : integer := 8;					-- "Packet Size"
			READ_LATENCY : integer := MOD_DEF_LATENCY );	-- Registers read delay
		port (
			CLK : in std_logic;				-- Global clock
			RSTn : in std_logic := '1';		-- Global async. reset
			
			RXD : in mod_data_type;			-- Received packet RAM data
			RXA : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	-- Rx RAM address
			RXS : in std_logic_vector( ADDR_WIDTH-1 downto 0 );		-- Rx packet size
			RXReq : in std_logic;	-- Received packet ready
			RXAck : out std_logic;	-- Acknoledge to RXReq

			TXD : out mod_data_type;		-- Transmitter packet RAM data
			TXA : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	-- Tx RAM address
			TXW : out std_logic;			-- Tx RAM write enable
			TXReq : out std_logic;			-- Tx packet ready request
			TXAck : in std_logic := '1';	-- Acknoledge to TXReq
			
			RegA : out mod_addr_type;			-- Register R/W address
			RegW : out std_logic;				-- Register write strobe
			RegDO : out mod_data_type;					-- Register write data
			RegDI : in mod_data_type := (others=>'1'));-- register read data
	end component;

	component ModControl is
		generic (
			DUPLEX : boolean := MOD_DUPLEX;				-- Enable/disable duplex operation
			BAUD_DIVIDER : integer := 0;				-- Rate = CLK/3/BAUD_DIVIDER
			REAL_DIVIDER : real := 0.0;					-- Rate = CLK/REAL_DIVIDER
			MOD_ADDR : integer := MOD_DEF_ADDR;			-- Module address
			READ_LATENCY : integer := MOD_DEF_LATENCY );	-- Register read delay, CLKs
		port (
			CLK : in std_logic;			-- Global clock
			RSTn : in std_logic := '1';	-- Async. reset
			
			RXD : in std_logic;			-- Serial data input
			TXD : out std_logic;		-- Serial data output
			TXE : out std_logic;		-- Transmittion enable
			
			AO : out mod_addr_type;	-- Register address
			WE : out std_logic;		-- Register write enable
			DO : out mod_data_type;	-- Register write data
			DI : in mod_data_type;	-- Register read data
	
			UP : out std_logic );	-- Register update indicator
	end component;

end package;

--package body modlike is
--end package body;

------------------------------------------------------------------------------------------
-- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL -- DPLL --
------------------------------------------------------------------------------------------

library ieee,work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModDigitalPll is
	generic (
		OVERSAMPLING : string := MOD_OVERSAMPLING );	-- supported x3, x4
	port (
		CLK : in std_logic;			-- Global clock
		RSTn : in std_logic := '1';	-- Async. reset
		EN : in std_logic := '1';	-- Clock enable/freq. divider input
		SDI : in std_logic;			-- Async. data input
		INH : in std_logic := '0';	-- Input data inhibit
		SDO : out std_logic;		-- Synchronous data output
		SCK : out std_logic );		-- Synchronous clock/data enable
end entity;

architecture rtl of ModDigitalPll is

	type RECEIVER_STATE is (
		Sample,
		Slower,
		Normal,
		Faster,
		SkipX4 );
		
	signal RXState, NextState : RECEIVER_STATE;
	signal DataTrack : std_logic_vector( 2 downto 0 ) := (others=>'1');
	signal DataEnable : boolean;
	signal InReg : std_logic;
	
begin

	assert	OVERSAMPLING/="x3" or
			OVERSAMPLING/="x4"
		report "Unsupported OVERSAMPLING parameter" severity ERROR;
		
	x3ovs: if OVERSAMPLING="x3" generate
		NextState <= Sample;
	end generate;
	x4ovs: if OVERSAMPLING="x4" generate
		NextState <= SkipX4;
	end generate;

	
	dpllrtl: process ( CLK, RSTn )
	begin
		if RSTn='0' then
			RXState <= Slower;
			DataTrack <= (others=>'1');
			DataEnable <= FALSE;
			SDO <= '1';
			InReg <= '0';
			
		elsif CLK'event and CLK='1' then
			if EN='1' then
				InReg <= SDI;
				DataTrack(0) <= InReg or INH;
				DataTrack(2 downto 1) <= DataTrack(1 downto 0);
				DataEnable <= ( RXState=Sample );
				case RXState is
					when Sample =>
						case DataTrack is
							when "100" | "011" =>
								RXState <= Slower;
							when "110" | "001" =>
								RXState <= Faster;
							when others =>
								RXState <= Normal;
						end case;
					
					when Slower =>
						RXState <= Normal;
					when Normal =>
						RXState <= Faster;
					when Faster =>
						RXState <= NextState;
					when SkipX4 =>
						RXState <= Sample;
				end case;
				
			else
				DataEnable <= FALSE;
			end if;
			
			if DataEnable then
				SDO <= DataTrack(2);
			end if;
			
		end if;
	end process;
	
	SCK <= '1' when DataEnable else '0';
	
end architecture rtl;

--------------------------------------------------------------------------------------
-- RECEIVER -- RECEIVER -- RECEIVER -- RECEIVER -- RECEIVER -- RECEIVER -- RECEIVER --
--------------------------------------------------------------------------------------

library ieee,work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModDeserializer is
	generic (
		DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
		PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
		STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
		DATA_DIR		: string := MOD_DATA_DIR);		-- LSB, MSB
	port (
		CLK 	: in std_logic := '1';	-- Global clock
		RSTn	: in std_logic := '1';	-- Async. reset
		EN		: in std_logic := '1';	-- Serial clock enable (continious)
		SDI		: in std_logic;			-- Serial data input
		DO		: out std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data output
		RDYO	: out std_logic;		-- Parallel data output ready
		RDYI	: in std_logic := '1';	-- Data acquisition input
		PE		: out std_logic;		-- Parity error flag
		FE		: out std_logic );		-- Frame error flag
end entity;

architecture rtl of ModDeserializer is

	pure function GetSize(
		DATA_BITS : integer;
		PARITY_MODE : string )
		return integer is
	begin
		if	PARITY_MODE="EVEN" or
			PARITY_MODE="ODD" or
			PARITY_MODE="MARK" or
			PARITY_MODE="SPACE" then
			return DATA_BITS+2;
		elsif PARITY_MODE="NONE" then
			return DATA_BITS+1;
		else
			report "Unknown PARITY_MODE parameter value" severity ERROR;
			return 9;
		end if;
	end function;
	
	pure function SwapBits(
		DIN : std_logic_vector )
		return std_logic_vector is
		variable Dreg : std_logic_vector ( DIN'range );
	begin
		for i in DIN'range loop
			Dreg( DIN'low+i ) := DIN( DIN'high-i );
		end loop;
		return Dreg;
	end function;
	
	pure function InitParity(
		DATA_BITS : integer )
		return std_logic is
	begin
		if ((DATA_BITS/2)*2)=DATA_BITS then
			return '1';
		else
			return '0';
		end if;
	end;
			
	constant SH_SIZE : integer := GetSize( DATA_BITS, PARITY_MODE );
	signal ShiftReg : std_logic_vector( SH_SIZE-1 downto 0 );
	alias StartBit : std_logic is ShiftReg( SH_SIZE-1 );
	signal Parity : std_logic;

begin
	
	deserrtl: process ( CLK, RSTn )
	variable DataOut : std_logic_vector( DATA_BITS-1 downto 0 );
	begin
		if RSTn='0' then
			RDYO <= '0';
			PE <= '0';
			FE <= '0';
			Parity <= InitParity( DATA_BITS );
			DO <= (others=>'0');
			ShiftReg <= (others=>'1');
			
		elsif CLK'event and CLK='1' then
			if EN='1' then
				ShiftReg(0) <= SDI;
				if StartBit='0' then
					DataOut := ShiftReg( SH_SIZE-2 downto SH_SIZE-1-DATA_BITS );
					if DATA_DIR="MSB" then
						DO <= DataOut;
					elsif DATA_DIR="LSB" then
						DO <= SwapBits( DataOut );
					else
						report "Unknown DATA_DIR parameter value" severity ERROR;
					end if;
					ShiftReg( SH_SIZE-1 downto 1 ) <= (others=>'1');
					
					RDYO <= '1';
					Parity <= SDI xor not InitParity( DATA_BITS );
					FE <= not SDI;
					
					if PARITY_MODE="ODD" then
						PE <= Parity;
					elsif PARITY_MODE="EVEN" then
						PE <= not Parity;
					elsif PARITY_MODE="MARK" then
						PE <= ShiftReg(0);
					elsif PARITY_MODE="SPACE" then
						PE <= not ShiftReg(0);
					else
						PE <= '0';
					end if;
				else
					ShiftReg( SH_SIZE-1 downto 1 ) <= ShiftReg( SH_SIZE-2 downto 0 );
					Parity <= Parity xor not SDI;
					if RDYI='1' then
						RDYO <= '0';
					end if;
				end if;
			else
				if RDYI='1' then
					RDYO <= '0';
				end if;
			end if;
			
		end if;
	end process;
	
end architecture;

--------------------------------------------------------------------------------------------
-- TRANSMITTER -- TRANSMITTER -- TRANSMITTER -- TRANSMITTER -- TRANSMITTER -- TRANSMITTER --
--------------------------------------------------------------------------------------------

library ieee,work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModSerializer is
	generic (
		DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
		PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
		STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
		DATA_DIR		: string := MOD_DATA_DIR);		-- LSB, MSB
	port (
		CLK 	: in std_logic := '1';	-- Global clock
		RSTn	: in std_logic := '1';	-- Async. reset
		EN		: in std_logic := '1';	-- Serial clock enable/freq. divider
		SDO		: out std_logic;		-- Serial data output
		SDE		: out std_logic;		-- Serial data enable output
		DI		: in std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data input
		SI		: in std_logic := '0';	-- Enable silense transmittion/data disable
		RDYO	: out std_logic;			-- Input register ready output
		RDYI	: in std_logic := '1' );	-- Data ready input
end entity;

architecture rtl of ModSerializer is
	
	pure function GetSize(
		DATA_BITS : integer;
		PARITY_MODE : string )
		return integer is
	begin
		if	PARITY_MODE="EVEN" or
			PARITY_MODE="ODD" or
			PARITY_MODE="MARK" or
			PARITY_MODE="SPACE" then
			return DATA_BITS+2;
		elsif PARITY_MODE="NONE" then
			return DATA_BITS+1;
		else
			report "Unknown PARITY_MODE parameter value" severity ERROR;
			return 9;
		end if;
	end function;
	
	pure function SwapBits(
		DIN : std_logic_vector )
		return std_logic_vector is
		variable Dreg : std_logic_vector ( DIN'range );
	begin
		for i in DIN'range loop
			Dreg( DIN'low+i ) := DIN( DIN'high-i );
		end loop;
		return Dreg;
	end function;
	
	pure function InitParity(
		PARITY_MODE : string )
		return std_logic is
	begin
		if	PARITY_MODE="EVEN" or
			PARITY_MODE="MARK" then
			return '0';
		elsif
			PARITY_MODE="ODD" or
			PARITY_MODE="SPACE" or
			PARITY_MODE="NONE" then
			return '1';
		else
			report "Unknown PARITY_MODE parameter value" severity ERROR;
			return '1';
		end if;
	end;
			
	constant SH_SIZE : integer := GetSize( DATA_BITS, PARITY_MODE );
	signal ShiftReg : std_logic_vector( DATA_BITS downto 0 );
	signal DataReg : std_logic_vector( DATA_BITS-1 downto 0 );
	signal Parity, InputEmpty, SilenceReg, SilenceTX, EnablePre, EnableOut, SerialOut : std_logic;
	
	constant StateInit		: integer := SH_SIZE + STOP_BITS - 1;
	constant StateParity	: integer := STOP_BITS + 1;
	constant StateReady		: integer := 0;
	signal State : integer range 0 to StateInit;

begin
	
	serrtl: process ( CLK, RSTn )
	variable DataOut : std_logic_vector( DATA_BITS-1 downto 0 );
	begin
		if RSTn='0' then
			Parity <= InitParity( PARITY_MODE );
			InputEmpty <= '1';
			DataReg <= (others=>'1');
			ShiftReg <= (others=>'1');
			SilenceReg <= '1';
			SilenceTX <= '1';
			EnableOut <= '0';
			EnablePre <= '0';
			SerialOut <= '1';
			
		elsif CLK'event and CLK='1' then
			if RDYI='1' and InputEmpty='1' then
				DataReg <= DI;
				SilenceReg <= SI;
				InputEmpty <='0';
			elsif EN='1' and State=StateReady then
				InputEmpty <= '1';
			end if;
				
			if EN='1' then
				if State=StateReady then
					if InputEmpty='0' then
						if DATA_DIR="MSB" then
							DataOut := DataReg;
						elsif DATA_DIR="LSB" then
							DataOut := SwapBits( DataReg );
						else
							report "Unknown DATA_DIR parameter value" severity ERROR;
						end if;
						
						ShiftReg <= ( '0' & DataOut );
						EnablePre <= not SilenceReg;
						EnableOut <= EnableOut or (not SilenceReg);
						SilenceTX <= SilenceReg;
						Parity <= InitParity( PARITY_MODE );
						State <= StateInit;
					else
						ShiftReg( DATA_BITS downto 1 ) <= ShiftReg( DATA_BITS-1 downto 0 );
						ShiftReg(0) <= '1';
						EnablePre <= '0';
						EnableOut <= EnablePre;
					end if;
				else
					EnableOut <= EnablePre;
					State <= State-1;
					Parity <= Parity xor ShiftReg( DATA_BITS-1 );
					ShiftReg( DATA_BITS-1 downto 1 ) <= ShiftReg( DATA_BITS-2 downto 0 );
					ShiftReg(0) <= '1';
					
					if PARITY_MODE/="NONE" and State=StateParity then
						if PARITY_MODE="EVEN" or PARITY_MODE="ODD" then
							ShiftReg( DATA_BITS ) <= Parity;
						elsif PARITY_MODE="MARK" then
							ShiftReg( DATA_BITS ) <= '0';
						else
							ShiftReg( DATA_BITS ) <= ShiftReg( DATA_BITS-1 );
						end if;
					else
						ShiftReg( DATA_BITS ) <= ShiftReg( DATA_BITS-1 );
					end if;
					
				end if;
				SerialOut <= ShiftReg(DATA_BITS) or SilenceTX;
			end if;
			
		end if;
	end process;

	RDYO <= InputEmpty;
	SDO <= SerialOut;
	SDE <= EnableOut;
end;

--------------------------------------------------------------------------------------------------------
-- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER -- FRACT.DIVIDER --
--------------------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModFractDivider is
	generic (
		DIVIDER : real := 3.0*real(MOD_BAUD_DIVIDER);
		PRECESION : real := 200.0 );
	port (
		CLK : in std_logic;			-- Main clock
		RSTn : in std_logic := '1';	-- Async. reset
		EN : in std_logic := '1';	-- Output & divide enable
		Q : out std_logic );		-- Output pulses
end entity;

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

architecture rtl of ModFractDivider is

	constant ADDER_WIDTH : integer := integer( ceil( log2(PRECESION) ) );
	constant COUNTER_WIDTH : integer := ADDER_WIDTH + integer( ceil( log2(DIVIDER) ) ) - 1;
	constant ADDER_VALUE : integer := integer((2.0**real(COUNTER_WIDTH))/DIVIDER);
	constant ADDER : std_logic_vector( ADDER_WIDTH-1 downto 0 ) := conv_std_logic_vector( ADDER_VALUE, ADDER_WIDTH );
	
	signal Counter : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
	signal EnOut, Ovf, OvfOut : std_logic;
	
begin
	process ( CLK, RSTn )
		variable AIN, AOUT : std_logic_vector( COUNTER_WIDTH downto 0 );
	begin
		if RSTn='0' then
			Counter <= (others=>'0');
			EnOut <= '0';
			Ovf <= '0';
			OvfOut <= '0';
		elsif CLK'event and CLK='1' then
		
			EnOut <= EN;
			OvfOut <= Ovf and EnOut;
			
			if EN='1' then
				AIN( AIN'high downto ADDER'length ) := (others=>'0');
				AIN( ADDER'range ) := ADDER;
				AOUT := AIN + ('0' & Counter);
				
				Counter <= AOUT(Counter'range);
				Ovf <= AOUT(AOUT'high);
			else
				Ovf <= '0';
			end if;
			
		end if;
	end process;
	
	Q <= OvfOut;
	
end architecture;
		
-----------------------------------------------------------------------------------------------------------
-- INT.DIVIDER -- INT.DIVIDER -- INT.DIVIDER -- INT.DIVIDER -- INT.DIVIDER -- INT.DIVIDER -- INT.DIVIDER --
-----------------------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModIntDivider is
	generic (
		DIVIDER : integer := MOD_BAUD_DIVIDER );
	port (
		CLK : in std_logic;			-- Main clock
		RSTn : in std_logic := '1';	-- Async. reset
		EN : in std_logic := '1';	-- Output & divide enable
		Q : out std_logic );		-- Output pulses
end entity;

library ieee;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

architecture rtl of ModIntDivider is
	signal Counter : integer range 0 to DIVIDER-1 := DIVIDER-1;
	signal Overflow : std_logic;
begin
	indiv: process ( CLK, RSTn )
	begin
		if RSTn='0' then
			Counter <= DIVIDER-1;
			Overflow <= '0';
			Q <= '0';
		elsif CLK'event and CLK='1' then
			if EN='1' then
				if Counter=1 then
					Overflow <= '1';
				else
					Overflow <= '0';
				end if;
				if Overflow='1' then
					Counter <= DIVIDER-1;
				else
					Counter <= Counter-1;
				end if;
			end if;
			Q <= EN and Overflow;
		end if;
	end process;
end;	

----------------------------------------------------------------------------------------------------
-- TRANCEIVER -- TRANCEIVER -- TRANCEIVER -- TRANCEIVER -- TRANCEIVER -- TRANCEIVER -- TRANCEIVER --
----------------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModTranceiver is
	generic (
		DUPLEX			: boolean := MOD_DUPLEX;		-- Enable/disable duplex mode
		DATA_BITS		: integer := MOD_DATA_BITS;		-- 1..32
		PARITY_MODE		: string := MOD_PARITY_MODE;	-- NONE, EVEN, ODD, MARK, SPACE
		STOP_BITS		: integer := MOD_STOP_BITS;		-- 0, 1, 2, ...
		DATA_DIR		: string := MOD_DATA_DIR;		-- LSB, MSB
		BAUD_DIVIDER	: integer := 8;					-- Rate = Fclk/BAUD_DIVIDER/3
		REAL_DIVIDER	: real := 25.0 );				-- Rate = Fclk/BAUD_DIVIDER
	port (
		CLK		: in std_logic;			-- Main clock
		RSTn	: in std_logic := '1';	-- Async. reset (0)
		
		TX		: out std_logic;		-- Serial output
		RX		: in std_logic := '1';	-- Serial input
		TE		: out std_logic;		-- Serial output enable
		
		TDi		: in std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data input
		TRDYi	: in std_logic := '0';		-- TX data ready input
		TRDYo	: out std_logic;			-- TX input register empty output
		
		RDo		: out std_logic_vector( DATA_BITS-1 downto 0 );	-- Parallel data output
		RRDYi	: in std_logic := '1';	-- Data acquisition input
		RRDYo	: out std_logic;		-- Parallel data output ready
		PE		: out std_logic;		-- Parity error flag
		FE		: out std_logic );		-- Frame error flag
end entity;

architecture rtl of ModTranceiver is
	signal Baud, Baudx3, Serial, Clock, Inhibit, Transmit : std_logic;
begin

	TE <= Transmit;
	Inhibit <=	'0' when DUPLEX else
				Transmit;

-- x3 BAUD GENERATOR

	intbaud: if BAUD_DIVIDER>0 generate	
		arxdiv: ModIntDivider
			generic map (
				DIVIDER => BAUD_DIVIDER )
			port map (
				CLK => CLK,
				RSTn => RSTn,
				Q => Baudx3 );
	end generate;
	
	fracbaud: if BAUD_DIVIDER<=0 generate
		arxdiv: ModFractDivider
			generic map (
				DIVIDER => REAL_DIVIDER / 3.0,
				PRECESION => 200.0 )
			port map (
				CLK => CLK,
				RSTn => RSTn,
				Q => Baudx3 );
	end generate;

-- STATION RECEIVER

	bpll: ModDigitalPll
		generic map (
			OVERSAMPLING => "x3" )
		port map (
			CLK => CLK,
			RSTn => RSTn,
			EN => Baudx3,
			SDI => RX,
			INH => Inhibit,
			SDO => Serial,
			SCK => Clock );

	brx: ModDeserializer
		generic map (
			DATA_BITS => DATA_BITS,
			PARITY_MODE => PARITY_MODE,
			STOP_BITS => STOP_BITS,
			DATA_DIR => DATA_DIR )
		port map (
			CLK => CLK,
			RSTn => RSTn,
			EN => Clock,
			SDI => Serial,
			DO => RDo,
			RDYO => RRDYo,
			RDYI => RRDYi,
			PE => PE,
			FE => FE );
	
-- STATION TRANSMITTER

	atxdiv: ModIntDivider
		generic map (
			DIVIDER => 3 )
		port map (
			CLK => CLK,
			RSTn => RSTn,
			EN => Baudx3,
			Q => Baud );
			
	atx: ModSerializer
		generic map (
			DATA_BITS => DATA_BITS,
			PARITY_MODE => PARITY_MODE,
			STOP_BITS => STOP_BITS,
			DATA_DIR => DATA_DIR )
		port map (
			CLK => CLK,
			RSTn => RSTn,
			EN => Baud,
			SDO => TX,
			SDE => Transmit,
			DI => TDi,
			RDYO => TRDYo,
			RDYI => TRDYi );

end architecture;

------------------------------------------------------------------------------------------
-- DECODER -- DECODER -- DECODER -- DECODER -- DECODER -- DECODER -- DECODER -- DECODER --
------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModDecoder is
	generic (
		ADDR_WIDTH : integer := 8;				-- "Packet size"
		MOD_ADDR : integer := MOD_DEF_ADDR );	-- Module address
	port (
		CLK : in std_logic;				-- Global clock
		RSTn : in std_logic := '1';		-- Async. reset
		
		DI : in mod_data_type;			-- Data input
		DIE : in std_logic := '1';		-- Input data ready (ack==1)
		DERR : in std_logic := '0';		-- Input data error flag
		
		DO : out mod_data_type;		-- Packet data out
		DOE : out std_logic;		-- Packet data write enable
		AO : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	--Packet data address
		
		RDYO : out std_logic;			-- Packet ready
		RDYI : in std_logic := '1';		-- Processor acknoledge
		
		RAM_AI : in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		RAM_DO : out mod_data_type );	-- Packet buffer interface
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_hardware.all;
use work.ak_types.all;

architecture rtl of ModDecoder is

	type RX_DECODED_DATA is (
		Unknown,
		Start,
		Digit,
		CR,
		LF );
		
	type RX_MACHINE is (
		Idle,
		AH,
		AL,
		DH,
		DL,
		LF );
		
	signal BaseAddr : byte;
	signal AsciiDecoder : nibble;
	signal AsciiFound : std_logic;
	
	signal St1Type : RX_DECODED_DATA;
	signal St1Data : byte;
	signal St1Strobe, St1MatchAH, St1MatchAL : std_logic;
	
	signal St2Data, St2CRC : byte;
	signal St2State : RX_MACHINE;
	signal St2Strobe, St2SOF, St2EOF : std_logic;
	
	signal St3Data : byte;
	signal St3Addr : std_logic_vector( ADDR_WIDTH-1 downto 0 );
	signal St3Enable, St3Strobe, St3Request, St3Overflow : std_logic;
	
begin

	DO <= St3Data;
	DOE <= St3Strobe;
	AO <= St3Addr;
	RDYO <= St3Request;

	BaseAddr <= conv_std_logic_vector( MOD_ADDR, 8 );
	AsciiDecoder <= ascii2nibble(DI);
	AsciiFound <= is_ascii_digit(DI);
	St1MatchAL <= match( St1data( 3 downto 0 ), MOD_ADDR ) and St1MatchAH;
	St3Strobe <= St2Strobe;

	process ( CLK, RSTn )
	begin
		if RSTn='0' then
			
			St1Type <= Unknown;
			St1Data <= (others=>'0');
			St1Strobe <= '0';
			St1MatchAH <= '0';
			
			St2Data <= (others=>'0');
			St2CRC <= (others=>'0');
			St2State <= Idle;
			St2Strobe <= '0';
			St2SOF <= '0';
			St2EOF <= '0';
			
			St3Data <= (others=>'0');
			St3Addr <= (others=>'0');
			St3Enable <= '0';
			St3Request <= '0';
			St3Overflow <= '0';
			
		elsif rising_edge(CLK) then

		-- Stage 1 - symbol to command conversion
		
			if DIE='1' then
				St1Data( 3 downto 0 ) <= AsciiDecoder;
				St1Data( 7 downto 4 ) <= St1Data( 3 downto 0 );
				
				if DERR='1' then
					St1Type <= Unknown;
				elsif DI=MOD_SOF_MARKER then
					St1Type <= Start;
				elsif DI=MOD_CR_MARKER then
					St1Type <= CR;
				elsif DI=MOD_LF_MARKER then
					St1Type <= LF;
				elsif AsciiFound='1' then
					St1Type <= Digit;
				else
					St1Type <= Unknown;
				end if;
				
				St1MatchAH <= match( St1Data( 3 downto 0 ), BaseAddr( 7 downto 4 ) );
			end if;
			
			St1Strobe <= DIE;

		-- Stage 2 - SOF, EOF, Addr, Data & CRC detection
			
			if St1Strobe='1' and St1Type=Digit and St2State=DL then
				St2Strobe <= '1';
			else
				St2Strobe <= '0';
			end if;
			
			if St1Strobe='1' and St2State=LF and St1Type=LF and match( St2CRC, 0 )='1' then
				St2EOF <= '1';
			else
				St2EOF <= '0';
			end if;
			
			if St1Strobe='1' and St1Type=Start then
				St2SOF <= '1';
			else
				St2SOF <= '0';
			end if;
			
			if St1Strobe='1' then

				if St2State=AL then
					St2CRC <= St1Data;
				elsif St2State=DL then
					St2CRC <= St2CRC + St1Data;
				end if;
				
				if St2State=DL then
					St2Data <= St1Data;
				end if;
			
				if St1Type=Start then
					St2State <= AH;
				else
					case St2State is
						when AH =>
							if St1Type=Digit and St3Request='0' then
								St2State <= AL;
							else
								St2State <= Idle;
							end if;
						
						when AL =>
							if St1Type=Digit and St1MatchAL='1' then
								St2State <= DH;
							else
								St2State <= Idle;
							end if;
						
						when DH =>
							if St1Type=CR then
								St2State <= LF;
							elsif St1Type=Digit then
								St2State <= DL;
							else
								St2State <= Idle;
							end if;
						
						when DL =>
							if St1Type=Digit then
								St2State <= DH;
							else
								St2State <= Idle;
							end if;
						
						when others =>
							St2State <= Idle;
							
					end case;
				end if;
				
			end if;
			
		-- Stage 3 - packet sending => data, length, req->ack, check overflow
		
			St3Data <= St2Data;
			
			if St3Strobe='1' then
				St3Enable <= '1';
			elsif St2SOF='1' then
				St3Enable <= '0';
			end if;
			
			if St2EOF='1' and St3Overflow='0' and St3Enable='1' then
				St3Request <= '1';
			elsif RDYI='1' then
				St3Request <= '0';
			end if;
			
			if St2Strobe='1' then
				if St3Enable='0' then
					St3Addr <= (others=>'0');
					St3Overflow <= '0';
				else

					St3Addr <= St3Addr + 1;
					if match( St3Addr, -1 )='1' then
						St3overflow <= '1';
					end if;
				end if;
			end if;
			
		end if;
	end process;
	
	buff : dpsyncram
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,
			DATA_WIDTH => 8,
			READ_LATENCY => 2 )
		port map (
			LCLK => CLK,
			LADR => St3Addr,
			LDI => St3Data,
			LWE => St3Strobe,
			
			RCLK => CLK,
			RADR => RAM_AI,
			RDO => RAM_DO );
			
end architecture;

--------------------------------------------------------------------------------------------
-- CODER -- CODER -- CODER -- CODER -- CODER -- CODER -- CODER -- CODER -- CODER -- CODER --
--------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModCoder is
	generic (
		ADDR_WIDTH : integer := 8;				-- "Packet size"
		MOD_ADDR : integer := MOD_DEF_ADDR );	-- Module address
	port (
		CLK : in std_logic;				-- Global clock
		RSTn : in std_logic := '1';		-- Async. reset
		
		RAM_AI : in std_logic_vector( ADDR_WIDTH-1 downto 0 ) := (others=>'0');
		RAM_DI : in mod_data_type := (others=>'0');-- Packet RAM data
		RAM_WE : in std_logic := '0';		-- Packet RAM write enable
		
		PktReq : in std_logic := '1';	-- Packet send request
		PktAck : out std_logic;			-- Packet send acknoledge
		
		TXD : out mod_data_type;	-- Data to transmit
		RDYO : out std_logic;		-- TXD enable
		RDYI : in std_logic := '1');-- Transmitter acknoledge
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_hardware.all;
use work.ak_types.all;

architecture rtl of ModCoder is

	type TX_MACHINE is (
		Idle,
--		TransmitSilence,
		TransmitSOF,
		WaitBuff,
		TransmitDH,
		TransmitDL,
		TransmitCR,
		TransmitLF );
		
	signal TxState : TX_MACHINE;
	signal PktAddr, PktSize, ReqSize : std_logic_vector( ADDR_WIDTH-1 downto 0 );
	signal PktData, HLData, TxData, CRC, ASCIIdata, BaseAddr : byte;
	signal WriteReq, WriteAck, PktEnd1, PktEnd2, PktEnd3 : std_logic;
		
begin

	ReqSize <= RAM_AI;
	BaseAddr <= conv_std_logic_vector( MOD_ADDR, 8 );
	
	PktAck <= '1' when TxState=Idle else '0';
	WriteReq <= '1' when TxState=TransmitSOF
			or TxState=TransmitDH or TxState=TransmitDL
			or TxState=TransmitCR or TxState=TransmitLF else '0';
	ASCIIdata <= nibble2ascii( HLData( 7 downto 4 ) );

	TXD <= TxData;
	RDYO <= WriteReq;
	WriteAck <= RDYI;
	
	process ( CLK, RSTn )
	begin
		if RSTn = '0' then
		
-- PROCESS RESET

			PktAddr <= (others=>'0');--
			PktSize  <= (others=>'0');--
			PktEnd1 <= '0';--
			PktEnd2 <= '0';--
			PktEnd3 <= '0';--

			HLData <= (others=>'0');--
			TxData <= (others=>'0');--
			CRC <= (others=>'0');--
			
			TxState <= Idle;--
			
		elsif CLK'event and CLK='1' then
		
-- PROCESS FLAGS

			if TxState=Idle then
				PktAddr <= (others=>'0');
				PktSize <= ReqSize;
				PktEnd1 <= '0';
				PktEnd2 <= '0';
				PktEnd3 <= '0';
			else
				PktEnd1 <= PktEnd1 or match( PktSize, 0 );
				if TxState=TransmitDL and WriteAck='1' then
					inc( PktAddr );
					dec( PktSize );
					PktEnd2 <= PktEnd2 or PktEnd1;
					PktEnd3 <= PktEnd3 or PktEnd2;
				end if;
			end if;

			if TxState=Idle then
				HLData <= BaseAddr;
			elsif TxState=WaitBuff then
				HLData( 7 downto 4 ) <= HLData( 3 downto 0 );
			elsif TxState=TransmitDL and WriteAck='1' then
				if PktEnd1='1' then
					HLData <= CRC;
				else
					HLData <= PktData;
				end if;
			end if;
			
			if TxState=Idle then
				TxData <= MOD_SOF_MARKER;
			elsif TxState=TransmitCR and WriteAck='1' then
				TxData <= MOD_LF_MARKER;
			elsif TxState=WaitBuff then
				if PktEnd3='1' then
					TxData <= MOD_CR_MARKER;
				else
					TxData <= ASCIIdata;
				end if;
			elsif TxState=TransmitDH and WriteAck='1' then
				TxData <= ASCIIdata;
			end if;
			
			if TxState=Idle then
				CRC <= (others=>'0');
			elsif TxState=WaitBuff then
				CRC <= CRC - HLData;
			end if;

-- PROCESS MACHINE

			case TxState is
				when Idle =>
					if PktReq='1' then
						TxState <= TransmitSOF;
					end if;
					
				when TransmitSOF =>
					if WriteAck='1' then
						TxState <= WaitBuff;
					end if;
					
				when WaitBuff =>
					if PktEnd3='1' then
						TxState <= TransmitCR;
					else
						TxState <= TransmitDH;
					end if;
					
				when TransmitDH =>
					if WriteAck='1' then
						TxState <= TransmitDL;
					end if;
					
				when TransmitDL =>
					if WriteAck='1' then
						TxState <= WaitBuff;
					end if;
					
				when TransmitCR =>
					if WriteAck='1' then
						TxState <= TransmitLF;
					end if;
					
				when TransmitLF =>
					if WriteAck='1' then
						TxState <= Idle;
					end if;
					
			end case;

		end if;
			
	end process;

	buff : dpsyncram
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,
			DATA_WIDTH => 8,
			READ_LATENCY => 2 )
		port map (
			LCLK => CLK,
			LADR => RAM_AI,
			LDI => RAM_DI,
			LWE => RAM_WE,
			
			RCLK => CLK,
			RADR => PktAddr,
			RDO => PktData );
	
end architecture;

---------------------------------------------------------------------------------------------
-- PROCESSOR -- PROCESSOR -- PROCESSOR -- PROCESSOR -- PROCESSOR -- PROCESSOR -- PROCESSOR --
---------------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModProcessor is
	generic (
		ADDR_WIDTH : integer := 8;					-- "Packet Size"
		READ_LATENCY : integer := MOD_DEF_LATENCY );	-- Registers read delay
	port (
		CLK : in std_logic;				-- Global clock
		RSTn : in std_logic := '1';		-- Global async. reset
		
		RXD : in mod_data_type;			-- Received packet RAM data
		RXA : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	-- Rx RAM address
		RXS : in std_logic_vector( ADDR_WIDTH-1 downto 0 );		-- Rx packet size
		RXReq : in std_logic;	-- Received packet ready
		RXAck : out std_logic;	-- Acknoledge to RXReq

		TXD : out mod_data_type;		-- Transmitter packet RAM data
		TXA : out std_logic_vector( ADDR_WIDTH-1 downto 0 );	-- Tx RAM address
		TXW : out std_logic;			-- Tx RAM write enable
		TXReq : out std_logic;			-- Tx packet ready request
		TXAck : in std_logic := '1';	-- Acknoledge to TXReq
		
		RegA : out mod_addr_type;			-- Register R/W address
		RegW : out std_logic;				-- Register write strobe
		RegDO : out mod_data_type;					-- Register write data
		RegDI : in mod_data_type := (others=>'1'));-- register read data
end entity;

library ieee, work;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ak_types.all;

architecture rtl of ModProcessor is

	type RX_DECODED_CMD is (
		Unknown,
		WriteRegs,
		ReadRegs );
		
	type DECODER_MACHINE is (
		Idle,
		WaitBuff2,
		WaitBuff1,
		SaveCommand,
		CheckCommand,
		CheckSize,
		Execute,
		WriteData,
		WaitRegs7,
		WaitRegs6,
		WaitRegs5,
		WaitRegs4,
		WaitRegs3,
		WaitRegs2,
		WaitRegs1,
		ReadData,
		ErrorHeader,
		ErrorCommand,
		ErrorCode,
		PipeDelay,
		SendAnswer );
		
	pure function GetLatency( i : integer ) return DECODER_MACHINE is
	begin
		case i is
			when 0 =>
				return WaitRegs1;
			when 1 =>
				return WaitRegs2;
			when 2 =>
				return WaitRegs3;
			when 3 =>
				return WaitRegs4;
			when 4 =>
				return WaitRegs5;
			when 5 =>
				return WaitRegs6;
			when 6 =>
				return WaitRegs7;
			when others =>
				report "CmdProcessor: unknown register read latency"
					severity ERROR;
				return ReadData;
		end case;
	end function;
		
	constant READ_INIT_STATE : DECODER_MACHINE := GetLatency(READ_LATENCY);

	signal ProcState : DECODER_MACHINE;
	signal ReqPktAddr, AnsPktAddr : std_logic_vector( ADDR_WIDTH-1 downto 0 );
	signal ReqData1, ReqData2, CmdCode, ErrCode, ReqPktSize : byte;
	signal CmdDecode : RX_DECODED_CMD;
	signal RegSize : byte;
	signal RegLong, RegZero, RegEnd : std_logic;
	signal ReqPktEnd1, ReqPktEnd2, ReqPktMatch : std_logic;
	signal RegAddr : word;
	signal AnsData, InData : byte;
	signal AnsPktWrite : std_logic;
	
begin

	RXAck <= '1' when ProcState=Idle else '0';
	TXReq <= '1' when ProcState=SendAnswer else '0';
	ReqData1 <= RXD;
	RXA <= ReqPktAddr;
	RegDO <= ReqData1;
	RegW <= '1' when ProcState=WriteData else '0';
	RegA <= RegAddr;

	TXD <= AnsData;
	TXA <= AnsPktAddr;
	TXW <= AnsPktWrite;
	
	process ( CLK, RSTn )
	begin
		if RSTn = '0' then
		
-- PROCESS RESET
		
			ProcState <= Idle;
			
			ReqPktAddr <= (others=>'0');--
			ReqPktSize <= (others=>'0');--
			ReqData2 <= (others=>'0');--
			ReqPktEnd1 <= '0';--
			ReqPktEnd2 <= '0';--
			ReqPktMatch <= '0';--
			
			AnsPktAddr <= (others=>'0');--
			InData <= (others=>'0');--
			AnsData <= (others=>'0');--
			AnsPktWrite <= '0';--
			
			CmdCode <= (others=>'0');--
			ErrCode <= MOD_ERR_FORMAT;--
			CmdDecode <= Unknown;--
			
			RegAddr <= (others=>'0');--
			RegSize <= (others=>'0');--
			RegLong <= '0';--
			RegZero <= '0';--
			RegEnd <= '0';--
	
		elsif CLK'event and CLK='1' then
		
-- PROCESS FLAGS
		
			if ProcState=ErrorHeader or ProcState=ErrorCommand then
				ErrCode <= ErrCode;
			elsif ProcState=CheckCommand then
				ErrCode <= MOD_ERR_COMMAND;
			else
				ErrCode <= MOD_ERR_FORMAT;
			end if;
		
			if ProcState=SaveCommand then
				CmdCode <= ReqData1;
				case ReqData1 is
					when MOD_WRITE_REGS =>
						CmdDecode <= WriteRegs;
					when MOD_READ_REGS =>
						CmdDecode <= ReadRegs;
					when others =>
						CmdDecode <= Unknown;
				end case;
			end if;
		
			InData <= RegDI;
		
			case ProcState is
				when ReadData =>
					AnsData <= InData;
				when ErrorCommand =>
					AnsData <= CmdCode or X"80";
				when ErrorCode =>
					AnsData <= ErrCode;
				when others =>
					Ansdata <= ReqData1;
			end case;
					
			if ProcState=SaveCommand or ProcState=CheckCommand
					or ProcState=CheckSize or ProcState=Execute
					or ProcState=ErrorCommand or ProcState=ErrorCode
					or ProcState=ReadData then
				AnsPktWrite <= '1';
			else
				AnsPktWrite <= '0';
			end if;
			
			if ProcState=WaitBuff1 or ProcState=ErrorHeader then
				AnsPktAddr <= (others=>'0');
			elsif AnsPktWrite='1' then
				inc( AnsPktAddr );
			end if;
		
			ReqData2 <= ReqData1;
			ReqPktMatch <= match( ReqPktSize, ReqData2 );
		
			if ProcState=Idle then
				ReqPktAddr <= (others=>'0');
				ReqPktSize <= RXS;
				ReqPktEnd1 <= '0';
				ReqPktEnd2 <= '0';
			else
				inc( ReqPktAddr );
				dec( ReqPktSize );
				ReqPktEnd1 <= ReqPktEnd1 or match( ReqPktSize, 0 );
				ReqPktEnd2 <= ReqPktEnd2 or ReqPktEnd1;
			end if;
			
			if ProcState=Execute then
				RegAddr( 15 downto 8 ) <= ReqData1;
				RegAddr( 7 downto 0 ) <= ReqData2;
			elsif ProcState/=Idle then
				inc( RegAddr );
			end if;
			
			if ProcState=CheckCommand then
				RegSize <= ReqData1;
			elsif ProcState=CheckSize then
				if conv_integer(RegSize) < 2 then
					RegEnd <= '1';
				else
					RegEnd <= '0';
				end if;
			elsif ProcState=ReadData then
				dec( RegSize );
				if conv_integer(RegSize) < 3 then
					RegEnd <= '1';
				end if;
			end if;
			
			--if ProcState=CheckCommand then
				--RegEnd <= '0';
			--elsif conv_integer(RegSize) < 3 then
				--RegEnd <= '1';
			--end if;

			if conv_integer(ReqData1) > 250 then
				RegLong <= '1';
			else
				RegLong <= '0';
			end if;
			RegZero <= match( ReqData1, 0 );
			
-- PROCESS STATE
			
			case ProcState is
				when Idle =>
					if RXReq='1' and TXAck='1' then
						ProcState <= WaitBuff2;
					end if;
					
				when WaitBuff2 =>
					ProcState <= WaitBuff1;
					
				when WaitBuff1 =>
					if ReqPktEnd1='1' then
						ProcState <= SendAnswer; --PING command
					else
						ProcState <= SaveCommand;
					end if;
					
				when SaveCommand =>
					ProcState <= CheckCommand;
					
				when CheckCommand =>
					if CmdDecode=ReadRegs or CmdDecode=WriteRegs then
						ProcState <= CheckSize;
					else
						ProcState <= ErrorHeader;
					end if;
					
				when CheckSize =>
					if RegLong='1' or RegZero='1' then
						ProcState <= ErrorHeader;
					else
						ProcState <= Execute;
					end if;
					
				when Execute =>
					case CmdDecode is
						when ReadRegs =>
							if ReqPktEnd1='1' and ReqPktEnd2='0' then
								ProcState <= READ_INIT_STATE;
							else
								ProcState <= ErrorHeader;
							end if;
						when others => --WriteRegs
							if ReqPktEnd1='0' and ReqPktMatch='1' then
								ProcState <= WriteData;
							else
								ProcState <= ErrorHeader;
							end if;
					end case;
					
				when WriteData =>
					if ReqPktEnd1='1' then
						ProcState <= SendAnswer;
					end if;
					
				when WaitRegs7 =>
					ProcState <= WaitRegs6;
					
				when WaitRegs6 =>
					ProcState <= WaitRegs5;
					
				when WaitRegs5 =>
					ProcState <= WaitRegs4;
					
				when WaitRegs4 =>
					ProcState <= WaitRegs3;
					
				when WaitRegs3 =>
					ProcState <= WaitRegs2;
					
				when WaitRegs2 =>
					ProcState <= WaitRegs1;
					
				when WaitRegs1 =>
					ProcState <= ReadData;
					
				when ReadData =>
					if RegEnd='1' then
						ProcState <= PipeDelay;
					end if;
					
				when ErrorHeader =>
					ProcState <= ErrorCommand;
					
				when ErrorCommand =>
					ProcState <= ErrorCode;
					
				when ErrorCode =>
					ProcState <= PipeDelay;
					
				when PipeDelay =>
					ProcState <= SendAnswer;
					
				when SendAnswer =>
					if TXAck='1' then
						ProcState <= Idle;
					end if;
					
			end case;
		end if;
			
	end process;
	
end architecture;

--------------------------------------------------------------------------------------
-- CONTROL INTERFACE -- CONTROL INTERFACE -- CONTROL INTERFACE -- CONTROL INTERFACE --
--------------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use work.modlike.all;

entity ModControl is
	generic (
		DUPLEX : boolean := MOD_DUPLEX;				-- Enable/disable duplex operation
		BAUD_DIVIDER : integer := 0;				-- Rate = CLK/3/BAUD_DIVIDER
		REAL_DIVIDER : real := 0.0;					-- Rate = CLK/REAL_DIVIDER
		MOD_ADDR : integer := MOD_DEF_ADDR;			-- Module address
		READ_LATENCY : integer := MOD_DEF_LATENCY );	-- Register read delay, CLKs
	port (
		CLK : in std_logic;			-- Global clock
		RSTn : in std_logic := '1';	-- Async. reset
		
		RXD : in std_logic;			-- Serial data input
		TXD : out std_logic;		-- Serial data output
		TXE : out std_logic;		-- Transmittion enable
		
		AO : out mod_addr_type;	-- Register address
		WE : out std_logic;		-- Register write enable
		DO : out mod_data_type;	-- Register write data
		DI : in mod_data_type;	-- Register read data

		UP : out std_logic );	-- Register update indicator
end entity;

library work;
use work.ak_types.all;

architecture rtl of ModControl is

	constant ADDR_WIDTH : integer := 8;

	signal RxData, TxData, ReqData, AnsData : byte := (others=>'0');
	signal RxReady, ParErr, FrmErr, RxError, TxEnable, TxReady,
			RxReq, RxAck, AnsWrite, TxReq, TxAck : std_logic;
	signal RxSize, ReqAddr, AnsAddr : std_logic_vector( ADDR_WIDTH-1 downto 0 );
	
	signal RegUp, WEreg : std_logic;
	signal RegUpDelay : std_logic_vector(2 downto 0);
	
begin

	process ( CLK, RSTn )
	begin
		if RSTn='0' then
			RegUp <= '0';
			RegUpDelay <= (others=>'1');
		elsif rising_edge(CLK) then
			if WEreg='1' then
				RegUp <= '1';
				RegUpDelay <= (others=>'1');
			else
				dec(RegUpDelay);
				RegUp <= RegUp and (not match(RegUpDelay,0));
			end if;
		end if;
	end process;
	
	UP <= RegUp;
	
	serdes: ModTranceiver
		generic map (
			DUPLEX => DUPLEX,
			DATA_BITS => 7,
			PARITY_MODE => "NONE",
			STOP_BITS => 1,
			DATA_DIR => "LSB",
			BAUD_DIVIDER => BAUD_DIVIDER,
			REAL_DIVIDER => REAL_DIVIDER )
		port map (
			CLK => CLK,
			RSTn => RSTn,			
			RX => RXD,
			RDo => RxData( 6 downto 0 ),
			RRDYi => '1',
			RRDYo => RxReady,
			PE => ParErr,
			FE => FrmErr,
			TX => TXD,
			TE => TXE,
			TDi => TxData( 6 downto 0 ),
			TRDYi => TxEnable,
			TRDYo => TxReady );

	RxError <= ParErr or FrmErr;
	RxData(7) <= '0';
			
	decoder: ModDecoder
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,	-- "Packet size"
			MOD_ADDR => MOD_ADDR )	-- Module address
		port map (
			CLK => CLK,				-- Global clock
			RSTn => RSTn,			-- Async. reset
			DI => RxData,			-- Data input
			DIE => RxReady,			-- Input data ready (ack==1)
			DERR => RxError,		-- Input data error flag
			AO => RxSize,			-- Packet size
			RDYO => RxReq,			-- Packet ready
			RDYI => RxAck,			-- Processor acknoledge
			RAM_AI => ReqAddr,		-- Packet data address
			RAM_DO => ReqData );	-- Packet buffer interface

	seqproc: ModProcessor
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,	-- "Packet Size"
			READ_LATENCY => READ_LATENCY )	-- Registers read delay
		port map (
			CLK => CLK,			-- Global clock
			RSTn => RSTn,		-- Global async. reset			
			RXD => ReqData,		-- Received packet RAM data
			RXA => ReqAddr,		-- Rx RAM address
			RXS => RxSize,		-- Rx packet size
			RXReq => RxReq,		-- Received packet ready
			RXAck => RxAck,		-- Acknoledge to RXReq
			TXD => AnsData,		-- Transmitter packet RAM data
			TXA => AnsAddr,		-- Tx RAM address
			TXW => AnsWrite,	-- Tx RAM write enable
			TXReq => TxReq,		-- Tx packet ready request
			TXAck => TxAck,		-- Acknoledge to TXReq
			RegA => AO,			-- Register R/W address
			RegW => WEreg,		-- Register write strobe
			RegDO => DO,		-- Register write data
			RegDI => DI );		-- register read data
			
	WE <= WEreg;

	coder: ModCoder
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,	-- "Packet size"
			MOD_ADDR => MOD_ADDR )		-- Module address
		port map (
			CLK => CLK,			-- Global clock
			RSTn => RSTn,		-- Async. reset
			RAM_AI => AnsAddr,	--Packet RAM data address/packet size
			RAM_DI => AnsData,	-- Packet RAM data
			RAM_WE => AnsWrite,	-- Packet RAM write enable
			PktReq => TxReq,	-- Packet send request
			PktAck => TxAck,	-- Packet send acknoledge
			TXD => TxData,		-- Data to transmit
			RDYO => TxEnable,	-- TXD enable
			RDYI => TxReady );	-- Transmitter acknoledge
			
end architecture;

--------------------------------------------------------------------------------------		
-- END -- END -- END -- END -- END -- END -- END -- END -- END -- END -- END -- END --
--------------------------------------------------------------------------------------
