-------------------------------------------------------------------------------
-- ROM file write/read package

library ieee, work;
use ieee.std_logic_1164.all;
use work.ak_types.all;

package romio is

-- Altera MIF ROM file writer

	procedure WriteMIF(
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8;		-- word width
		comment : string := "" );	-- default value

-- Altera MIF parallel ROM file writer

	procedure WriteMIF(
		dataA : int_array;			-- input data
		dataB : int_array;			-- input data
		name : string;				-- file name
		widthA : integer := 8;		-- word width
		widthB : integer := 8;		-- word width
		comment : string := "" );	-- default value
		
-- Altera MIF ROM file reader

	procedure ReadMIF(
		variable data : out int_array;	-- output data
		name : string;					-- file name
		def : integer := -1 );			-- defult value

-- Intel HEX 16 ROM file writer

	procedure WriteHEX(
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8;		-- word width
		comment : string := "" );	-- default value
		
-- Xilinx ROM file writer

	procedure WriteTXT(
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8 );	-- word width 	
end package;

-------------------------------------------------------------------------------
-- ROM file write/read body

library ieee;
use std.textio.all;

package body romio is

-- Altera MIF ROM file writer

	procedure WriteMIF (
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8;		-- word width
		comment : string := "" ) is	-- default value
		
		variable str : line;
		procedure write( val : string ) is begin write( str, val ); end procedure;
		
		variable v : std_logic_vector( width-1 downto 0 );
		file mif : text open write_mode is name;
		
	begin
		if comment'length>0 then
			write( "-- " );
			write(str,comment);
			writeline(mif,str);
		end if;
		
		write("DEPTH = "); write(str,data'length,right,0); write(";"); writeline(mif,str);
		write("WIDTH = "); write(str,width,right,0); write(";"); writeline(mif,str);
		write("ADDRESS_RADIX = HEX;"); writeline(mif,str);
		write("DATA_RADIX = BIN;"); writeline(mif,str);
		write("CONTENT"); writeline(mif,str);
		write("BEGIN"); writeline(mif,str);
		
		for i in data'low to data'high loop
			write("0");
			write(str,string16h(i));
			write(" : ");
			v := to_vector( data(i), v'length );
			write(str,bin_string(v));
			if data(i)>31 and data(i)<127 then
				write(" ; -- "); write(str,ascii(data(i)));
			else
				write(" ;");
			end if;
			writeline(mif,str);
		end loop;

		write("END;"); writeline(mif,str);
	end procedure;

-- Altera MIF parallel ROM file writer

	procedure WriteMIF(
		dataA : int_array;			-- input data
		dataB : int_array;			-- input data
		name : string;				-- file name
		widthA : integer := 8;		-- word width
		widthB : integer := 8;		-- word width
		comment : string := "" ) is	-- default value

		variable str : line;
		procedure write( val : string ) is begin write( str, val ); end procedure;
		
		variable va : std_logic_vector( widthA-1 downto 0 );
		variable vb : std_logic_vector( widthB-1 downto 0 );
		file mif : text open write_mode is name;
		
	begin
		if comment'length>0 then
			write("-- ");
			write(str,comment);
			writeline(mif,str);
		end if;
		
		write("DEPTH = "); write(str,dataA'length,right,0); write(";"); writeline(mif,str);
		write("WIDTH = "); write(str,widthA+widthB,right,0); write(";"); writeline(mif,str);
		write("ADDRESS_RADIX = HEX;"); writeline(mif,str);
		write("DATA_RADIX = BIN;"); writeline(mif,str);
		write("CONTENT"); writeline(mif,str);
		write("BEGIN"); writeline(mif,str);
		
		for i in dataA'low to dataA'high loop
			write("0");
			write(str,string16h(i));
			write(" : ");
			va := to_vector( dataA(i), va'length );
			vb := to_vector( dataB(i), vb'length );
			write(str,bin_string(va));
			write(str,bin_string(vb));
			write(" ;");
			writeline(mif,str);
		end loop;

		write("END;"); writeline(mif,str);
	end procedure;
	
-- Altera MIF ROM file reader

	procedure ReadMIF(
		variable data : out int_array;	-- output data
		name : string;					-- file name
		def : integer := -1 ) is		-- defult value
		
		variable str : line;
		procedure write( val : string ) is begin write( str, val ); end procedure;
		
		variable pos, addr, vect, alen, dlen, tmp, total : integer;
		variable ident : string (1 to 3);
		file mif : text open read_mode is name;
		variable aradix, dradix : integer;
		constant trace : boolean := FALSE;
		
	begin
		for i in data'range loop
			data(i) := def;
		end loop;
		aradix := 16;
		dradix := 2;
		total := 0;
		
	-- Find "BEGIN"
		while not ENDFILE(mif) loop
			readline(mif,str);
			if str'length>=5 then
				if str(1 to 5)="BEGIN" or str(1 to 5)="begin" then
					if trace then
						writeline(OUTPUT,str);
						write( "-- BEGIN found" ); writeline(OUTPUT,str);
					end if;
					exit;
				end if;
			end if;
			if trace then
				writeline(OUTPUT,str);
			end if;
		end loop;
		
	-- Loop until "END" or EOF
		while not ENDFILE(mif) loop
			readline(mif,str);
			if str'length>=3 then
				if str(1 to 3)="END" or str(1 to 3)="end" then
					if trace then
						writeline(OUTPUT,str);
						write( "-- END found" ); writeline(OUTPUT,str);
					end if;
					exit;
				end if;
			end if;
			
			addr := 0;
			vect := 0;
			pos := 1;
			alen := 0;
			dlen := 0;
			
		-- Get HEX address
			-- Find digit
			while pos<=str'length loop
				tmp := char2hex(str(pos));
				exit when tmp>=0 and tmp<aradix;
				pos := pos+1;
			end loop;
			-- Find not digit
			alen := pos;
			while pos<=str'length loop
				tmp := char2hex(str(pos));
				exit when tmp<0 or tmp>=aradix;
				pos := pos+1;
				addr := addr*aradix + tmp;
			end loop;
			alen := pos-alen;
			-- Find ":"
			while pos<=str'length loop
				exit when str(pos)=':';
				pos := pos+1;
			end loop;
			
		-- Get BIN data
			-- Find digit
			while pos<=str'length loop
				tmp := char2hex(str(pos));
				exit when tmp>=0 and tmp<dradix;
				pos := pos+1;
			end loop;
			-- Find not digit
			dlen := pos;
			while pos<=str'length loop
				tmp := char2hex(str(pos));
				exit when tmp<0 or tmp>=dradix;
				pos := pos+1;
				vect := vect*dradix + tmp;
			end loop;
			dlen := pos-dlen;
			-- Find ";"
			while pos<=str'length loop
				if str(pos)=';' then
					-- Add string
					pos := -1;
					exit;
				end if;
				pos := pos+1;
			end loop;
						
			if trace then
				writeline(OUTPUT,str);			
			end if;
			if dlen>0 and alen>0 then
				if trace then
					write("-- addr("); write(str,alen); write(")="); write(str,addr);
					write(" data("); write(str,dlen); write(")="); write(str,vect);
					writeline(OUTPUT,str);
				end if;
				data(addr) := vect;
				total := total+1;
			end if;
			
		end loop;
		
		str := null;
		write("ROM file "); write(str,name); write(" contains "); write(str,total);
		write(" data words."); writeline(OUTPUT,str);
	
	end procedure;

-- Intel HEX 16 ROM file writer

	procedure WriteHEX(
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8;		-- word width
		comment : string := "" ) is	-- default value

		variable str : line;
		procedure write( val : string ) is begin write( str, val ); end procedure;
		
		variable v : std_logic_vector( width-1 downto 0 );
		file hex : text open write_mode is name;
		
	begin
		write("BEGIN"); writeline(hex,str);
		
		for i in data'low to data'high loop
		end loop;

		write("END;"); writeline(hex,str);
	end procedure;

-- Xilinx ROM file writer

	procedure WriteTXT (
		data : int_array;			-- input data
		name : string;				-- file name
		width : integer := 8 ) is	-- word width
		
		variable str : line;
		
		variable v : std_logic_vector( width-1 downto 0 );
		file txt : text open write_mode is name;
		
	begin
		
		for i in data'low to data'high loop
			v := to_vector( data(i), v'length );
			write(str,bin_string(v));
			writeline(txt,str);
		end loop;

	end procedure;	
end package body;

-------------------------------------------------------------------------------
