`timescale 1ns/1ps

`include "packages.sv"

module tpm
#(parameter INPUT_CLK = 100_000_000,
  parameter TARGET_TRM_CLK = 10_000_000)
(clk, rstn, tx_attn, rx_attn, phase, start, emitting_request, cycle_emission, rdy, tpm_clk, tpm_ena, tpm_data, tpm_gate, duty_cycle_value, pulse_width_value);

localparam DATA_SIZE_BITS_FOR_ATTENUATORS = 4;
localparam DATA_SIZE_BITS_FOR_PHASE_SHIFTER = 5;
localparam QTY_OF_ADJUSTABLE_FIELDS_ATTENUATORS = 2;
localparam QTY_OF_ADJUSTABLE_FIELDS_PHASE_SHIFTER = 1;
localparam AGGREGATING_BITS = 2;

input logic clk, rstn;
input logic [DATA_SIZE_BITS_FOR_ATTENUATORS-1:0] tx_attn, rx_attn;
input logic [DATA_SIZE_BITS_FOR_PHASE_SHIFTER-1:0] phase;
input logic [$clog2(common_values::DUTY_CYCLE_MAX_VALUE)-1:0] duty_cycle_value;
input logic [$clog2(common_values::PULSE_WIDTH_MAX_VALUE_US)-1:0] pulse_width_value;
input logic start;
input logic emitting_request;
input logic cycle_emission;
output logic rdy;
output logic tpm_clk, tpm_ena, tpm_data, tpm_gate;


logic tpm_gate_standart_control, tpm_gate_pulse_control;
assign tpm_gate = tpm_gate_standart_control || tpm_gate_pulse_control;

logic emitting_request_buffer;

logic main_cycle_rdy, tpm_gate_rdy;
assign rdy = main_cycle_rdy && tpm_gate_rdy;
assign tpm_gate_rdy = ~tpm_gate;

logic [(DATA_SIZE_BITS_FOR_ATTENUATORS*QTY_OF_ADJUSTABLE_FIELDS_ATTENUATORS)+(DATA_SIZE_BITS_FOR_PHASE_SHIFTER*QTY_OF_ADJUSTABLE_FIELDS_PHASE_SHIFTER)+AGGREGATING_BITS-1:0] data_vector;

//***************CATCH**********************
logic start_catch1, start_catch2;

always @(posedge clk, negedge rstn)
begin
	if(~rstn)
	begin
		start_catch1 <= 0;
		start_catch2 <= 0;
	end
	else
	begin
		start_catch1 <= start;
		start_catch2 <= start_catch1;
	end
end

logic start_goes_up;
assign start_goes_up = (start_catch1 && ~start_catch2);

initial
begin
	start_catch1 = 0;
	start_catch2 = 0;
end



//***********OUTPUT/INTERNAL TPM CLK GENERATION***********
localparam DIVIDER = INPUT_CLK/(2*TARGET_TRM_CLK);
localparam QTY_OF_OUTPUT_CLKS = 46;
localparam QTY_OF_TOTAL_SESSION_CLKS = 58;

logic tpm_clk_buffer;
logic is_output_tpm_clk_active;
logic [$clog2(DIVIDER)-1:0] input_clk_counter; //counts input clk to issue output tpm clock
logic [$clog2(2*QTY_OF_TOTAL_SESSION_CLKS - 1)-1:0] tpm_edge_counter; //counts edges from tpm clock for session control

logic start_transmission_from_duty_counter;

enum logic {enIDLE_TPM_CLK, enGENERATING_CLK_FOR_ENTIRE_TRANSLATION_SESSION} tpm_clk_state;

assign tpm_clk = is_output_tpm_clk_active ? tpm_clk_buffer : 1'b0;

always @(posedge clk, negedge rstn)
begin
	if(~rstn) ResetTpmClock();
	else
	begin
		
		case(tpm_clk_state)
		
		enIDLE_TPM_CLK:
		begin
			if(start_goes_up || start_transmission_from_duty_counter)
			begin 
				$display($time, " Starting transmitting session");
				data_vector <= {reverse_4_bits(tx_attn), reverse_4_bits(rx_attn), 2'b0, reverse_5_bits(phase)};
				emitting_request_buffer <= emitting_request;
				is_output_tpm_clk_active <= 1;
				main_cycle_rdy <= 0;
				tpm_clk_state <= enGENERATING_CLK_FOR_ENTIRE_TRANSLATION_SESSION;
			end
		end

		enGENERATING_CLK_FOR_ENTIRE_TRANSLATION_SESSION:
		begin
			if(input_clk_counter == DIVIDER - 1)
			begin
				input_clk_counter <= 0;
				tpm_clk_buffer <= ~tpm_clk_buffer;
				
				if(tpm_edge_counter == 2*QTY_OF_OUTPUT_CLKS - 1) //Generated 46 output clks. Enough fot output. Continue generating inside
				begin
					$display($time," Stop generating output clk");
					is_output_tpm_clk_active <= 0;
				end					
				
				if(tpm_edge_counter == 2*QTY_OF_TOTAL_SESSION_CLKS - 1)
				begin
					$display($time, " Session finished!");
					main_cycle_rdy <= 1;
					tpm_edge_counter <= 0;
					tpm_clk_state <= enIDLE_TPM_CLK;
				end
				else tpm_edge_counter <= tpm_edge_counter + 1;
				
			end
			else input_clk_counter <= input_clk_counter + 1;			
		end
		
		default:;
		
		endcase
		
	end
end

initial ResetTpmClock();

function void ResetTpmClock();
	input_clk_counter = 0;
	tpm_edge_counter = 0;
	tpm_clk_buffer = 0;
	is_output_tpm_clk_active = 0;
	main_cycle_rdy = 1;
	tpm_clk_state = enIDLE_TPM_CLK;
	data_vector = 0;
	emitting_request_buffer = 0;
endfunction

//****************************************
logic [$clog2(2*QTY_OF_TOTAL_SESSION_CLKS - 1)-1:0] tpm_clk_counter; //counter to form tpm signals timings

always @(negedge tpm_clk_buffer, negedge rstn, posedge rdy)
begin
	if(~rstn) tpm_clk_counter <= 0;
	else if(rdy) tpm_clk_counter <= 0;	
			else tpm_clk_counter <= tpm_clk_counter + 1;
end

initial tpm_clk_counter = 0;

//***********TPM ENA GENERATION***********
localparam CLK_NUMBER_ENA_STROBE_DOWN_1 = 29;
localparam CLK_NUMBER_ENA_STROBE_UP_1   = 30;
localparam CLK_NUMBER_ENA_STROBE_DOWN_2 = 34;
localparam CLK_NUMBER_ENA_STROBE_UP_2   = 35;

always @(negedge tpm_clk_buffer, negedge rstn)
begin
	if(~rstn) ResetTpmEna();
	else
	begin
		if(tpm_clk_counter == CLK_NUMBER_ENA_STROBE_DOWN_1 - 1) tpm_ena <= 0;
		if(tpm_clk_counter == CLK_NUMBER_ENA_STROBE_UP_1 - 1)   tpm_ena <= 1;
		if(tpm_clk_counter == CLK_NUMBER_ENA_STROBE_DOWN_2 - 1) tpm_ena <= 0;
		if(tpm_clk_counter == CLK_NUMBER_ENA_STROBE_UP_2 - 1)   tpm_ena <= 1;		
	end
end

initial ResetTpmEna();

function void ResetTpmEna();
	tpm_ena = 1;
endfunction

//***********TPM DATA GENERATION***********
localparam CLK_NUMBER_DATA_VECTOR_START_POINT = 10;
localparam CLK_NUMBER_DATA_VECTOR_END_POINT   = 25;
localparam CLK_NUMBER_DATA_STROBE_UP_1   = 25;
localparam CLK_NUMBER_DATA_STROBE_DOWN_1 = 27;
localparam CLK_NUMBER_DATA_STROBE_UP_2   = 29;
localparam CLK_NUMBER_DATA_STROBE_DOWN_2 = 30;
localparam CLK_NUMBER_DATA_STROBE_UP_3   = 32;
localparam CLK_NUMBER_DATA_STROBE_DOWN_3 = 33;
localparam CLK_NUMBER_DATA_STROBE_UP_4   = 34;
localparam CLK_NUMBER_DATA_STROBE_DOWN_4 = 35;

logic tpm_data_buffer;
assign tpm_data = ((CLK_NUMBER_DATA_VECTOR_START_POINT - 1) < tpm_clk_counter) && (tpm_clk_counter <= (CLK_NUMBER_DATA_VECTOR_END_POINT - 1)) ? data_vector[CLK_NUMBER_DATA_VECTOR_END_POINT - 1 - tpm_clk_counter] : tpm_data_buffer;

always @(negedge tpm_clk_buffer, negedge rstn)
begin
	if(~rstn) ResetTpmData();
	else
	begin
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_UP_1 - 1)   tpm_data_buffer <= 1;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_DOWN_1 - 1) tpm_data_buffer <= 0;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_UP_2 - 1)   tpm_data_buffer <= 1;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_DOWN_2 - 1) tpm_data_buffer <= 0;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_UP_3 - 1)   tpm_data_buffer <= 1;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_DOWN_3 - 1) tpm_data_buffer <= 0;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_UP_4 - 1)   tpm_data_buffer <= 1;
		if(tpm_clk_counter == CLK_NUMBER_DATA_STROBE_DOWN_4 - 1) tpm_data_buffer <= 0;		
	end
end

initial ResetTpmData();

function void ResetTpmData();
	tpm_data_buffer = 0;
endfunction

//***********TPM GATE GENERATION***********
localparam CLK_NUMBER_GATE_STROBE_UP_1 	 = 48;
localparam CLK_NUMBER_GATE_STROBE_DOWN_1   = 58;

always @(negedge tpm_clk_buffer, negedge rstn)
begin
	if(~rstn) ResetTpmGateStandartControl();
	else
	begin
		if(emitting_request_buffer || cycle_emission)
		begin
			if(tpm_clk_counter == CLK_NUMBER_GATE_STROBE_UP_1 - 1) tpm_gate_standart_control   <= 1;
			if(tpm_clk_counter == CLK_NUMBER_GATE_STROBE_DOWN_1 - 1)   tpm_gate_standart_control   <= 0;
		end
	end
end

initial ResetTpmGateStandartControl();

function void ResetTpmGateStandartControl();
	tpm_gate_standart_control = 0;
endfunction

//*********PULSE WIDTH - DUTY CYCLE**************
//pulse width
localparam MULTIPLIER_FOR_US = 100; //T=0.01 us. Pulse width measured in us => 100 multiplier
localparam MAX_PULSE_WIDTH_COUNTER_VALUE = common_values::PULSE_WIDTH_MAX_VALUE_US * MULTIPLIER_FOR_US;
localparam MINIMUM_PULSE_WIDTH_VALUE = 1 * MULTIPLIER_FOR_US; //1us * MULTIPLIER. Min gate pulse = 1us

logic [$clog2(MAX_PULSE_WIDTH_COUNTER_VALUE)-1:0] pulse_width_value_for_counter;

//duty cycle
localparam ONE_TRANSMISSION_LENGTH_WITHOUT_GATE_PULSE_CLK_CYCLES = 480;
localparam ADDITIONAL_DELAY_US = 20;
localparam MIN_FIXED_TIME_IN_CLKS_BETWEEN_GATES = ONE_TRANSMISSION_LENGTH_WITHOUT_GATE_PULSE_CLK_CYCLES + ADDITIONAL_DELAY_US;
localparam MAX_DUTY_CYCLE_COUNTER_VALUE = common_values::DUTY_CYCLE_MAX_VALUE * MULTIPLIER_FOR_US;

logic [$clog2(MAX_DUTY_CYCLE_COUNTER_VALUE)-1:0] width_duty_counter, duty_cycle_value_for_counter;
logic width_duty_counter_enable;


always @(posedge clk, negedge rstn)
begin
	if(~rstn) ResetWidthDutyCycleControl();
	else
	begin
		if(width_duty_counter_enable)
		begin
			
			if((width_duty_counter == pulse_width_value_for_counter - 1) && tpm_gate_pulse_control) //acts when counting till gate ends
			begin
				$display($time, " Turn off the gate");				
				tpm_gate_pulse_control <= 0;
				width_duty_counter <= 0;
				if(~cycle_emission) //single pulse with custom width. Not needed to count further
				begin
					$display("No cycle emission. Turn off counter till next gate pulse");					
					width_duty_counter_enable <= 0;
				end	
			end
			else
			begin
				if((width_duty_counter == duty_cycle_value_for_counter) && (~tpm_gate_pulse_control)) //acts when counting till next frame
				begin
					$display($time, " Big counter is matched");
					if(cycle_emission) start_transmission_from_duty_counter <= 1;
					width_duty_counter_enable <= 0;
				end
				else
				begin
					width_duty_counter <= width_duty_counter + 1;
					start_transmission_from_duty_counter <= 0;
				end
			end	
			
		end
		else
		begin
		
			width_duty_counter <= 0;
		
			if(tpm_gate_standart_control) //default gate is set
			begin
				width_duty_counter_enable <= 1;			
				tpm_gate_pulse_control <= 1;
				$display("Set gate width controled pulse");
			end
				
		end
		
		if(start) 
		begin		
			width_duty_counter_enable <= 0;
			width_duty_counter <= 0;
			start_transmission_from_duty_counter <= 0;
		end
	end
end

initial ResetWidthDutyCycleControl();

function void ResetWidthDutyCycleControl();
begin
	width_duty_counter_enable = 0;
	width_duty_counter = 0;
	start_transmission_from_duty_counter = 0;
	tpm_gate_pulse_control = 0;
end
endfunction

//***BUFFER FOR PULSE WIDTH AND DUTY CYCLE***
always @(posedge start, negedge rstn)
begin
	if(~rstn)
	begin
		duty_cycle_value_for_counter <= 0;
		pulse_width_value_for_counter <= 0;
	end
	else
	begin
		pulse_width_value_for_counter <= (pulse_width_value > MINIMUM_PULSE_WIDTH_VALUE/MULTIPLIER_FOR_US) ? (pulse_width_value * MULTIPLIER_FOR_US - 1) : (MINIMUM_PULSE_WIDTH_VALUE - 1);
		duty_cycle_value_for_counter  <= (duty_cycle_value * MULTIPLIER_FOR_US > MIN_FIXED_TIME_IN_CLKS_BETWEEN_GATES) ? (duty_cycle_value * MULTIPLIER_FOR_US - ONE_TRANSMISSION_LENGTH_WITHOUT_GATE_PULSE_CLK_CYCLES - 2)
																																					       : MIN_FIXED_TIME_IN_CLKS_BETWEEN_GATES - ONE_TRANSMISSION_LENGTH_WITHOUT_GATE_PULSE_CLK_CYCLES - 2;
	end
end

initial
begin
	duty_cycle_value_for_counter = 0;
	pulse_width_value_for_counter = 0;
end

//**************OTHER FUNCTIONS**************
function [3:0] reverse_4_bits(input logic [3:0] input_bus);
	for(integer i = 0; i < 4; i = i + 1)
	begin
		reverse_4_bits[3 - i] = input_bus[i];
	end
endfunction

function [4:0] reverse_5_bits(input logic [4:0] input_bus);
	for(integer i = 0; i < 5; i = i + 1)
	begin
		reverse_5_bits[4 - i] = input_bus[i];
	end
endfunction


endmodule:tpm

