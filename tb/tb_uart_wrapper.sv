`timescale 1ns/1ps

typedef struct packed
{
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx, mtx, duty_cycle;
	logic [15:0] pulse_width;
} uart_wrapper_connection_t;


module tb_uart_wrapper;

logic clk;
logic rstn;
logic [55:0] data_vector;
logic tx; //rx
logic rx; //tx
logic led;
logic busy;
logic event_flag;
logic new_data_strobe;
uart_wrapper_connection_t data_from_packet;


slave_packet_transceiver #(56, 3_000_000, 100_000_000) DUT(.*);

parameter CLK_PERIOD_NS = 10;



class TestUartThread;


localparam UART_BAUDRATE_Mb = 3;
localparam UART_BIT_TIME_NS = 1000/UART_BAUDRATE_Mb;
localparam UART_HALF_BIT_TIME_NS = UART_BIT_TIME_NS/2.0;

local struct
{
	byte unsigned true_command;
	bit unsigned [63:0] pkt;
} data;




typedef struct packed
{
	logic [31:0] preamble_frame_delimiter;	
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx, mtx, duty_cycle;
	logic [15:0] pulse_width;
	logic [7:0] end_of_frame;
} packet_from_pc_t;

typedef struct packed
{
	logic [31:0] preamble_frame_delimiter;	
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx, mtx, duty_cycle;
	logic [23:0] pulse_width;
	logic [7:0] end_of_frame;
} wrong_packet_pattern_from_pc_t;
 
 
localparam packet_from_pc_t true_packet_pattern_0 = '{preamble_frame_delimiter:32'hAA_AA_AA_FD,
																		mdr_phase: 8'h1,
																		mdr_att_rx: 8'h3,
																		mdr_att_tx:8'h5,
																		mtx: 8'hAA,
																		duty_cycle: 8'hBB,
																		pulse_width: 16'hABCD,
																		end_of_frame: 8'hDF };
																		
localparam packet_from_pc_t true_packet_pattern_1 = '{preamble_frame_delimiter:32'hAA_AA_AA_FD,
																		mdr_phase: 8'h9,
																		mdr_att_rx: 8'h7,
																		mdr_att_tx:8'h4,
																		mtx: 8'h00,
																		duty_cycle: 8'h11,
																		pulse_width: 16'h5555,
																		end_of_frame: 8'hDF };

localparam packet_from_pc_t true_packet_pattern_2 = '{preamble_frame_delimiter:32'hAA_AA_AA_FD,
																		mdr_phase: 8'h00,
																		mdr_att_rx: 8'h00,
																		mdr_att_tx:8'h00,
																		mtx: 8'h00,
																		duty_cycle: 8'h00,
																		pulse_width: 16'h1111,
																		end_of_frame: 8'hDF };																			
																		
																		
localparam packet_from_pc_t false_packet_pattern_0 = '{ preamble_frame_delimiter:32'hAA_EE_AA_FD,
																		mdr_phase: 8'h88,
																		mdr_att_rx: 8'h77,
																		mdr_att_tx:8'h66,
																		mtx: 8'h55,
																		duty_cycle: 8'h44,
																		pulse_width: 16'h1234,
																		end_of_frame: 8'hDF };
																		
localparam packet_from_pc_t false_packet_pattern_1 = '{ preamble_frame_delimiter:32'hAA_EE_AA_FD,
																		mdr_phase: 8'h88,
																		mdr_att_rx: 8'h88,
																		mdr_att_tx:8'h88,
																		mtx: 8'h88,
																		duty_cycle: 8'h88,
																		pulse_width: 16'h8888,
																		end_of_frame: 8'hEE };


localparam wrong_packet_pattern_from_pc_t wrong_packet_pattern = '{ preamble_frame_delimiter:32'hAA_AA_AA_FD,
																		mdr_phase: 8'h88,
																		mdr_att_rx: 8'h88,
																		mdr_att_tx:8'h88,
																		mtx: 8'h88,
																		duty_cycle: 8'h88,
																		pulse_width: 23'h888899,
																		end_of_frame: 8'hEE };																		
																	

task InitUart;
	rx = 1;
	rstn = 0;
	#100;
	rstn = 1;
	#100;
endtask


task SendByteViaUart(byte unsigned transmitting_byte);
begin
	rx = 0;
	#UART_BIT_TIME_NS;
	for (int i = 0; i < 8; i = i + 1)
	begin
		rx = transmitting_byte[i];
		#UART_BIT_TIME_NS;
	end
	rx = 1;
	#UART_BIT_TIME_NS;
	#500;
end
endtask

task SendPacketViaUart(packet_from_pc_t packet);
begin
	SendByteViaUart(packet.preamble_frame_delimiter[31:24]);
	SendByteViaUart(packet.preamble_frame_delimiter[23:16]);
	SendByteViaUart(packet.preamble_frame_delimiter[15:8]);
	SendByteViaUart(packet.preamble_frame_delimiter[7:0]);
	SendByteViaUart(packet.mdr_phase);
	SendByteViaUart(packet.mdr_att_rx);
	SendByteViaUart(packet.mdr_att_tx);
	SendByteViaUart(packet.mtx);
	SendByteViaUart(packet.duty_cycle);
	SendByteViaUart(packet.pulse_width[15:8]);
	SendByteViaUart(packet.pulse_width[7:0]);
	SendByteViaUart(packet.end_of_frame);
end
endtask

task SendWrongPatternPacketViaUart(wrong_packet_pattern_from_pc_t packet);
begin
	SendByteViaUart(packet.preamble_frame_delimiter[31:24]);
	SendByteViaUart(packet.preamble_frame_delimiter[23:16]);
	SendByteViaUart(packet.preamble_frame_delimiter[15:8]);
	SendByteViaUart(packet.preamble_frame_delimiter[7:0]);
	SendByteViaUart(packet.mdr_phase);
	SendByteViaUart(packet.mdr_att_rx);
	SendByteViaUart(packet.mdr_att_tx);
	SendByteViaUart(packet.mtx);
	SendByteViaUart(packet.duty_cycle);
	SendByteViaUart(packet.pulse_width[23:16]);
	SendByteViaUart(packet.pulse_width[15:8]);
	SendByteViaUart(packet.pulse_width[7:0]);
	SendByteViaUart(packet.end_of_frame);
end
endtask

endclass

TestUartThread uart;

initial begin
//	$monitor($time, " state=%s", DUT.transceiver_machine_state,
//				" frame state=%s", DUT.frame_counter);
	
	clk = 0;
	event_flag = 0;

	uart.InitUart();
	
	$display("TRANSMITTING WRONG PACKET 0");
	uart.SendPacketViaUart(uart.false_packet_pattern_0);
	
	@(DUT.transceiver_machine_state === DUT.enUART_IDLE);
	
	$display("TRANSMITTING RIGHT PACKET 0");
	uart.SendPacketViaUart(uart.true_packet_pattern_0);
	@(DUT.transceiver_machine_state === DUT.enWAITING_FOR_EVENT);
	
	event_flag = 1;
	#100;
	event_flag = 0;

	uart.SendByteViaUart(8'hFF); //oops something on line...
	
	#50000;
	
	uart.SendByteViaUart(8'h00); //oops something on line...again
	uart.SendByteViaUart(8'h23); //oops something on line...again
	uart.SendByteViaUart(8'hFF); //oops something on line...again
	
	$display("TRANSMITTING RIGHT PACKET 1");
	uart.SendPacketViaUart(uart.true_packet_pattern_1);
	
	@(DUT.transceiver_machine_state === DUT.enWAITING_FOR_EVENT);
	event_flag = 1;
	#100;
	event_flag = 0;
	
	#50000;
	
	
	$display("TRANSMITTING WRONG PACKET 1");
	uart.SendPacketViaUart(uart.false_packet_pattern_1);
	
	#50000;
	
	$display("TRANSMITTING RIGHT PACKET 0");
	uart.SendPacketViaUart(uart.true_packet_pattern_0);
	@(DUT.transceiver_machine_state === DUT.enWAITING_FOR_EVENT);
	event_flag = 1;
	#100;
	event_flag = 0;
	
	$display("TRANSMITTING RIGHT PACKET 2");
	uart.SendPacketViaUart(uart.true_packet_pattern_2);
	
	$display("TRANSMITTING WRONG PACKET PATTERN");
	uart.SendWrongPatternPacketViaUart(uart.wrong_packet_pattern);
	
	#50000;
	

	$display("TRANSMITTING RIGHT PACKET 2");
	uart.SendPacketViaUart(uart.true_packet_pattern_2);
	
	#50000;
	$stop;
end



always #(CLK_PERIOD_NS/2.0) clk = ~clk;

endmodule