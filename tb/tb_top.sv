`timescale 1ns/1ps

import common_values::*;


module tb_top();

logic clk;
logic tpm_clk, tpm_ena, tpm_data, tpm_gate;
logic rx_from_ft232;
logic tx_to_ft232;
logic tpm_status;
logic [2:0] leds; 
logic enable_driver_12, enable_driver_34;
logic external_button;
logic [3:0] switch;


top DUT(.*);



class UartThread;

localparam VALUE_PHASE = 5'h13;
localparam VALUE_RX_ATT = 4'h9;
localparam VALUE_TX_ATT = 4'h7;
localparam DUTY_CYCLE = 10'd500;
localparam PULSE_WIDTH = 8'd1;
localparam UART_BAUDRATE_Mb = 3;
localparam UART_BIT_TIME_NS = 1000/UART_BAUDRATE_Mb;
localparam UART_HALF_BIT_TIME_NS = UART_BIT_TIME_NS/2.0;



typedef struct packed
{
	logic [7:0] preamble_frame_delimiter;	
	logic [7:0] mdr_phase, mdr_att_rx, mdr_att_tx;
	logic [23:0] mtx_duty_cycle_pulse_width;
	logic [7:0] end_of_frame;
} packet_from_pc_t;


packet_from_pc_t test_package, emitting_package, singled_package;


function new();
	CreatePkt();
endfunction


task InitUart;
	rx_from_ft232 = 1;
	#100;
endtask

integer error_counter = 0;

task SendByteViaUart(input byte unsigned packet);
begin
	$display($time, " PC starts sending data via UART");
	rx_from_ft232 = 0;
	#UART_BIT_TIME_NS;
	for (int i = 0; i < 8; i = i + 1)
	begin
		rx_from_ft232 = packet[i];
		#UART_BIT_TIME_NS;
	end
	rx_from_ft232 = 1;
	#UART_BIT_TIME_NS;
	#500;
	$display($time, " PC has sended one byte via UART");
end
endtask


task SendPacketViaUart(input packet_from_pc_t packet);
begin
	SendByteViaUart(packet.preamble_frame_delimiter[7:0]);
	SendByteViaUart(packet.mdr_phase);
	SendByteViaUart(packet.mdr_att_rx);
	SendByteViaUart(packet.mdr_att_tx);
	SendByteViaUart(packet.mtx_duty_cycle_pulse_width[23:16]);
	SendByteViaUart(packet.mtx_duty_cycle_pulse_width[15:8]);
	SendByteViaUart(packet.mtx_duty_cycle_pulse_width[7:0]);
	SendByteViaUart(packet.end_of_frame);
end
endtask

task ReceiveByteViaUart;
byte unsigned uart_data_frame; 
begin
	@(negedge tx_to_ft232);
	#UART_BIT_TIME_NS;
	
	for (int i = 0; i < 8; i = i + 1)
	begin
		uart_data_frame[i] = tx_to_ft232;
		#UART_BIT_TIME_NS;
	end
	
	#UART_BIT_TIME_NS;
//	$display("PC got byte via uart=%h", uart_data_frame);
end
endtask



local function void CreatePkt;
	test_package.preamble_frame_delimiter  = 8'hFD;
	test_package.mdr_phase = {3'b100,VALUE_PHASE};
	test_package.mdr_att_rx = {4'b1010,VALUE_RX_ATT};
	test_package.mdr_att_tx = {4'b1100,VALUE_TX_ATT};
	test_package.mtx_duty_cycle_pulse_width = {DUTY_CYCLE , 3'b111, 1'b0, 1'b0 , 1'b0, PULSE_WIDTH}; //set, no cycle, emit
	test_package.end_of_frame = 8'hDF;
	$display("Packet created=%h", test_package);
	
	singled_package.preamble_frame_delimiter  = 8'hFD;
	singled_package.mdr_phase = {3'b100,VALUE_PHASE};
	singled_package.mdr_att_rx = {4'b1010,VALUE_RX_ATT};
	singled_package.mdr_att_tx = {4'b1100,VALUE_TX_ATT};
	singled_package.mtx_duty_cycle_pulse_width = {10'd5 , 3'b111, 1'b1, 1'b0 , 1'b0, 8'd1}; //send, no cycle, no emit
	singled_package.end_of_frame = 8'hDF;
	
	emitting_package.preamble_frame_delimiter  = 8'hFD;
	emitting_package.mdr_phase = {3'b100, 5'b0};
	emitting_package.mdr_att_rx = {4'b1010, 4'b0};
	emitting_package.mdr_att_tx = {4'b1100, 4'b0};
	emitting_package.mtx_duty_cycle_pulse_width = {10'd5 , 3'b111, 1'b1, 1'b1 , 1'b0, 8'd1}; //send, cycle, no emit
	emitting_package.end_of_frame = 8'hDF;
	$display("Packet created=%h", emitting_package);
	
endfunction

endclass

UartThread uart;


logic frame_error, receiver_data_is_rdy, master_got_data;
assign frame_error = DUT.uart_wrapper.frame_error;
assign receiver_data_is_rdy = DUT.uart_wrapper.receiver_data_is_ready;
assign master_got_data = DUT.uart_wrapper.master_got_data;



initial begin
	$display("MAX_DUTY_CYCLE_COUNTER_VALUE=%d", DUT.tpm_signals_generator.MAX_DUTY_CYCLE_COUNTER_VALUE);
	$display("size of big counter=%d", $bits(DUT.tpm_signals_generator.width_duty_counter));
	$display("common_values::DUTY_CYCLE_MAX_VALUE - 1=%d", common_values::DUTY_CYCLE_MAX_VALUE - 1);
	rx_from_ft232 = 1;
	
	clk = 0;
	tpm_status = 1;
	external_button = 0;

	uart = new;
	uart.InitUart();
	
	force DUT.cycle_emission = 1;
	#100_000;
	release DUT.cycle_emission;
	
	#500_000;
	
	
/*	for(int i = 0; i < 200; i = i + 1)
	begin
		uart.SendByteViaUart($urandom_range(0,255));
		#50000;
	end
*/
	uart.SendPacketViaUart(uart.test_package);
	
	#100_000;
	
	uart.SendPacketViaUart(uart.emitting_package);

	
	#500_000;

	uart.SendPacketViaUart(uart.singled_package);
	
	#5000_000;
	
//	uart.SendPacketViaUart(uart.test_package);
	
	#5000_000;
	
//	$display("error_counter=%d", uart.error_counter);
	$stop;
end


always #5 clk = ~clk;


initial forever uart.ReceiveByteViaUart();


logic tpm_data_strobe;
assign tpm_data_strobe = (((DUT.tpm_signals_generator.CLK_NUMBER_DATA_VECTOR_START_POINT - 1) < DUT.tpm_signals_generator.tpm_clk_counter) && (DUT.tpm_signals_generator.tpm_clk_counter <= (DUT.tpm_signals_generator.CLK_NUMBER_DATA_VECTOR_END_POINT - 1)));

logic tpm_rdy;
assign tpm_rdy = DUT.tpm_signals_generator.rdy;

logic cycled_emission;
assign cycled_emission = DUT.tpm_signals_generator.cycle_emission;


logic emission_request;
assign emission_request = DUT.emitting_request;

logic start_transmission_from_duty_counter;
assign start_transmission_from_duty_counter = DUT.tpm_signals_generator.start_transmission_from_duty_counter;

logic tpm_gate_standart;
assign tpm_gate_standart = DUT.tpm_signals_generator.tpm_gate_standart_control;

logic tpm_gate_pulse;
assign tpm_gate_pulse = DUT.tpm_signals_generator.tpm_gate_pulse_control;



int duty_cycle_value;
assign duty_cycle_value = DUT.tpm_signals_generator.duty_cycle_value;

int pulse_width_value;
assign pulse_width_value = DUT.tpm_signals_generator.pulse_width_value;

int phase_value;
assign phase_value = DUT.tpm_signals_generator.phase;

logic start;
assign start = DUT.start_tpm_session;

int pulse_value;
assign pulse_value= DUT.tpm_signals_generator.pulse_width_value_for_counter;

int duty_value;
assign duty_value = DUT.tpm_signals_generator.duty_cycle_value_for_counter;


logic width_duty_counter_enable;
assign width_duty_counter_enable = DUT.tpm_signals_generator.width_duty_counter_enable;

int width_duty_counter;
assign width_duty_counter = DUT.tpm_signals_generator.width_duty_counter;

int duty_cycle_value_for_counter;
assign duty_cycle_value_for_counter = DUT.tpm_signals_generator.duty_cycle_value_for_counter;
endmodule	