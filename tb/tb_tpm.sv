`timescale 1ns/1ns

import common_values::*;

module tb_TPM();

logic clk, rstn;
logic [3:0] tx_attn, rx_attn;
logic [4:0] phase;
logic start;
logic rdy;
logic tpm_clk, tpm_ena, tpm_data, tpm_gate;


logic [$clog2(common_values::DUTY_CYCLE_MAX_VALUE)-1:0] duty_cycle_value;
logic [$clog2(common_values::PULSE_WIDTH_MAX_VALUE_US)-1:0] pulse_width_value;

logic emitting_request;
logic cycle_emission;

tpm DUT(.*);

byte clk_counter;

initial
begin
	$monitor($time, " width_duty_counter_enable=%b", DUT.width_duty_counter_enable);
	duty_cycle_value = 2;
	pulse_width_value = 100;
	emitting_request = 1;
	cycle_emission = 1;
	tx_attn = 4'b1110;
	rx_attn = 4'b0011;
	phase = 5'b01001;
	clk_counter = 0;
	clk = 0;
	rstn = 1;
	#20;
	$display("PULSE_WIDTH_MAX_VALUE_US=%d",  PULSE_WIDTH_MAX_VALUE_US);
	$display("DUTY_CYCLE_MAX_VALUE_PERCENT=%d", DUTY_CYCLE_MAX_VALUE);
	$display("MAX_DUTY_CYCLE_COUNTER_VALUE=%d", DUT.MAX_DUTY_CYCLE_COUNTER_VALUE);
	$display("pulse_width_value_for_counter=%d", DUT.pulse_width_value_for_counter);
	$display("duty_cycle_value_for_counter=%d", DUT.duty_cycle_value_for_counter);
	start = 0;
	#20;
//	start = 1;
	#20;
	start = 0;

	
	@(DUT.tpm_clk_state == DUT.enIDLE_TPM_CLK);
	#2000000;


	
//	#50000000;
	
	$stop;
end

always #5 clk = ~clk;

logic tpm_gate_standart_control, tpm_gate_pulse_control;
assign tpm_gate_standart_control = DUT.tpm_gate_standart_control;
assign tpm_gate_pulse_control = DUT.tpm_gate_pulse_control;


logic main_cycle_rdy, tpm_gate_rdy;
assign main_cycle_rdy = DUT.main_cycle_rdy;
assign tpm_gate_rdy = DUT.tpm_gate_rdy;

logic data_strobe;
assign data_strobe = (((DUT.CLK_NUMBER_DATA_VECTOR_START_POINT - 1) < DUT.tpm_clk_counter) && (DUT.tpm_clk_counter <= (DUT.CLK_NUMBER_DATA_VECTOR_END_POINT - 1)));

//always @(negedge tpm_clk) if(data_strobe) $display($time, " pointer on data vector=%d", DUT.tpm_clk_counter - DUT.CLK_NUMBER_DATA_VECTOR_START_POINT, " vector bit=%b", DUT.data_vector[DUT.CLK_NUMBER_DATA_VECTOR_END_POINT - 1 - DUT.tpm_clk_counter]);


logic width_duty_counter_enable;
assign width_duty_counter_enable = DUT.width_duty_counter_enable;

endmodule